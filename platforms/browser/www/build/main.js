webpackJsonp([24],{

/***/ 11:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VarsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var VarsProvider = /** @class */ (function () {
    function VarsProvider(storage) {
        this.storage = storage;
        this.usuario = null;
        this.first = true;
    }
    VarsProvider.prototype.init = function () {
        return this.storage.get('dados');
    };
    VarsProvider.prototype.getUsuario = function () {
        return this.usuario;
    };
    VarsProvider.prototype.setUsuario = function (usuario) {
        this.usuario = usuario;
    };
    VarsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */]])
    ], VarsProvider);
    return VarsProvider;
}());

//# sourceMappingURL=vars.js.map

/***/ }),

/***/ 129:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VagaProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var VagaProvider = /** @class */ (function () {
    function VagaProvider(http) {
        this.http = http;
        this.url = 'http://167.250.199.228:8080';
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({
                'Content-Type': 'application/json'
            }),
            observe: 'response'
        };
        this.accessToken = '';
    }
    VagaProvider.prototype.setHeaderToken = function (token) {
        this.accessToken = token;
    };
    VagaProvider.prototype.getVagas = function (_a, page) {
        var nome = _a.nome, cidade = _a.cidade, estado = _a.estado;
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        var params = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpParams */]();
        if (nome)
            params = params.append('nome', nome);
        if (cidade)
            params = params.append('cidade', cidade);
        if (estado)
            params = params.append('estado', estado);
        params = params.append('page', page);
        params = params.append('size', '10');
        return this.http.get(this.url + '/api/vagas', __assign({}, this.httpOptions, { params: params })).map(function (response) { return response['body']; });
    };
    VagaProvider.prototype.getVaga = function (id) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        var params = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpParams */]();
        params = params.append('id', id.toString());
        params = params.append('page', '0');
        params = params.append('size', '5');
        return this.http.get(this.url + '/api/moradias', __assign({}, this.httpOptions, { params: params })).map(function (response) { return response['body']; });
    };
    VagaProvider.prototype.addVaga = function (vaga) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.post(this.url + '/api/vagas', vaga, this.httpOptions).map(function (response) { return response['body']; });
    };
    VagaProvider.prototype.updateVaga = function (vaga) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.put(this.url + '/api/vagas', vaga, this.httpOptions).map(function (response) { return response['body']; });
    };
    VagaProvider.prototype.deleteVaga = function (vaga) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        var id = typeof vaga === 'number' ? vaga : vaga.id;
        return this.http.delete(this.url, this.httpOptions).map(function (response) { return response['body']; });
    };
    VagaProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], VagaProvider);
    return VagaProvider;
}());

//# sourceMappingURL=vaga.js.map

/***/ }),

/***/ 152:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuIndexPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_vars_vars__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__vaga_list_vaga_list__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tarefa_list_tarefa_list__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__moradia_list_moradia_list__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__evento_list_evento_list__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__notificacoes_notificacoes__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__membro_list_membro_list__ = __webpack_require__(90);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var MenuIndexPage = /** @class */ (function () {
    function MenuIndexPage(appCtrl, navCtrl, navParams, varsProvider) {
        this.appCtrl = appCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.varsProvider = varsProvider;
        this.dadosUsuario = null;
        this.vagaListPage = __WEBPACK_IMPORTED_MODULE_3__vaga_list_vaga_list__["a" /* VagaListPage */];
        this.tarefaListPage = __WEBPACK_IMPORTED_MODULE_4__tarefa_list_tarefa_list__["a" /* TarefaListPage */];
        this.moradiaListPage = __WEBPACK_IMPORTED_MODULE_5__moradia_list_moradia_list__["a" /* MoradiaListPage */];
        this.eventoListPage = __WEBPACK_IMPORTED_MODULE_6__evento_list_evento_list__["a" /* EventoListPage */];
        this.notificacoesPage = __WEBPACK_IMPORTED_MODULE_7__notificacoes_notificacoes__["a" /* NotificacoesPage */];
        this.membroListPage = __WEBPACK_IMPORTED_MODULE_8__membro_list_membro_list__["a" /* MembroListPage */];
        this.dadosUsuario = this.varsProvider.getUsuario();
    }
    MenuIndexPage.prototype.goToAndSetRoot = function (page, params) {
        this.appCtrl.getRootNav().setRoot(page, (params ? params : {}));
        // this.navCtrl.setRoot(page, (params ? params : {}));
    };
    MenuIndexPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-menu-index',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/menu-index/menu-index.html"*/'\n<ion-content padding>\n    <ion-grid>\n        <ion-row style="margin-top: -15px;">\n            <ion-col (click)="goToAndSetRoot(moradiaListPage, {})">\n                <p text-center>\n                    <img width="80px" height="80px" src="assets/imgs/logo_buscar.jpeg">\n                </p>\n                <h3 text-center>\n                    Moradias\n                </h3>\n            </ion-col>\n            <ion-col (click)="goToAndSetRoot(eventoListPage, {})">\n                <p text-center>\n                    <img width="80px" height="80px" src="assets/imgs/logo_eventos.jpeg">\n                </p>\n                <h3 text-center>\n                    Eventos\n                </h3>\n            </ion-col>\n        </ion-row>\n\n        <ion-row style="margin-top: -15px;" [hidden]="!dadosUsuario?.usuario?.moradia">\n            <ion-col (click)="goToAndSetRoot(membroListPage, {})">\n                <p text-center>\n                    <img width="80px" height="80px" src="assets/imgs/logo_membros.jpeg">\n                </p>\n                <h3 text-center>\n                    Membros\n                </h3>\n            </ion-col>\n            <ion-col (click)="goToAndSetRoot(tarefaListPage, {})">\n                <p text-center>\n                    <img width="80px" height="80px" src="assets/imgs/logo_calendario.jpeg">\n                </p>\n                <h3 text-center>\n                    Tarefas\n                </h3>\n            </ion-col>\n        </ion-row>\n\n        <ion-row style="margin-top: -15px;">\n            <ion-col (click)="goToAndSetRoot(vagaListPage, {})">\n                <p text-center>\n                    <img width="80px" height="80px" src="assets/imgs/logo_editar.jpeg">\n                </p>\n                <h3 text-center>\n                    Vagas\n                </h3>\n            </ion-col>\n            <ion-col [hidden]="!dadosUsuario?.usuario?.moradia" (click)="goToAndSetRoot(notificacoesPage, {})">\n                <p text-center>\n                    <img width="80px" height="80px" src="assets/imgs/logo_notificacoes.jpeg">\n                </p>\n                <h3 text-center>\n                    Notificações\n                </h3>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/menu-index/menu-index.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_vars_vars__["a" /* VarsProvider */]])
    ], MenuIndexPage);
    return MenuIndexPage;
}());

//# sourceMappingURL=menu-index.js.map

/***/ }),

/***/ 153:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VagaModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_vars_vars__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_moradia_moradia__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var VagaModalPage = /** @class */ (function () {
    function VagaModalPage(navCtrl, navParams, varsProvider, moradiaProvider, loadingCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.varsProvider = varsProvider;
        this.moradiaProvider = moradiaProvider;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.nomeResponsavel = '';
        this.dadosUsuario = this.varsProvider.getUsuario();
        if (!this.dadosUsuario) {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
            this.navCtrl.popToRoot();
        }
        this.moradiaProvider.setHeaderToken(this.dadosUsuario['accessToken']);
        this.vaga = this.navParams.get('vaga');
    }
    VagaModalPage.prototype.candidatarUsuario = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: 'Por favor, aguarde.'
        });
        loader.present();
        this.moradiaProvider.solicitarIngressoAMoradia(this.vaga['moradia']['id'], this.dadosUsuario.usuario.id).subscribe(function (response) {
            if (response['meta']['messageCode'] === "msg.solicitacao.enviada") {
                loader.dismiss();
                var alert_1 = _this.alertCtrl.create({
                    title: 'Solicitação Enviada!',
                    subTitle: 'Sua solicitação de vaga na moradia foi feita com sucesso.',
                    buttons: [
                        {
                            text: 'OK',
                            handler: function () {
                                _this.navCtrl.pop();
                            }
                        }
                    ]
                });
                alert_1.present();
            }
            else {
                loader.dismiss();
                var alert_2 = _this.alertCtrl.create({
                    title: 'Outra mensage',
                    subTitle: response['meta']['messageCode'],
                    buttons: [
                        {
                            text: 'OK',
                            handler: function () {
                                _this.navCtrl.pop();
                            }
                        }
                    ]
                });
                alert_2.present();
            }
        }, function (error) {
            loader.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Aviso',
                subTitle: 'Não foi possível realizar a operação.',
                buttons: [
                    {
                        text: 'OK',
                        handler: function () {
                            _this.navCtrl.pop();
                        }
                    }
                ]
            });
            alert.present();
        });
    };
    VagaModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-vaga-modal',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/vaga-modal/vaga-modal.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>Anúncio de Vaga</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <div class="container">\n        <ion-grid>\n            <form>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Nome do Responsável {{ nomeResponsavel }}</ion-label>\n                        <ion-input type="text" [value]="vaga?.nomeCriador" [readonly]="true"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Quantidade de quartos disponíveis</ion-label>\n                        <ion-input type="number" [value]="vaga.qtdQuartos" [readonly]="true"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Descrição do(s) quarto(s)</ion-label>\n                        <ion-input type="text" [value]="vaga.descricao" [readonly]="true"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Público Alvo</ion-label>\n                        <!--<ion-option value="todos" [selected]="vaga.publicoAlvo === \'todos\'">Todos</ion-option>-->\n                        <!--<ion-option value="homens" [selected]="vaga.publicoAlvo === \'homens\'">Homens</ion-option>-->\n                        <!--<ion-option value="mulheres" [selected]="vaga.publicoAlvo === \'mulheres\'">Mulheres</ion-option>-->\n                        <ion-input type="text" *ngIf="vaga.publicoAlvo === \'todos\'" value="Todos" [readonly]="true"></ion-input>\n                        <ion-input type="text" *ngIf="vaga.publicoAlvo === \'homens\'" value="Homens" [readonly]="true"></ion-input>\n                        <ion-input type="text" *ngIf="vaga.publicoAlvo === \'mulheres\'" value="Mulheres" [readonly]="true"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Observações</ion-label>\n                        <ion-textarea [value]="vaga.observacoes" [readonly]="true"></ion-textarea>\n                    </ion-item>\n                </ion-row>\n                <div padding>\n                    <button ion-button *ngIf="dadosUsuario?.usuario?.moradia === null" (click)="candidatarUsuario()">\n                        Candidatar-me para a Vaga\n                    </button>\n                </div>\n            </form>\n        </ion-grid>\n    </div>\n\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/vaga-modal/vaga-modal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_vars_vars__["a" /* VarsProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_moradia_moradia__["a" /* MoradiaProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], VagaModalPage);
    return VagaModalPage;
}());

//# sourceMappingURL=vaga-modal.js.map

/***/ }),

/***/ 154:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoradiaModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_moradia_moradia__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_vars_vars__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MoradiaModalPage = /** @class */ (function () {
    function MoradiaModalPage(navCtrl, navParams, loadingCtrl, moradiaProvider, alertCtrl, varsProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.moradiaProvider = moradiaProvider;
        this.alertCtrl = alertCtrl;
        this.varsProvider = varsProvider;
        this.dadosUsuario = this.varsProvider.getUsuario();
        this.moradia = this.navParams.get('moradia');
    }
    MoradiaModalPage.prototype.solicitarIngresso = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: 'Por favor, aguarde.'
        });
        loader.present();
        this.moradiaProvider.solicitarIngressoAMoradia(this.moradia.id, this.dadosUsuario['usuario']['id']).subscribe(function (response) {
            console.log('Response', response);
            loader.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Solicitação Enviada!',
                subTitle: 'Sua solicitação foi enviada com sucesso.',
                buttons: [
                    {
                        text: 'OK',
                        handler: function () {
                            _this.navCtrl.pop();
                        }
                    }
                ]
            });
            alert.present();
        }, function (error) {
            loader.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Aviso',
                subTitle: 'Não foi possível realizar a operação.',
                buttons: [
                    {
                        text: 'OK',
                        handler: function () {
                            _this.navCtrl.pop();
                        }
                    }
                ]
            });
            alert.present();
        });
    };
    MoradiaModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-moradia-modal',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/moradia-modal/moradia-modal.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>Detalhes da Moradia</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n    <div class="container">\n        <ion-grid>\n            <form>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Nome</ion-label>\n                        <ion-input type="text" [value]="moradia?.nome" [readonly]="true"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>CEP</ion-label>\n                        <ion-input type="number" [value]="moradia?.cep" [readonly]="true"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Endereço</ion-label>\n                        <ion-input type="text" [value]="moradia?.endereco" [readonly]="true"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-col col-3>\n                        <ion-item>\n                            <ion-label floating>Nº</ion-label>\n                            <ion-input type="number" [value]="moradia?.numero" [readonly]="true"></ion-input>\n                        </ion-item>\n                    </ion-col>\n                    <ion-col col-9>\n                        <ion-item>\n                            <ion-label floating>Complemento</ion-label>\n                            <ion-input type="text" [value]="moradia?.complemento" [readonly]="true"></ion-input>\n                        </ion-item>\n                    </ion-col>\n                </ion-row>\n                <ion-row>\n                    <ion-col col-3>\n                        <ion-list>\n                            <ion-item>\n                                <ion-label floating>UF</ion-label>\n                                <ion-input type="text" [value]="moradia?.estado" [readonly]="true">{{ moradia?.estado}}</ion-input>\n                            </ion-item>\n                        </ion-list>\n                    </ion-col>\n                    <ion-col col-5>\n                        <ion-item>\n                            <ion-label floating>Cidade</ion-label>\n                            <ion-input type="text" [value]="moradia?.cidade" [readonly]="true"></ion-input>\n                        </ion-item>\n                    </ion-col>\n                    <ion-col col-4>\n                        <ion-item>\n                            <ion-label floating>Bairro</ion-label>\n                            <ion-input type="text" [value]="moradia?.bairro" [readonly]="true"></ion-input>\n                        </ion-item>\n                    </ion-col>\n                </ion-row>\n                <button ion-button [hidden]="dadosUsuario?.usuario?.moradia !== null" (click)="solicitarIngresso()">\n                    Solicitar Ingresso na Moradia\n                </button>\n            </form>\n        </ion-grid>\n    </div>\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/moradia-modal/moradia-modal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_moradia_moradia__["a" /* MoradiaProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_vars_vars__["a" /* VarsProvider */]])
    ], MoradiaModalPage);
    return MoradiaModalPage;
}());

//# sourceMappingURL=moradia-modal.js.map

/***/ }),

/***/ 155:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_evento_evento__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_vars_vars__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var EventoPage = /** @class */ (function () {
    function EventoPage(navCtrl, navParams, formBuilder, eventoProvider, loadingCtrl, alertCtrl, toastCtrl, varsProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.eventoProvider = eventoProvider;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.varsProvider = varsProvider;
        this.dadosUsuario = this.varsProvider.getUsuario();
        if (this.dadosUsuario) {
            this.eventoProvider.setHeaderToken(this.dadosUsuario['accessToken']);
        }
        else {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]);
            this.navCtrl.popToRoot();
        }
        this.eventoForm = this.formBuilder.group({
            nome: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            descricao: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(100)])],
            telefoneContato: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].minLength(10), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(11)])],
            atracoes: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(100)])],
            dataRealizacao: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            valorEntrada: [null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])]
        });
    }
    EventoPage.prototype.onSubmit = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Por favor, aguarde."
        });
        loader.present();
        var data = {
            nome: this.eventoForm.value.nome,
            descricao: this.eventoForm.value.descricao,
            dataRealizacao: this.eventoForm.value.dataRealizacao,
            telefoneContato: this.eventoForm.value.telefoneContato,
            atracoes: this.eventoForm.value.atracoes,
            valorEntrada: this.eventoForm.value.valorEntrada,
            criadorId: this.dadosUsuario['usuario']['id'],
            moradiaId: this.dadosUsuario['usuario']['moradia']['id']
        };
        this.eventoProvider.addEvento(data).subscribe(function (response) {
            console.log('ADD Evento Response', response);
            if (response['meta']['messageCode'] === "msg.anuncioevento.registrado") {
                loader.dismiss();
                var alert_1 = _this.alertCtrl.create({
                    title: 'Evento Criado!',
                    subTitle: 'Evento criado com sucesso.',
                    buttons: [
                        {
                            text: 'OK',
                            handler: function () {
                                _this.navCtrl.pop();
                            }
                        }
                    ]
                });
                alert_1.present();
            }
            else {
                loader.dismiss();
                var alert_2 = _this.alertCtrl.create({
                    title: 'Mensagem',
                    subTitle: response['meta']['messageCode'],
                    buttons: [
                        {
                            text: 'OK',
                            handler: function () {
                                _this.navCtrl.pop();
                            }
                        }
                    ]
                });
                alert_2.present();
            }
        }, function (error) {
            console.log('ADD Evento ERROR', error);
            loader.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Aviso',
                subTitle: 'Não foi possível criar o evento.',
                buttons: [
                    {
                        text: 'OK',
                        handler: function () {
                            _this.navCtrl.pop();
                        }
                    }
                ]
            });
            alert.present();
        });
    };
    EventoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-evento',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/evento/evento.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>Evento</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <div class="container">\n        <ion-grid>\n            <form [formGroup]="eventoForm" (ngSubmit)="onSubmit()">\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Nome</ion-label>\n                        <ion-input type="text" formControlName="nome"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Descrição</ion-label>\n                        <ion-input type="text" formControlName="descricao"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Telefone de Contato</ion-label>\n                        <ion-input type="tel" formControlName="telefoneContato"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Atrações</ion-label>\n                        <ion-input type="text" formControlName="atracoes"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Data de Realização</ion-label>\n                        <ion-datetime displayFormat="DD MMM YYYY HH:mm" formControlName="dataRealizacao"></ion-datetime>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Valor da Entrada</ion-label>\n                        <ion-input type="number" formControlName="valorEntrada"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <div padding>\n                    <button ion-button btn-block color="submitButton" type="submit"\n                            block [disabled]="!eventoForm.valid">\n                        Cadastrar Evento\n                    </button>\n                </div>\n            </form>\n        </ion-grid>\n    </div>\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/evento/evento.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_3__providers_evento_evento__["a" /* EventoProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_vars_vars__["a" /* VarsProvider */]])
    ], EventoPage);
    return EventoPage;
}());

//# sourceMappingURL=evento.js.map

/***/ }),

/***/ 156:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventoModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_vars_vars__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_evento_evento__ = __webpack_require__(79);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EventoModalPage = /** @class */ (function () {
    function EventoModalPage(navCtrl, navParams, varsProvider, eventoProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.varsProvider = varsProvider;
        this.eventoProvider = eventoProvider;
        this.dadosUsuario = this.varsProvider.getUsuario();
        if (this.dadosUsuario) {
            this.eventoProvider.setHeaderToken(this.dadosUsuario['accessToken']);
        }
        this.evento = this.navParams.get('evento');
        // this.evento.dataRealizacao = this.evento.dataRealizacao.split('T')[0].split('-')[2] + '/' + this.evento.dataRealizacao.split('T')[0].split('-')[1] + '/' + this.evento.dataRealizacao.split('T')[0].split('-')[0] + ' às ' + this.evento.dataRealizacao.split('T')[1].split('.')[0].split(':')[0] + ':' + this.evento.dataRealizacao.split('T')[1].split('.')[0].split(':')[1];
    }
    EventoModalPage.prototype.backButton = function () {
        this.navCtrl.pop();
    };
    EventoModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-evento-modal',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/evento-modal/evento-modal.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-buttons left>\n            <button navPop ion-button icon-only>\n                <ion-icon name="arrow-round-back" class="headerButton"></ion-icon>\n            </button>\n        </ion-buttons>\n        <ion-title>Informações do Evento</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <div class="container">\n        <ion-grid>\n            <form>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Nome</ion-label>\n                        <ion-input [value]="evento.nome" [readonly]="true"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Descrição</ion-label>\n                        <ion-input type="text" [value]="evento.descricao" [readonly]="true"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Telefone de Contato</ion-label>\n                        <ion-input type="tel" [value]="evento.telefoneContato" [readonly]="true"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Atrações</ion-label>\n                        <ion-input type="text" [value]="evento.atracoes" [readonly]="true"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Data de Realização</ion-label>\n                        <ion-input type="text" [value]="evento.dataRealizacao" [readonly]="true"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Valor da Entrada</ion-label>\n                        <ion-input type="text" [value]="\'R$ \' + evento.valorEntrada.toString()" [readonly]="true"></ion-input>\n                    </ion-item>\n                </ion-row>\n\n            </form>\n        </ion-grid>\n    </div>\n\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/evento-modal/evento-modal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_vars_vars__["a" /* VarsProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_evento_evento__["a" /* EventoProvider */]])
    ], EventoModalPage);
    return EventoModalPage;
}());

//# sourceMappingURL=evento-modal.js.map

/***/ }),

/***/ 157:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SemanaIndexPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_vars_vars__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_tarefa_tarefa__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tarefa_tarefa__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__avaliar_tarefa_avaliar_tarefa__ = __webpack_require__(84);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SemanaIndexPage = /** @class */ (function () {
    function SemanaIndexPage(navCtrl, navParams, varsProvider, tarefaProvider, loadingCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.varsProvider = varsProvider;
        this.tarefaProvider = tarefaProvider;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.search = false;
        this.searching = false;
        this.pageSearch = 0;
        this.hasNextPageSearch = false;
        this.nomeSearch = '';
        this.scrollEnabled = true;
        this.tarefaPage = __WEBPACK_IMPORTED_MODULE_4__tarefa_tarefa__["a" /* TarefaPage */];
        this.avaliarTarefaPage = __WEBPACK_IMPORTED_MODULE_5__avaliar_tarefa_avaliar_tarefa__["a" /* AvaliarTarefaPage */];
        this.tarefas = [
            {
                id: 30,
                nome: 'Limpar a casa',
                descricao: 'Tem que limpar a casa, poxa',
                moradiaId: 1,
                responsaveis: [1, 4, 5],
                tipo: 'Tarefa Doméstica',
                dataConclusao: '2018-12-15'
            }
        ];
        this.dadosUsuario = this.varsProvider.getUsuario();
        if (this.dadosUsuario) {
            this.tarefaProvider.setHeaderToken(this.dadosUsuario['accessToken']);
        }
    }
    SemanaIndexPage.prototype.atualizarTarefa = function (tarefa, finalizado) {
        // const loader = this.loadingCtrl.create(
        //     {
        //         content: "Por favor, aguarde enquanto executamos a operação.",
        //     }
        // );
        // let url = 'http://167.250.197.3:1026/v1/contextEntities/' + tarefa.id.toString() + '/attributes/finalizado';
        // let mensagem = "";
        // let headers = new Headers();
        // headers.append('Content-Type', 'application/json');
        // headers.append('Accept', 'application/json');
        //
        // let options = new RequestOptions(
        //     {
        //         headers: headers
        //     }
        // );
        // let data = JSON.stringify(
        //     {
        //         "value": finalizado
        //     }
        // );
        // this.http.put(url, data, options).subscribe(
        //     data => {
        //         let statusCode = data.status;
        //         if(statusCode === 200)
        //         {
        //             mensagem = "Tarefa atualizada com sucesso.";
        //         } else {
        //             mensagem = "Não foi possível atualizar a tarefa. Atualize a lista de tarefas e tente novamente.";
        //         }
        //         loader.dismiss();
        //         const alert = this.alertCtrl.create(
        //             {
        //                 title: 'Mensagem',
        //                 subTitle: mensagem,
        //                 buttons: [
        //                     {
        //                         text: 'OK',
        //                         handler: () => {
        //                             this.getTarefas();
        //                         }
        //                     }
        //                 ]
        //             }
        //         );
        //         alert.present();
        //     },
        //     error => {
        //         mensagem = "Não foi possível atualizar a tarefa. Verifique sua conexão com a internet e tente novamente.";
        //         loader.dismiss();
        //         const alert = this.alertCtrl.create(
        //             {
        //                 title: 'Mensagem',
        //                 subTitle: mensagem,
        //                 buttons: [
        //                     {
        //                         text: 'OK',
        //                         handler: () => {
        //                             this.getTarefas();
        //                         }
        //                     }
        //                 ]
        //             }
        //         );
        //         alert.present();
        //     }
        // );
    };
    SemanaIndexPage.prototype.excluirTarefa = function (tarefa) {
        // const loader = this.loadingCtrl.create(
        //     {
        //         content: "Por favor, aguarde enquanto executamos a operação.",
        //     }
        // );
        // let url = 'http://167.250.197.3:1026/v1/contextEntities/' + tarefa.id.toString();
        // let mensagem = "";
        // this.http.delete(url).subscribe(
        //     data => {
        //         let statusCode = data.status;
        //         if(statusCode === 200)
        //         {
        //             mensagem = "Tarefa excluída com sucesso.";
        //         } else {
        //             mensagem = "Não foi possível excluir a tarefa. Atualize a lista de tarefas e tente novamente.";
        //         }
        //         loader.dismiss();
        //         const alert = this.alertCtrl.create(
        //             {
        //                 title: 'Mensagem',
        //                 subTitle: mensagem,
        //                 buttons: [
        //                     {
        //                         text: 'OK',
        //                         handler: () => {
        //                             this.getTarefas();
        //                         }
        //                     }
        //                 ]
        //             }
        //         );
        //         alert.present();
        //     },
        //     error => {
        //         mensagem = "Não foi possível excluir a tarefa. Verifique sua conexão com a internet e tente novamente.";
        //         loader.dismiss();
        //         const alert = this.alertCtrl.create(
        //             {
        //                 title: 'Mensagem',
        //                 subTitle: mensagem,
        //                 buttons: [
        //                     {
        //                         text: 'OK',
        //                         handler: () => {
        //                             this.getTarefas();
        //                         }
        //                     }
        //                 ]
        //             }
        //         );
        //         alert.present();
        //     }
        // );
    };
    SemanaIndexPage.prototype.getTarefas = function (_a, page) {
        var _this = this;
        var nome = _a.nome, moradia = _a.moradia;
        this.searching = true;
        if (!this.loader) {
            this.loader = this.loadingCtrl.create({
                content: 'Por favor, aguarde.'
            });
            this.loader.present();
        }
        if (this.pageSearch == 0)
            this.tarefas = [];
        this.tarefaProvider.getTarefas({
            nome: '',
            moradia: this.dadosUsuario.usuario.moradia.id
        }, 0).subscribe(function (response) {
            console.log(response);
            _this.searching = false;
            _this.loader.dismiss();
            if (response['meta']['messageCode'] === "msg.servico.execucao.sucesso") {
                response['data']['tarefas'].forEach(function (item) {
                    if (item.responsaveis.indexOf(_this.dadosUsuario['usuario']['id']) > -1) {
                        _this.tarefas.push(item);
                    }
                });
            }
            else {
            }
        }, function (error) {
            console.log(error);
            _this.searching = false;
            _this.loader.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Mensagem',
                subTitle: 'Não foi possível consultar as tarefas.',
                buttons: ['OK']
            });
            alert.present();
        });
    };
    SemanaIndexPage.prototype.scrollTarefas = function (infiniteScroll) {
        var _this = this;
        setTimeout(function () {
            if (_this.hasNextPageSearch) {
                _this.pageSearch += 1;
                _this.getTarefas({
                    nome: _this.nomeSearch,
                    moradia: _this.dadosUsuario.usuario.moradia.id
                }, _this.pageSearch);
            }
            if (_this.pageSearch + 1 >= _this.totalPages) {
                _this.scrollEnabled = false;
                // infiniteScroll.enabled = false;
            }
            infiniteScroll.complete();
        }, 500);
    };
    SemanaIndexPage.prototype.ionViewWillEnter = function () {
        this.searching = true;
        this.loader = this.loadingCtrl.create({
            content: 'Por favor, aguarde'
        });
        // this.loader.present();
        this.getTarefas({
            nome: '',
            moradia: this.dadosUsuario.usuario.moradia.id
        }, 0);
    };
    SemanaIndexPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-semana-index',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/semana-index/semana-index.html"*/'<ion-content padding>\n    <h3 *ngIf="!searching && tarefas.length == 0" padding>Nenhuma tarefa.</h3>\n    <ion-list>\n        <ion-item-sliding *ngFor="let tarefa of tarefas; let i = index">\n            <ion-item>\n                {{ tarefa.nome}}\n                <p>{{ tarefa.descricao }}</p>\n            </ion-item>\n            <ion-item-options side="right">\n                <button ion-button color="primary" [navPush]="tarefaPage" [navParams]="{mode: \'edit\', tarefa: tarefa, usuario: dadosUsuario}">\n                    <ion-icon name="create"></ion-icon>\n                    Editar\n                </button>\n                <button ion-button color="secondary" [navPush]="avaliarTarefaPage" [navParams]="{tarefa: tarefa, usuario: dadosUsuario}">\n                    <ion-icon name="star"></ion-icon>\n                    Avaliar\n                </button>\n                <button ion-button color="danger" (click)="excluirTarefa(tarefa)">\n                    <ion-icon name="trash"></ion-icon>\n                    Excluir\n                </button>\n            </ion-item-options>\n        </ion-item-sliding>\n    </ion-list>\n    <ion-infinite-scroll (ionInfinite)="scrollVagas($event)" [enabled]="scrollEnabled">\n        <ion-infinite-scroll-content></ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n    <ion-fab right bottom>\n        <button ion-fab color="orange" [navPush]="tarefaPage" [navParams]="{mode: \'create\', usuario: dadosUsuario}">\n            <ion-icon name="add"></ion-icon>\n        </button>\n    </ion-fab>\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/semana-index/semana-index.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_vars_vars__["a" /* VarsProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_tarefa_tarefa__["a" /* TarefaProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], SemanaIndexPage);
    return SemanaIndexPage;
}());

//# sourceMappingURL=semana-index.js.map

/***/ }),

/***/ 158:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EsqueciSenhaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_usuario_usuario__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var EsqueciSenhaPage = /** @class */ (function () {
    function EsqueciSenhaPage(navCtrl, navParams, formBuilder, loadingCtrl, usuarioProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.loadingCtrl = loadingCtrl;
        this.usuarioProvider = usuarioProvider;
        this.form = this.formBuilder.group({
            email: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
        });
    }
    EsqueciSenhaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EsqueciSenhaPage');
    };
    EsqueciSenhaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-esqueci-senha',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/esqueci-senha/esqueci-senha.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Esqueci minha Senha</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <div class="container">\n        <ion-grid>\n            <form [formGroup]="form" (ngSubmit)="enviar()">\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Email</ion-label>\n                        <ion-input type="text" formControlName="email"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <div padding>\n                    <button ion-button btn-block color="submitButton" type="submit" block [disabled]="!form.valid">\n                        Enviar\n                    </button>\n                </div>\n            </form>\n        </ion-grid>\n    </div>\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/esqueci-senha/esqueci-senha.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3__providers_usuario_usuario__["a" /* UsuarioProvider */]])
    ], EsqueciSenhaPage);
    return EsqueciSenhaPage;
}());

//# sourceMappingURL=esqueci-senha.js.map

/***/ }),

/***/ 159:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BasePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__usuario_create_usuario_create__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BasePage = /** @class */ (function () {
    function BasePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.usuarioCreatePage = __WEBPACK_IMPORTED_MODULE_2__usuario_create_usuario_create__["a" /* UsuarioCreatePage */];
        this.loginPage = __WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */];
    }
    BasePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-base',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/base/base.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>FACTIO</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <div class="container">\n        <ion-grid>\n            <ion-row>\n                <ion-col>\n                    <div id="logo" text-center></div>\n                </ion-col>\n            </ion-row>\n            <br>\n            <br>\n            <ion-row>\n                <ion-col col-5>\n                    <p text-center>Já possui conta?</p>\n                    <button ion-button btn-block block color="factioBase" [navPush]="loginPage">\n                        Entrar\n                    </button>\n                </ion-col>\n                <ion-col col-2></ion-col>\n                <ion-col col-5>\n                    <p text-center>Novo no FACTIO?</p>\n                    <button ion-button btn-block block color="buttonGrey" [navPush]="usuarioCreatePage">Cadastrar</button>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </div>\n</ion-content>\n\n<ion-footer style="background-color: #E3E0F5; height: 150px;" >\n    <ion-toolbar transparent>\n        <ion-grid>\n            <ion-row style="margin-top: -15px;">\n                <ion-col>\n                    <p text-center>\n                        <img width="80px" height="80px" src="assets/imgs/lupa.jpeg">\n                    </p>\n                    <h3 text-center>\n                        Moradias\n                    </h3>\n                </ion-col>\n                <ion-col>\n                    <p text-center>\n                        <img width="80px" height="80px" src="assets/imgs/evento.jpeg">\n                    </p>\n                    <h3 text-center>\n                        Eventos\n                    </h3>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/base/base.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], BasePage);
    return BasePage;
}());

//# sourceMappingURL=base.js.map

/***/ }),

/***/ 160:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocalizacaoMembrosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_moradia_moradia__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_vars_vars__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__localizacao_modal_localizacao_modal__ = __webpack_require__(161);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LocalizacaoMembrosPage = /** @class */ (function () {
    function LocalizacaoMembrosPage(navCtrl, navParams, moradiaProvider, varsProvider, loadingCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.moradiaProvider = moradiaProvider;
        this.varsProvider = varsProvider;
        this.loadingCtrl = loadingCtrl;
        this.modalCtrl = modalCtrl;
        this.membros = [];
        this.todosMembros = [];
        this.searching = false;
        this.dadosUsuario = this.varsProvider.getUsuario();
        console.log(this.dadosUsuario);
        if (this.dadosUsuario) {
            this.moradiaProvider.setHeaderToken(this.dadosUsuario['accessToken']);
        }
        else {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* LoginPage */]);
            this.navCtrl.popToRoot();
        }
    }
    LocalizacaoMembrosPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.searching = true;
        var loader = this.loadingCtrl.create({
            content: 'Por favor, aguarde.'
        });
        loader.present();
        this.membros = [];
        this.moradiaProvider.getMembrosDaMoradia(this.dadosUsuario['usuario']['moradia']['id']).subscribe(function (response) {
            _this.searching = false;
            if (response['meta']['messageCode'] === "msg.servico.execucao.sucesso") {
                response['data']['usuarios'].forEach(function (usuario) {
                    var ultimoLoginStr = usuario['ultimoLogin'].split('T')[0].split('-')[2] + '/' + usuario['ultimoLogin'].split('T')[0].split('-')[1] + '/' + usuario['ultimoLogin'].split('T')[0].split('-')[0] + ' às ' + usuario['ultimoLogin'].split('T')[1].split('.')[0].split(':')[0] + ':' + usuario['ultimoLogin'].split('T')[1].split('.')[0].split(':')[1];
                    usuario['ultimoLogin'] = ultimoLoginStr;
                    _this.todosMembros.push(usuario);
                    if (usuario['id'] !== _this.dadosUsuario['usuario']['id'])
                        _this.membros.push(usuario);
                });
                loader.dismiss();
            }
            else {
                loader.dismiss();
            }
        }, function (error) {
            _this.searching = false;
            console.log('error', error);
            loader.dismiss();
        });
    };
    LocalizacaoMembrosPage.prototype.showModal = function (membros) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__localizacao_modal_localizacao_modal__["a" /* LocalizacaoModalPage */], { membros: membros });
        modal.present();
    };
    LocalizacaoMembrosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-localizacao-membros',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/localizacao-membros/localizacao-membros.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu" class="headerButton"></ion-icon>\n        </button>\n        <ion-title>Localizar Membros</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <h3 *ngIf="!searching && membros.length === 0" padding>\n        Nenhum Usuário.\n    </h3>\n    <ion-list>\n        <ion-item *ngIf="membros.length > 0" (click)="showModal(todosMembros)">Todos</ion-item>\n\n        <ion-item *ngFor="let membro of membros" (click)="showModal([membro])">\n            <h4>{{ membro.nome }}</h4>\n        </ion-item>\n    </ion-list>\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/localizacao-membros/localizacao-membros.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_moradia_moradia__["a" /* MoradiaProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_vars_vars__["a" /* VarsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], LocalizacaoMembrosPage);
    return LocalizacaoMembrosPage;
}());

//# sourceMappingURL=localizacao-membros.js.map

/***/ }),

/***/ 161:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocalizacaoModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_vars_vars__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// https://docs.google.com/presentation/d/e/2PACX-1vScoho1ensbR4qCI9AIuQN55BZVvK73pAjI7sumDvW3CrxxHnrmpXWUjx2-8CpFibqU1EjLKCRhuthJ/pub?start=false&loop=false&delayms=3000&slide=id.g3e207b82bf_886_0
// http://www.fabricadecodigo.com/google-maps-e-geolocalizacao-com-ionic/
// https://github.com/ionic-team/ionic-native-google-maps/tree/master/documents/marker
// API KEY = AIzaSyCaSPHbraJ2Y2N04TGHVd14TTAdQzx9jIM
var LocalizacaoModalPage = /** @class */ (function () {
    function LocalizacaoModalPage(navCtrl, navParams, varsProvider, geolocation, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.varsProvider = varsProvider;
        this.geolocation = geolocation;
        this.alertCtrl = alertCtrl;
        this.membros = [];
        this.markers = [];
        this.dadosUsuario = this.varsProvider.getUsuario();
        this.membros = this.navParams.get('membros');
        console.log('membros', this.membros);
    }
    LocalizacaoModalPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (resp) {
            var req = {
                position: {
                    lat: resp.coords.latitude,
                    lng: resp.coords.longitude
                }
            };
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["b" /* Geocoder */].geocode(req).then(function (results) {
                // console.log('results', results);
            });
            _this.loadMap(resp.coords);
        }).catch(function (error) {
            var alert = _this.alertCtrl.create({
                title: 'Aviso',
                subTitle: 'Não foi possível determinar sua localização.',
                buttons: ['OK']
            });
            alert.present();
            _this.loadMap({
                // Coordenadas do Brasil
                latitude: -14.235,
                longitude: -51.9253
            });
        });
    };
    LocalizacaoModalPage.prototype.loadMap = function (coordinates) {
        var _this = this;
        // This code is necessary for browser
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["a" /* Environment */].setEnv({
            'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyCaSPHbraJ2Y2N04TGHVd14TTAdQzx9jIM',
            'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyCaSPHbraJ2Y2N04TGHVd14TTAdQzx9jIM'
        });
        var mapOptions = {
            camera: {
                target: {
                    lat: coordinates.latitude,
                    lng: coordinates.longitude
                },
                zoom: 14,
                tilt: 30
            }
        };
        this.map = __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["c" /* GoogleMaps */].create(document.getElementById('map_canvas'), mapOptions);
        this.map.one(__WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MAP_READY).then(function () {
            _this.membros.forEach(function (membro) {
                if (membro.latitude && membro.longitude) {
                    console.log('blaaaaaaaaaaaaaaaaa', membro);
                    var marker_1 = _this.map.addMarkerSync({
                        title: membro.nome,
                        icon: 'black',
                        snippet: 'Última atualização: ' + membro['ultimoLogin'],
                        animation: 'DROP',
                        position: {
                            lat: parseFloat(membro.latitude),
                            lng: parseFloat(membro.longitude)
                        }
                    });
                    marker_1.on(__WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK).subscribe(function () {
                        marker_1.showInfoWindow();
                    }, function (error) {
                        console.log('errooo');
                    });
                    _this.markers.push(marker_1);
                }
            });
        }).catch(function (error) {
            console.log(error);
            var alert = _this.alertCtrl.create({
                title: 'Aviso',
                subTitle: 'Não foi possível abrir o mapa.',
                buttons: [
                    {
                        text: 'OK',
                        handler: function () {
                            _this.navCtrl.pop();
                        }
                    }
                ]
            });
            alert.present();
        });
    };
    LocalizacaoModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-localizacao-modal',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/localizacao-modal/localizacao-modal.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-buttons left>\n            <button navPop ion-button icon-only>\n                <ion-icon name="arrow-round-back" class="headerButton"></ion-icon>\n            </button>\n        </ion-buttons>\n        <ion-title text-center>Visualização de Localização</ion-title>\n    </ion-navbar>\n</ion-header>\n<ion-content>\n    <div id="map_canvas"></div>\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/localizacao-modal/localizacao-modal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_vars_vars__["a" /* VarsProvider */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], LocalizacaoModalPage);
    return LocalizacaoModalPage;
}());

//# sourceMappingURL=localizacao-modal.js.map

/***/ }),

/***/ 171:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 171;

/***/ }),

/***/ 18:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__esqueci_senha_esqueci_senha__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__usuario_create_usuario_create__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_usuario_usuario__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_moradia_moradia__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_vars_vars__ = __webpack_require__(11);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, appCtrl, formBuilder, loadingCtrl, storage, alertCtrl, usuarioProvider, moradiaProvider, varsProvider, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.appCtrl = appCtrl;
        this.formBuilder = formBuilder;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.usuarioProvider = usuarioProvider;
        this.moradiaProvider = moradiaProvider;
        this.varsProvider = varsProvider;
        this.events = events;
        this.esqueciSenhaPage = __WEBPACK_IMPORTED_MODULE_5__esqueci_senha_esqueci_senha__["a" /* EsqueciSenhaPage */];
        this.usuarioCreatePage = __WEBPACK_IMPORTED_MODULE_6__usuario_create_usuario_create__["a" /* UsuarioCreatePage */];
        this.loginForm = this.formBuilder.group({
            email: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].email, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
            senha: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].minLength(1), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(20), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])]
        });
    }
    LoginPage.prototype.submitForm = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Por favor, aguarde.",
        });
        loader.present();
        this.usuarioProvider.authenticate(this.loginForm.value.email, this.loginForm.value.senha).subscribe(function (response) {
            if (response['meta']['messageCode'] === "msg.servico.execucao.sucesso") {
                var dados_1 = {
                    accessToken: response['data']['accessToken'],
                    usuario: __assign({}, response['data']['usuario'])
                };
                _this.moradiaProvider.setHeaderToken(dados_1['accessToken']);
                _this.moradiaProvider.getMoradiaPeloUsuario(response['data']['usuario']['id']).subscribe(function (res) {
                    dados_1['usuario']['moradia'] = __assign({}, res['data']);
                    _this.varsProvider.setUsuario(dados_1);
                    _this.storage.set('dados', JSON.stringify(dados_1));
                    _this.events.publish('updateUsuario');
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
                    _this.navCtrl.popToRoot();
                    loader.dismiss();
                }, function (err) {
                    _this.varsProvider.setUsuario(dados_1);
                    _this.storage.set('dados', JSON.stringify(dados_1));
                    _this.events.publish('updateUsuario');
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
                    _this.navCtrl.popToRoot();
                    loader.dismiss();
                });
            }
        }, function (error) {
            loader.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Aviso',
                subTitle: (error.status === 401 ? 'Email ou senha não estão corretos.' : 'Não foi possível realizar o login.'),
                buttons: ['OK']
            });
            alert.present();
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/login/login.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>Login</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <div class="container">\n        <ion-grid>\n            <ion-row>\n                <ion-col>\n                    <!--<div id="logo" text-center></div>-->\n                    <div text-center>\n                        <img src="assets/imgs/logo_factio.jpeg">\n                    </div>\n\n                </ion-col>\n            </ion-row>\n            <form [formGroup]="loginForm" (ngSubmit)="submitForm()">\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Email</ion-label>\n                        <ion-input type="email" formControlName="email"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Senha</ion-label>\n                        <ion-input type="password" formControlName="senha"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <div padding>\n                    <button ion-button btn-block color="submitButton" type="submit" block [disabled]="!loginForm.valid">\n                        Entrar\n                    </button>\n                    <p right [navPush]="esqueciSenhaPage">Esqueci a Senha</p>\n                </div>\n            </form>\n            <ion-row>\n                <ion-col>\n                    <p text-center>Novo no Factio?</p>\n                </ion-col>\n            </ion-row>\n            <ion-row>\n                <ion-col>\n                    <button ion-button btn-block color="submitButton" block [navPush]="usuarioCreatePage">\n                        Criar uma Conta\n                    </button>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </div>\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_7__providers_usuario_usuario__["a" /* UsuarioProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_moradia_moradia__["a" /* MoradiaProvider */], __WEBPACK_IMPORTED_MODULE_9__providers_vars_vars__["a" /* VarsProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 214:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/avaliar-tarefa/avaliar-tarefa.module": [
		434,
		23
	],
	"../pages/base/base.module": [
		435,
		22
	],
	"../pages/esqueci-senha/esqueci-senha.module": [
		436,
		21
	],
	"../pages/evento-list/evento-list.module": [
		437,
		20
	],
	"../pages/evento-modal/evento-modal.module": [
		438,
		19
	],
	"../pages/evento/evento.module": [
		439,
		18
	],
	"../pages/localizacao-membros/localizacao-membros.module": [
		440,
		17
	],
	"../pages/localizacao-modal/localizacao-modal.module": [
		441,
		16
	],
	"../pages/login/login.module": [
		442,
		15
	],
	"../pages/membro-detail/membro-detail.module": [
		443,
		14
	],
	"../pages/membro-list/membro-list.module": [
		444,
		13
	],
	"../pages/menu-index/menu-index.module": [
		445,
		12
	],
	"../pages/moradia-detail/moradia-detail.module": [
		454,
		11
	],
	"../pages/moradia-list/moradia-list.module": [
		446,
		10
	],
	"../pages/moradia-modal/moradia-modal.module": [
		447,
		9
	],
	"../pages/moradia/moradia.module": [
		450,
		8
	],
	"../pages/notificacoes/notificacoes.module": [
		448,
		7
	],
	"../pages/semana-index/semana-index.module": [
		449,
		6
	],
	"../pages/tarefa-list/tarefa-list.module": [
		451,
		5
	],
	"../pages/tarefa/tarefa.module": [
		452,
		4
	],
	"../pages/usuario-create/usuario-create.module": [
		453,
		3
	],
	"../pages/vaga-list/vaga-list.module": [
		455,
		2
	],
	"../pages/vaga-modal/vaga-modal.module": [
		457,
		1
	],
	"../pages/vaga/vaga.module": [
		456,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 214;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 22:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoradiaProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MoradiaProvider = /** @class */ (function () {
    function MoradiaProvider(http) {
        this.http = http;
        this.url = 'http://167.250.199.228:8080/api/moradias';
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({
                'Content-Type': 'application/json'
            }),
            observe: 'response'
        };
        this.accessToken = '';
    }
    MoradiaProvider.prototype.setHeaderToken = function (token) {
        this.accessToken = token;
    };
    MoradiaProvider.prototype.getMoradias = function (_a, page) {
        var nome = _a.nome, cidade = _a.cidade, estado = _a.estado;
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        var params = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpParams */]();
        if (nome)
            params = params.append('nome', nome);
        if (cidade)
            params = params.append('cidade', cidade);
        if (estado)
            params = params.append('estado', estado);
        params = params.append('page', page);
        params = params.append('size', '10');
        return this.http.get(this.url, __assign({}, this.httpOptions, { params: params })).map(function (response) { return response['body']; });
    };
    MoradiaProvider.prototype.getMembrosDaMoradia = function (moradiaId) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        var params = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpParams */]();
        params = params.append('moradiaId', moradiaId.toString());
        return this.http.get(this.url + '/membros', __assign({}, this.httpOptions, { params: params })).map(function (response) { return response['body']; });
    };
    MoradiaProvider.prototype.getMoradiaPeloUsuario = function (usuarioId) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.get(this.url + '/membro/' + usuarioId.toString(), __assign({}, this.httpOptions)).map(function (response) { return response['body']; });
    };
    MoradiaProvider.prototype.getMoradia = function (nome) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        var params = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpParams */]();
        params = params.append('nome', nome);
        params = params.append('page', '0');
        params = params.append('size', '10');
        return this.http.get(this.url, __assign({}, this.httpOptions, { params: params })).map(function (response) { return response['body']; });
    };
    MoradiaProvider.prototype.solicitarIngressoAMoradia = function (moradiaId, usuarioId) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.post(this.url + '/solicitacaoIngresso', {
            moradia: moradiaId,
            usuario: usuarioId
        }, this.httpOptions).map(function (response) { return response['body']; });
    };
    MoradiaProvider.prototype.listarSolicitacoesIngressoMoradia = function (moradiaId) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.get(this.url + '/solicitacoes/' + moradiaId.toString(), this.httpOptions).map(function (response) { return response['body']; });
    };
    MoradiaProvider.prototype.removerSolicitacao = function (moradia, usuario) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.post(this.url + '/removerSolicitacao', {
            moradia: moradia,
            usuario: usuario
        }, this.httpOptions).map(function (response) { return response['body']; });
    };
    MoradiaProvider.prototype.addMembroAMoradia = function (moradia, usuario) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.post(this.url + '/inserirMembro', {
            moradia: moradia,
            usuario: usuario
        }, this.httpOptions).map(function (response) { return response['body']; });
    };
    MoradiaProvider.prototype.addMoradia = function (moradia) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.post(this.url, moradia, this.httpOptions).map(function (response) { return response['body']; });
    };
    MoradiaProvider.prototype.updateMoradia = function (moradia) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.put(this.url, moradia, this.httpOptions).map(function (response) { return response['body']; });
    };
    MoradiaProvider.prototype.deleteMoradia = function (moradia) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        var id = typeof moradia === 'number' ? moradia : moradia.id;
        return this.http.delete(this.url, this.httpOptions).pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["tap"])(function (_) {
            console.log('pronto');
        }));
    };
    MoradiaProvider.prototype.consultarCEP = function (cep) {
        var url = "https://viacep.com.br/ws/" + cep + "/json/";
        return this.http.get(url);
    };
    MoradiaProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], MoradiaProvider);
    return MoradiaProvider;
}());

//# sourceMappingURL=moradia.js.map

/***/ }),

/***/ 278:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MembroDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the MembroDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MembroDetailPage = /** @class */ (function () {
    function MembroDetailPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    MembroDetailPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MembroDetailPage');
    };
    MembroDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-membro-detail',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/membro-detail/membro-detail.html"*/'<!--\n  Generated template for the MembroDetailPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar>\n        <ion-title>membro-detail</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/membro-detail/membro-detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], MembroDetailPage);
    return MembroDetailPage;
}());

//# sourceMappingURL=membro-detail.js.map

/***/ }),

/***/ 279:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(280);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(287);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 287:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(433);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_login_login__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_moradia_moradia__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_moradia_list_moradia_list__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_tarefa_tarefa__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_tarefa_list_tarefa_list__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_evento_evento__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_evento_list_evento_list__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_vaga_vaga__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_vaga_list_vaga_list__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_esqueci_senha_esqueci_senha__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_usuario_create_usuario_create__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_base_base__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_menu_index_menu_index__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_semana_index_semana_index__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_status_bar__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_splash_screen__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_network__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__angular_common_http__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__providers_tarefa_tarefa__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__providers_moradia_moradia__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__providers_vaga_vaga__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__providers_evento_evento__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__providers_usuario_usuario__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_localizacao_membros_localizacao_membros__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_avaliar_tarefa_avaliar_tarefa__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_moradia_detail_moradia_detail__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_membro_detail_membro_detail__ = __webpack_require__(278);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_membro_list_membro_list__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_moradia_modal_moradia_modal__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_evento_modal_evento_modal__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_vaga_modal_vaga_modal__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__providers_vars_vars__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_notificacoes_notificacoes__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_localizacao_modal_localizacao_modal__ = __webpack_require__(161);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_moradia_moradia__["a" /* MoradiaPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_moradia_list_moradia_list__["a" /* MoradiaListPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_tarefa_tarefa__["a" /* TarefaPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_tarefa_list_tarefa_list__["a" /* TarefaListPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_evento_evento__["a" /* EventoPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_evento_list_evento_list__["a" /* EventoListPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_vaga_vaga__["a" /* VagaPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_vaga_list_vaga_list__["a" /* VagaListPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_esqueci_senha_esqueci_senha__["a" /* EsqueciSenhaPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_usuario_create_usuario_create__["a" /* UsuarioCreatePage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_base_base__["a" /* BasePage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_menu_index_menu_index__["a" /* MenuIndexPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_semana_index_semana_index__["a" /* SemanaIndexPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_localizacao_membros_localizacao_membros__["a" /* LocalizacaoMembrosPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_avaliar_tarefa_avaliar_tarefa__["a" /* AvaliarTarefaPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_moradia_detail_moradia_detail__["a" /* MoradiaDetailPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_membro_detail_membro_detail__["a" /* MembroDetailPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_membro_list_membro_list__["a" /* MembroListPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_moradia_modal_moradia_modal__["a" /* MoradiaModalPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_evento_modal_evento_modal__["a" /* EventoModalPage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_vaga_modal_vaga_modal__["a" /* VagaModalPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_notificacoes_notificacoes__["a" /* NotificacoesPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_localizacao_modal_localizacao_modal__["a" /* LocalizacaoModalPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/avaliar-tarefa/avaliar-tarefa.module#AvaliarTarefaPageModule', name: 'AvaliarTarefaPage', segment: 'avaliar-tarefa', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/base/base.module#BasePageModule', name: 'BasePage', segment: 'base', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/esqueci-senha/esqueci-senha.module#EsqueciSenhaPageModule', name: 'EsqueciSenhaPage', segment: 'esqueci-senha', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/evento-list/evento-list.module#EventoListPageModule', name: 'EventoListPage', segment: 'evento-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/evento-modal/evento-modal.module#EventoModalPageModule', name: 'EventoModalPage', segment: 'evento-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/evento/evento.module#EventoPageModule', name: 'EventoPage', segment: 'evento', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/localizacao-membros/localizacao-membros.module#LocalizacaoMembrosPageModule', name: 'LocalizacaoMembrosPage', segment: 'localizacao-membros', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/localizacao-modal/localizacao-modal.module#LocalizacaoModalPageModule', name: 'LocalizacaoModalPage', segment: 'localizacao-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/membro-detail/membro-detail.module#MembroDetailPageModule', name: 'MembroDetailPage', segment: 'membro-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/membro-list/membro-list.module#MembroListPageModule', name: 'MembroListPage', segment: 'membro-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/menu-index/menu-index.module#MenuIndexPageModule', name: 'MenuIndexPage', segment: 'menu-index', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/moradia-list/moradia-list.module#MoradiaListPageModule', name: 'MoradiaListPage', segment: 'moradia-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/moradia-modal/moradia-modal.module#MoradiaModalPageModule', name: 'MoradiaModalPage', segment: 'moradia-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notificacoes/notificacoes.module#NotificacoesPageModule', name: 'NotificacoesPage', segment: 'notificacoes', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/semana-index/semana-index.module#SemanaIndexPageModule', name: 'SemanaIndexPage', segment: 'semana-index', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/moradia/moradia.module#MoradiaPageModule', name: 'MoradiaPage', segment: 'moradia', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tarefa-list/tarefa-list.module#TarefaListPageModule', name: 'TarefaListPage', segment: 'tarefa-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tarefa/tarefa.module#TarefaPageModule', name: 'TarefaPage', segment: 'tarefa', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/usuario-create/usuario-create.module#UsuarioCreatePageModule', name: 'UsuarioCreatePage', segment: 'usuario-create', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/moradia-detail/moradia-detail.module#MoradiaDetailPageModule', name: 'MoradiaDetailPage', segment: 'moradia-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/vaga-list/vaga-list.module#VagaListPageModule', name: 'VagaListPage', segment: 'vaga-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/vaga/vaga.module#VagaPageModule', name: 'VagaPage', segment: 'vaga', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/vaga-modal/vaga-modal.module#VagaModalPageModule', name: 'VagaModalPage', segment: 'vaga-modal', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_26__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_25__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormsModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_moradia_moradia__["a" /* MoradiaPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_moradia_list_moradia_list__["a" /* MoradiaListPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_tarefa_tarefa__["a" /* TarefaPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_tarefa_list_tarefa_list__["a" /* TarefaListPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_evento_evento__["a" /* EventoPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_evento_list_evento_list__["a" /* EventoListPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_vaga_vaga__["a" /* VagaPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_vaga_list_vaga_list__["a" /* VagaListPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_esqueci_senha_esqueci_senha__["a" /* EsqueciSenhaPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_usuario_create_usuario_create__["a" /* UsuarioCreatePage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_base_base__["a" /* BasePage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_menu_index_menu_index__["a" /* MenuIndexPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_semana_index_semana_index__["a" /* SemanaIndexPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_localizacao_membros_localizacao_membros__["a" /* LocalizacaoMembrosPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_avaliar_tarefa_avaliar_tarefa__["a" /* AvaliarTarefaPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_moradia_detail_moradia_detail__["a" /* MoradiaDetailPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_membro_detail_membro_detail__["a" /* MembroDetailPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_membro_list_membro_list__["a" /* MembroListPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_moradia_modal_moradia_modal__["a" /* MoradiaModalPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_evento_modal_evento_modal__["a" /* EventoModalPage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_vaga_modal_vaga_modal__["a" /* VagaModalPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_notificacoes_notificacoes__["a" /* NotificacoesPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_localizacao_modal_localizacao_modal__["a" /* LocalizacaoModalPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["c" /* GoogleMaps */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_network__["a" /* Network */],
                {
                    provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */],
                    useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicErrorHandler */]
                },
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_27__providers_tarefa_tarefa__["a" /* TarefaProvider */],
                __WEBPACK_IMPORTED_MODULE_28__providers_moradia_moradia__["a" /* MoradiaProvider */],
                __WEBPACK_IMPORTED_MODULE_29__providers_vaga_vaga__["a" /* VagaProvider */],
                __WEBPACK_IMPORTED_MODULE_30__providers_evento_evento__["a" /* EventoProvider */],
                __WEBPACK_IMPORTED_MODULE_31__providers_usuario_usuario__["a" /* UsuarioProvider */],
                __WEBPACK_IMPORTED_MODULE_40__providers_vars_vars__["a" /* VarsProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 433:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_base_base__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_moradia_list_moradia_list__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_tarefa_list_tarefa_list__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_evento_list_evento_list__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_localizacao_membros_localizacao_membros__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_moradia_detail_moradia_detail__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_vaga_list_vaga_list__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_vaga_vaga__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_login_login__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_vars_vars__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_notificacoes_notificacoes__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


















var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, events, splashScreen, storage, varsProvider) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.events = events;
        this.splashScreen = splashScreen;
        this.storage = storage;
        this.varsProvider = varsProvider;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_14__pages_login_login__["a" /* LoginPage */];
        this.tarefaListPage = __WEBPACK_IMPORTED_MODULE_8__pages_tarefa_list_tarefa_list__["a" /* TarefaListPage */];
        this.moradiaListPage = __WEBPACK_IMPORTED_MODULE_7__pages_moradia_list_moradia_list__["a" /* MoradiaListPage */];
        this.eventoListPage = __WEBPACK_IMPORTED_MODULE_9__pages_evento_list_evento_list__["a" /* EventoListPage */];
        this.localizacaoMembros = __WEBPACK_IMPORTED_MODULE_10__pages_localizacao_membros_localizacao_membros__["a" /* LocalizacaoMembrosPage */];
        this.homePage = __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */];
        this.moradiaDetailPage = __WEBPACK_IMPORTED_MODULE_11__pages_moradia_detail_moradia_detail__["a" /* MoradiaDetailPage */];
        this.vagaListPage = __WEBPACK_IMPORTED_MODULE_12__pages_vaga_list_vaga_list__["a" /* VagaListPage */];
        this.vagaPage = __WEBPACK_IMPORTED_MODULE_13__pages_vaga_vaga__["a" /* VagaPage */];
        this.basePage = __WEBPACK_IMPORTED_MODULE_6__pages_base_base__["a" /* BasePage */];
        this.notificacoesPage = __WEBPACK_IMPORTED_MODULE_16__pages_notificacoes_notificacoes__["a" /* NotificacoesPage */];
        this.dadosUsuario = this.varsProvider.init().then(function (val) {
            if (val) {
                _this.varsProvider.setUsuario(JSON.parse(val));
            }
            _this.dadosUsuario = _this.varsProvider.getUsuario();
            _this.initializeApp();
        });
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */] },
            { title: 'Moradias', component: __WEBPACK_IMPORTED_MODULE_7__pages_moradia_list_moradia_list__["a" /* MoradiaListPage */] },
            { title: 'Tarefas', component: __WEBPACK_IMPORTED_MODULE_8__pages_tarefa_list_tarefa_list__["a" /* TarefaListPage */] },
        ];
        this.events.subscribe('updateUsuario', function () {
            _this.updateUsuario();
        });
    }
    MyApp.prototype.updateUsuario = function () {
        this.dadosUsuario = this.varsProvider.getUsuario();
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */]);
            _this.storage.remove('tarefas');
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.sair = function () {
        this.storage.remove('dados');
        this.storage.remove('tarefas');
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_14__pages_login_login__["a" /* LoginPage */]);
        this.nav.popToRoot();
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    MyApp.prototype.goToAndSetRoot = function (page, params) {
        this.nav.setRoot(page, (params ? params : {}));
    };
    MyApp.prototype.goToWithParams = function (page, params) {
        this.nav.push(page, params);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('menuButton'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], MyApp.prototype, "menuButton", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/app/app.html"*/'<ion-menu [content]="content">\n    <ion-content>\n        <ion-list>\n            <ion-item>\n                <div padding>\n                    <ion-header>\n                        <ion-navbar>\n                            <button ion-button menuToggle #menuButton>\n                                <ion-icon name="menu" class="headerButton"></ion-icon>\n                            </button>\n                            <ion-title>Painel de Controle</ion-title>\n                        </ion-navbar>\n                    </ion-header>\n                </div>\n            </ion-item>\n            <div>\n                <h1 padding (click)="goToAndSetRoot(homePage, {})" menuClose>Menu</h1>\n            </div>\n            <!--<ion-item *ngIf="dadosUsuario?.usuario.perfil.id === 1">-->\n                <!--<h2>Tornar-se Premium</h2>-->\n            <!--</ion-item>-->\n            <ion-item (click)="goToAndSetRoot(tarefaListPage, {})" menuClose\n                      [hidden]="dadosUsuario?.usuario?.moradia === null">\n                <h2>Tarefas</h2>\n            </ion-item>\n            <ion-item  (click)="goToAndSetRoot(vagaListPage, {})" menuClose>\n                <h2>Vagas</h2>\n            </ion-item>\n            <ion-item  (click)="goToAndSetRoot(eventoListPage, {})" menuClose>\n                <h2>Eventos</h2>\n            </ion-item>\n            <!--<ion-item menuClose *ngIf="dadosUsuario?.usuario.moradia !== null">-->\n                <!--<h2>Gerenciar Moradia</h2>-->\n            <!--</ion-item>-->\n            <!--*ngIf="dadosUsuario?.usuario?.moradia !== null"-->\n            <ion-item (click)="goToAndSetRoot(moradiaDetailPage, {})"\n                      [hidden]="!dadosUsuario?.usuario?.moradia" menuClose>\n                <h2>Gerenciar Moradia</h2>\n            </ion-item>\n            <ion-item [hidden]="!dadosUsuario?.usuario?.moradia" (click)="goToAndSetRoot(notificacoesPage, {})" menuClose>\n                <h2>Notificações</h2>\n            </ion-item>\n            <ion-item (click)="goToWithParams(vagaPage, {mode: \'create\'})"\n                      [hidden]="!dadosUsuario?.usuario?.moradia" menuClose>\n                <h2>Anunciar Vagas na Moradia</h2>\n            </ion-item>\n            <ion-item *ngIf="dadosUsuario?.usuario" (click)="goToAndSetRoot(moradiaListPage, {})" menuClose>\n                <h2>Procurar Moradias</h2>\n            </ion-item>\n            <!--<ion-item (click)="goTo(localizacaoMembros)" menuClose *ngIf="dadosUsuario?.usuario.moradia !== null">-->\n                <!--<h2>Localizar Membros</h2>-->\n            <!--</ion-item>-->\n            <ion-item [hidden]="!dadosUsuario?.usuario?.moradia" (click)="goToAndSetRoot(localizacaoMembros, {})" menuClose>\n                <h2>Localizar Membros</h2>\n            </ion-item>\n            <ion-item [hidden]="dadosUsuario?.usuario === null" menuClose>\n                <h2>Configurações</h2>\n            </ion-item>\n            <ion-item *ngIf="dadosUsuario" (click)="sair()" menuClose>\n                <h2>Sair</h2>\n            </ion-item>\n            <ion-item *ngIf="!dadosUsuario" (click)="goToAndSetRoot(basePage, {})" menuClose>\n                <h2>Fazer Login</h2>\n            </ion-item>\n        </ion-list>\n    </ion-content>\n</ion-menu>\n\n<ion-nav #content [root]="rootPage" swipeBackEnabled="false"></ion-nav>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_15__providers_vars_vars__["a" /* VarsProvider */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 45:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__menu_index_menu_index__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__semana_index_semana_index__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_tarefa_tarefa__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_moradia_moradia__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_vars_vars__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_usuario_usuario__ = __webpack_require__(52);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, navParams, storage, varsProvider, geolocation, toastCtrl, moradiaProvider, usuarioProvider, tarefaProvider) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.varsProvider = varsProvider;
        this.geolocation = geolocation;
        this.toastCtrl = toastCtrl;
        this.moradiaProvider = moradiaProvider;
        this.usuarioProvider = usuarioProvider;
        this.tarefaProvider = tarefaProvider;
        this.menuIndexPage = __WEBPACK_IMPORTED_MODULE_4__menu_index_menu_index__["a" /* MenuIndexPage */];
        this.semanaIndexPage = __WEBPACK_IMPORTED_MODULE_5__semana_index_semana_index__["a" /* SemanaIndexPage */];
        this.dadosUsuario = {};
        this.dadosUsuario = this.varsProvider.getUsuario();
        if (this.dadosUsuario) {
            this.moradiaProvider.setHeaderToken(this.dadosUsuario['accessToken']);
            this.usuarioProvider.setHeaderToken(this.dadosUsuario['accessToken']);
            this.moradiaProvider.getMoradiaPeloUsuario(this.dadosUsuario.usuario.id).subscribe(function (response) {
                _this.dadosUsuario.usuario.moradia = __assign({}, response['data']);
                _this.varsProvider.setUsuario(_this.dadosUsuario);
                _this.storage.set('dados', JSON.stringify(_this.dadosUsuario));
            }, function (error) {
                // Provavelmente não encontrou a moradia
            });
        }
        else {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
            this.navCtrl.popToRoot();
        }
    }
    HomePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (resp) {
            // Coordenadas do usuário
            var dataHorarioAtual = new Date().toISOString().split('.')[0];
            _this.usuarioProvider.updateLocalizacaoUsuario({
                usuarioId: _this.dadosUsuario.usuario.id,
                latitude: resp.coords.latitude.toString(),
                longitude: resp.coords.longitude.toString(),
                horaLocalizacao: dataHorarioAtual
            }).subscribe(function (response) {
                // console.log('response', response);
            }, function (error) {
                // console.log('error', error);
            });
            // sincronizar as tarefas
            _this.storage.get('tarefas').then(function (val) {
                if (val != null) {
                    var tarefas = JSON.parse(val);
                    var toast = _this.toastCtrl.create({
                        message: 'Sincronizando ' + (tarefas.length).toString() + (tarefas.length > 1 ? ' tarefas.' : ' tarefa.'),
                        duration: 5000
                    });
                    toast.present();
                    var tarefasComErro_1 = 0;
                    tarefas.forEach(function (item) {
                        _this.tarefaProvider.addTarefa(item).subscribe(function (response) {
                        }, function (error) {
                            tarefasComErro_1 += 1;
                        });
                    });
                    if (tarefasComErro_1 > 0) {
                        toast = _this.toastCtrl.create({
                            message: 'Não foi possível sincronizar ' + tarefasComErro_1.toString() + (tarefasComErro_1 == 1 ? ' tarefa.' : ' tarefas.')
                        });
                        toast.present();
                    }
                }
            });
        }).catch(function (e) {
            // Não conseguiu adquirir as coordenadas
        });
    };
    HomePage.prototype.logoff = function () {
        this.storage.remove('dados');
        this.storage.remove('tarefas');
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
        this.navCtrl.popToRoot();
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/home/home.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu" class="headerButton"></ion-icon>\n        </button>\n        <ion-title>Painel de Controle</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <ion-tabs tabsPlacement="top">\n        <ion-tab [root]="menuIndexPage" tabTitle="Menu"></ion-tab>\n        <ion-tab [root]="semanaIndexPage" tabTitle="Semana" *ngIf="dadosUsuario?.usuario?.moradia !== null"></ion-tab>\n    </ion-tabs>\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_9__providers_vars_vars__["a" /* VarsProvider */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */], __WEBPACK_IMPORTED_MODULE_8__providers_moradia_moradia__["a" /* MoradiaProvider */],
            __WEBPACK_IMPORTED_MODULE_10__providers_usuario_usuario__["a" /* UsuarioProvider */], __WEBPACK_IMPORTED_MODULE_6__providers_tarefa_tarefa__["a" /* TarefaProvider */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TarefaProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TarefaProvider = /** @class */ (function () {
    function TarefaProvider(http) {
        this.http = http;
        this.url = 'http://167.250.199.228:8080/api/tarefas';
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({
                'Content-Type': 'application/json'
            }),
            observe: 'response'
        };
        this.accessToken = '';
    }
    TarefaProvider.prototype.setHeaderToken = function (token) {
        this.accessToken = token;
    };
    TarefaProvider.prototype.getTarefas = function (_a, page) {
        var nome = _a.nome, moradia = _a.moradia;
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        var params = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpParams */]();
        if (nome)
            params = params.append('nome', nome);
        if (moradia)
            params = params.append('moradiaId', moradia);
        params = params.append('page', page);
        params = params.append('size', '10');
        return this.http.get(this.url, __assign({}, this.httpOptions, { params: params })).map(function (response) { return response['body']; });
    };
    TarefaProvider.prototype.getTarefa = function (_a) {
        var nome = _a.nome, moradia = _a.moradia;
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        var params = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpParams */]();
        params = params.append('nome', nome);
        params = params.append('moradia', moradia);
        params = params.append('page', '0');
        params = params.append('size', '10');
        return this.http.get(this.url, __assign({}, this.httpOptions, { params: params })).map(function (response) { return response['body']; });
    };
    TarefaProvider.prototype.avaliarTarefa = function (dados) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.post(this.url + '/concluirTarefa', dados, this.httpOptions).map(function (response) { return response['body']; });
    };
    TarefaProvider.prototype.addTarefa = function (tarefa) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.post(this.url, tarefa, this.httpOptions).map(function (response) { return response['body']; });
    };
    TarefaProvider.prototype.updateTarefa = function (tarefa) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.put(this.url, tarefa, this.httpOptions).map(function (response) { return response['body']; });
    };
    TarefaProvider.prototype.deleteTarefa = function (tarefaId) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        this.httpOptions['body'] = {
            tarefaId: tarefaId.toString()
        };
        var params = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpParams */]();
        params = params.append('tarefaId', tarefaId.toString());
        return this.http.delete(this.url, this.httpOptions).map(function (response) { return response['body']; });
    };
    TarefaProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], TarefaProvider);
    return TarefaProvider;
}());

//# sourceMappingURL=tarefa.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsuarioProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UsuarioProvider = /** @class */ (function () {
    function UsuarioProvider(http) {
        this.http = http;
        this.url = 'http://167.250.199.228:8080';
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({
                'Content-Type': 'application/json',
            }),
            observe: 'response'
        };
        this.accessToken = '';
    }
    UsuarioProvider.prototype.setHeaderToken = function (token) {
        this.accessToken = token;
    };
    UsuarioProvider.prototype.authenticate = function (email, senha) {
        return this.http.post(this.url + '/api/auth/login', {
            email: email,
            senha: senha
        }, this.httpOptions).map(function (response) { return response['body']; });
    };
    // public getUsuario(id: number): Observable<Usuario>
    // {
    //     this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
    //     return this.http.get<Usuario>(
    //         this.url
    //     ).pipe(
    //         tap(
    //             _ => {
    //                 console.log('pronto')
    //             }
    //         )
    //     );
    // }
    UsuarioProvider.prototype.addUsuario = function (usuario) {
        return this.http.post(this.url + '/api/auth/signin', usuario, this.httpOptions).map(function (response) { return response['body']; });
    };
    UsuarioProvider.prototype.updateLocalizacaoUsuario = function (dados) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.post(this.url + '/api/usuarios/salvarLocalizacao', dados, this.httpOptions).map(function (response) { return response['body']; });
    };
    UsuarioProvider.prototype.updateUsuario = function (usuario) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.put(this.url + '/api/usuarios', usuario, this.httpOptions).map(function (response) { return response['body']; });
    };
    UsuarioProvider.prototype.deleteUsuario = function (usuario) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        var id = typeof usuario === 'number' ? usuario : usuario.id;
        return this.http.delete(this.url, this.httpOptions).pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["tap"])(function (_) {
            console.log('pronto');
        }));
    };
    UsuarioProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], UsuarioProvider);
    return UsuarioProvider;
}());

//# sourceMappingURL=usuario.js.map

/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TarefaListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tarefa_tarefa__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__avaliar_tarefa_avaliar_tarefa__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_tarefa_tarefa__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__login_login__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_vars_vars__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var TarefaListPage = /** @class */ (function () {
    function TarefaListPage(navCtrl, navParams, loadingCtrl, tarefaProvider, alertCtrl, storage, varsProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.tarefaProvider = tarefaProvider;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.varsProvider = varsProvider;
        this.search = false;
        this.searching = false;
        this.pageSearch = 0;
        this.hasNextPageSearch = false;
        this.nomeSearch = '';
        this.scrollEnabled = true;
        this.tarefaPage = __WEBPACK_IMPORTED_MODULE_2__tarefa_tarefa__["a" /* TarefaPage */];
        this.avaliarTarefaPage = __WEBPACK_IMPORTED_MODULE_4__avaliar_tarefa_avaliar_tarefa__["a" /* AvaliarTarefaPage */];
        this.tarefas = [];
        this.moradia = this.navParams.get('moradia');
        this.dadosUsuario = this.varsProvider.getUsuario();
        if (this.dadosUsuario) {
            this.tarefaProvider.setHeaderToken(this.dadosUsuario['accessToken']);
        }
        else {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__login_login__["a" /* LoginPage */]);
            this.navCtrl.popToRoot();
        }
    }
    TarefaListPage.prototype.toggleSearchBar = function () {
        this.search = !this.search;
        if (this.search)
            this.content.scrollToTop();
    };
    TarefaListPage.prototype.onInput = function (event) {
        this.nomeSearch = event.target.value;
        this.pageSearch = 0;
        this.hasNextPageSearch = false;
        this.scrollEnabled = true;
        this.getTarefas({
            nome: this.nomeSearch,
            moradia: this.moradia.id
        }, 0);
    };
    TarefaListPage.prototype.atualizarTarefa = function (tarefa, finalizado) {
        // const loader = this.loadingCtrl.create(
        //     {
        //         content: "Por favor, aguarde enquanto executamos a operação.",
        //     }
        // );
        // let url = 'http://167.250.197.3:1026/v1/contextEntities/' + tarefa.id.toString() + '/attributes/finalizado';
        // let mensagem = "";
        // let headers = new Headers();
        // headers.append('Content-Type', 'application/json');
        // headers.append('Accept', 'application/json');
        //
        // let options = new RequestOptions(
        //     {
        //         headers: headers
        //     }
        // );
        // let data = JSON.stringify(
        //     {
        //         "value": finalizado
        //     }
        // );
        // this.http.put(url, data, options).subscribe(
        //     data => {
        //         let statusCode = data.status;
        //         if(statusCode === 200)
        //         {
        //             mensagem = "Tarefa atualizada com sucesso.";
        //         } else {
        //             mensagem = "Não foi possível atualizar a tarefa. Atualize a lista de tarefas e tente novamente.";
        //         }
        //         loader.dismiss();
        //         const alert = this.alertCtrl.create(
        //             {
        //                 title: 'Mensagem',
        //                 subTitle: mensagem,
        //                 buttons: [
        //                     {
        //                         text: 'OK',
        //                         handler: () => {
        //                             this.getTarefas();
        //                         }
        //                     }
        //                 ]
        //             }
        //         );
        //         alert.present();
        //     },
        //     error => {
        //         mensagem = "Não foi possível atualizar a tarefa. Verifique sua conexão com a internet e tente novamente.";
        //         loader.dismiss();
        //         const alert = this.alertCtrl.create(
        //             {
        //                 title: 'Mensagem',
        //                 subTitle: mensagem,
        //                 buttons: [
        //                     {
        //                         text: 'OK',
        //                         handler: () => {
        //                             this.getTarefas();
        //                         }
        //                     }
        //                 ]
        //             }
        //         );
        //         alert.present();
        //     }
        // );
    };
    TarefaListPage.prototype.excluirTarefa = function (tarefa) {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Por favor, aguarde enquanto executamos a operação.",
        });
        this.tarefaProvider.deleteTarefa(tarefa.id).subscribe(function (response) {
            console.log('response', response);
            loader.dismiss();
            if (response['meta']['messageCode'] === "msg.tarefa.deletada") {
                var index = _this.tarefas.indexOf(tarefa);
                _this.tarefas.splice(index, 1);
                var alert_1 = _this.alertCtrl.create({
                    title: 'Tarefa Excluída!',
                    subTitle: 'A tarefa foi excluída com sucesso.',
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }, function (error) {
            loader.dismiss();
            console.log('error', error);
            var alert = _this.alertCtrl.create({
                title: 'Aviso',
                subTitle: "Não foi possível excluir a tarefa. Verifique sua conexão com a internet e tente novamente.",
                buttons: ['OK']
            });
            alert.present();
        });
    };
    TarefaListPage.prototype.getTarefas = function (_a, page) {
        var _this = this;
        var nome = _a.nome, moradia = _a.moradia;
        this.searching = true;
        if (!this.loader) {
            this.loader = this.loadingCtrl.create({
                content: 'Por favor, aguarde.'
            });
            this.loader.present();
        }
        if (this.pageSearch == 0)
            this.tarefas = [];
        this.tarefaProvider.getTarefas({
            nome: '',
            moradia: this.dadosUsuario.usuario.moradia.id
        }, 0).subscribe(function (response) {
            console.log(response);
            _this.searching = false;
            _this.loader.dismiss();
            if (response['meta']['messageCode'] === "msg.servico.execucao.sucesso") {
                response['data']['tarefas'].forEach(function (item) {
                    if (!item.completa) {
                        item['dataConclusao'] = item['dataConclusao'].split('T')[0].split('-')[2] + '/' + item['dataConclusao'].split('T')[0].split('-')[1] + '/' + item['dataConclusao'].split('T')[0].split('-')[0];
                        _this.tarefas.push(item);
                    }
                });
            }
            else {
            }
        }, function (error) {
            console.log(error);
            _this.searching = false;
            _this.loader.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Mensagem',
                subTitle: 'Não foi possível consultar as tarefas.',
                buttons: ['OK']
            });
            alert.present();
        });
    };
    TarefaListPage.prototype.scrollTarefas = function (infiniteScroll) {
        var _this = this;
        setTimeout(function () {
            if (_this.hasNextPageSearch) {
                _this.pageSearch += 1;
                _this.getTarefas({
                    nome: _this.nomeSearch,
                    moradia: _this.dadosUsuario.usuario.moradia.id
                }, _this.pageSearch);
            }
            if (_this.pageSearch + 1 >= _this.totalPages) {
                _this.scrollEnabled = false;
                // infiniteScroll.enabled = false;
            }
            infiniteScroll.complete();
        }, 500);
    };
    TarefaListPage.prototype.ionViewWillEnter = function () {
        this.searching = true;
        this.loader = this.loadingCtrl.create({
            content: 'Por favor, aguarde'
        });
        this.loader.present();
        this.getTarefas({
            nome: '',
            moradia: this.dadosUsuario.usuario.moradia.id
        }, 0);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], TarefaListPage.prototype, "content", void 0);
    TarefaListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tarefa-list',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/tarefa-list/tarefa-list.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu" class="headerButton"></ion-icon>\n        </button>\n        <ion-title>Tarefas</ion-title>\n        <ion-buttons end>\n            <button ion-button (click)="toggleSearchBar()">\n                <ion-icon name="search" class="headerButton"></ion-icon>\n            </button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <ion-searchbar *ngIf="search" debounce="1000"\n                   [(ngModel)]="myInput"\n                   [showCancelButton]="shouldShowCancel"\n                   (ionInput)="onInput($event)"\n                   (ionCancel)="onCancel($event)">\n    </ion-searchbar>\n    <h3 *ngIf="!searching && tarefas.length == 0" padding>Nenhuma tarefa.</h3>\n    <ion-list>\n        <ion-item-sliding *ngFor="let tarefa of tarefas; let i = index">\n            <ion-item>\n                {{ tarefa.nome}}<ion-icon  *ngIf="tarefa.responsaveis.indexOf(dadosUsuario?.usuario?.id) > -1" name="bookmark" item-end></ion-icon>\n                <p>{{ tarefa.descricao }}</p>\n                <p>Data Limite para conclusão: {{ tarefa?.dataConclusao}}</p>\n\n            </ion-item>\n            <ion-item-options side="right">\n                <!--<button ion-button color="primary" [navPush]="tarefaPage" [navParams]="{mode: \'edit\', tarefa: tarefa, usuario: dadosUsuario}">-->\n                    <!--<ion-icon name="create"></ion-icon>-->\n                    <!--Editar-->\n                <!--</button>-->\n                <button ion-button color="secondary" [navPush]="avaliarTarefaPage" [navParams]="{tarefa: tarefa, usuario: dadosUsuario}">\n                    <ion-icon name="star"></ion-icon>\n                    Avaliar\n                </button>\n                <button ion-button color="danger" (click)="excluirTarefa(tarefa)">\n                    <ion-icon name="trash"></ion-icon>\n                    Excluir\n                </button>\n            </ion-item-options>\n        </ion-item-sliding>\n    </ion-list>\n    <ion-infinite-scroll (ionInfinite)="scrollVagas($event)" [enabled]="scrollEnabled">\n        <ion-infinite-scroll-content></ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n    <ion-fab right bottom>\n        <button ion-fab color="orange" [navPush]="tarefaPage" [navParams]="{mode: \'create\', usuario: dadosUsuario}">\n            <ion-icon name="add"></ion-icon>\n        </button>\n    </ion-fab>\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/tarefa-list/tarefa-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_5__providers_tarefa_tarefa__["a" /* TarefaProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_7__providers_vars_vars__["a" /* VarsProvider */]])
    ], TarefaListPage);
    return TarefaListPage;
}());

//# sourceMappingURL=tarefa-list.js.map

/***/ }),

/***/ 64:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VagaListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_vaga_vaga__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__vaga_modal_vaga_modal__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__vaga_vaga__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_vars_vars__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var VagaListPage = /** @class */ (function () {
    function VagaListPage(navCtrl, navParams, vagaProvider, loadingCtrl, alertCtrl, storage, geolocation, modalCtrl, varsProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.vagaProvider = vagaProvider;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.geolocation = geolocation;
        this.modalCtrl = modalCtrl;
        this.varsProvider = varsProvider;
        this.vagaModalPage = __WEBPACK_IMPORTED_MODULE_7__vaga_modal_vaga_modal__["a" /* VagaModalPage */];
        this.vagas = [];
        this.vagaPage = __WEBPACK_IMPORTED_MODULE_8__vaga_vaga__["a" /* VagaPage */];
        this.search = false;
        this.searching = false;
        this.pageSearch = 0;
        this.hasNextPageSearch = false;
        this.nomeSearch = '';
        this.cidadeSearch = '';
        this.estadoSearch = '';
        this.scrollEnabled = true;
        this.dadosUsuario = this.varsProvider.getUsuario();
        if (this.dadosUsuario) {
            this.vagaProvider.setHeaderToken(this.dadosUsuario['accessToken']);
        }
        else {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
            this.navCtrl.popToRoot();
        }
    }
    VagaListPage.prototype.toggleSearchBar = function () {
        this.search = !this.search;
        if (this.search)
            this.content.scrollToTop();
    };
    VagaListPage.prototype.onInput = function (event) {
        this.nomeSearch = event.target.value;
        this.pageSearch = 0;
        this.hasNextPageSearch = false;
        this.scrollEnabled = true;
        this.getVagas({
            nome: this.nomeSearch,
            cidade: this.cidadeSearch,
            estado: this.estadoSearch
        }, 0);
    };
    VagaListPage.prototype.showModal = function (vaga) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__vaga_modal_vaga_modal__["a" /* VagaModalPage */], { vaga: vaga, usuario: this.dadosUsuario });
        modal.present();
    };
    VagaListPage.prototype.getVagas = function (_a, page) {
        var _this = this;
        var nome = _a.nome, cidade = _a.cidade, estado = _a.estado;
        this.searching = true;
        if (!this.loader) {
            this.loader = this.loadingCtrl.create({
                content: 'Por favor, aguarde.'
            });
            this.loader.present();
        }
        if (this.pageSearch == 0)
            this.vagas = [];
        this.vagaProvider.getVagas({
            nome: (nome ? nome : ''),
            cidade: (cidade ? cidade : ''),
            estado: (estado ? estado : '')
        }, page).subscribe(function (response) {
            console.log(response);
            _this.searching = false;
            // this.pageSearch = response['data']['page'];
            // this.totalPages = response['data']['totalPages'];
            // this.hasNextPageSearch = !response['data']['last'];
            response['data']['content'].forEach(function (item) {
                _this.vagas.push(item);
            });
            _this.loader.dismiss();
        }, function (error) {
            _this.searching = false;
            _this.loader.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Mensagem',
                subTitle: 'Não foi possível consultar as vagas.',
                buttons: ['OK']
            });
            alert.present();
        });
    };
    VagaListPage.prototype.scrollVagas = function (infiniteScroll) {
        var _this = this;
        setTimeout(function () {
            if (_this.hasNextPageSearch) {
                _this.pageSearch += 1;
                _this.getVagas({
                    nome: _this.nomeSearch,
                    cidade: _this.cidadeSearch,
                    estado: _this.estadoSearch
                }, _this.pageSearch);
            }
            if (_this.pageSearch + 1 >= _this.totalPages) {
                _this.scrollEnabled = false;
                // infiniteScroll.enabled = false;
            }
            infiniteScroll.complete();
        }, 500);
    };
    VagaListPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.searching = true;
        this.loader = this.loadingCtrl.create({
            content: 'Por favor, aguarde.'
        });
        this.loader.present();
        this.geolocation.getCurrentPosition().then(function (resp) {
            var req = {
                position: {
                    lat: resp.coords.latitude,
                    lng: resp.coords.longitude
                }
            };
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["b" /* Geocoder */].geocode(req).then(function (results) {
                if (results) {
                    _this.cidadeSearch = results[0]['subAdminArea'];
                    _this.estadoSearch = results[0]['adminArea'];
                    switch (_this.estadoSearch) {
                        case 'Minas Gerais':
                            _this.estadoSearch = 'MG';
                            break;
                        case 'São Paulo':
                            _this.estadoSearch = 'SP';
                            break;
                        case 'Rio de Janeiro':
                            _this.estadoSearch = 'RJ';
                            break;
                        case 'Espírito Santo':
                            _this.estadoSearch = 'ES';
                            break;
                        case 'Bahia':
                            _this.estadoSearch = 'BA';
                            break;
                        case 'Rio Grande do Sul':
                            _this.estadoSearch = 'RS';
                            break;
                        case 'Rio Grande do Norte':
                            _this.estadoSearch = 'RN';
                            break;
                        case 'Santa Catarina':
                            _this.estadoSearch = 'SC';
                            break;
                        case 'Paraná':
                            _this.estadoSearch = 'PR';
                            break;
                        case 'Pará':
                            _this.estadoSearch = 'PA';
                            break;
                        case 'Piauí':
                            _this.estadoSearch = 'PI';
                            break;
                        case 'Pernambuco':
                            _this.estadoSearch = 'PE';
                            break;
                        case 'Sergipe':
                            _this.estadoSearch = 'SE';
                            break;
                        case 'Acre':
                            _this.estadoSearch = 'AC';
                            break;
                        case 'Alagoas':
                            _this.estadoSearch = 'AL';
                            break;
                        case 'Amapá':
                            _this.estadoSearch = 'AP';
                            break;
                        case 'Amazonas':
                            _this.estadoSearch = 'AM';
                            break;
                        case 'Ceará':
                            _this.estadoSearch = 'CE';
                            break;
                        case 'Distrito Federal':
                            _this.estadoSearch = 'DF';
                            break;
                        case 'Goiás':
                            _this.estadoSearch = 'GO';
                            break;
                        case 'Maranhão':
                            _this.estadoSearch = 'MA';
                            break;
                        case 'Mato Grosso':
                            _this.estadoSearch = 'MT';
                            break;
                        case 'Mato Grosso do Sul':
                            _this.estadoSearch = 'MS';
                            break;
                        case 'Paraíba':
                            _this.estadoSearch = 'PB';
                            break;
                        case 'Rondônia':
                            _this.estadoSearch = 'RO';
                            break;
                        case 'Roraima':
                            _this.estadoSearch = 'RR';
                            break;
                        case 'Tocantins':
                            _this.estadoSearch = 'TO';
                            break;
                        default:
                            _this.estadoSearch = '';
                            break;
                    }
                    _this.getVagas({
                        nome: '',
                        cidade: _this.cidadeSearch,
                        estado: _this.estadoSearch
                    }, 0);
                }
                else {
                    _this.cidadeSearch = '';
                    _this.estadoSearch = '';
                }
            });
        }).catch(function (error) {
            _this.loader.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Aviso',
                subTitle: 'Não foi possível determinar sua localização. Utilize os filtros.',
                buttons: ['OK']
            });
            alert.present();
            _this.getVagas({
                nome: '',
                cidade: _this.cidadeSearch,
                estado: _this.estadoSearch
            }, 0);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], VagaListPage.prototype, "content", void 0);
    VagaListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-vaga-list',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/vaga-list/vaga-list.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu" class="headerButton"></ion-icon>\n        </button>\n        <ion-title>Vagas</ion-title>\n        <ion-buttons end>\n            <button ion-button (click)="toggleSearchBar()">\n                <ion-icon name="search" class="headerButton"></ion-icon>\n            </button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <ion-searchbar *ngIf="search" debounce="1000"\n                   [(ngModel)]="myInput"\n                   [showCancelButton]="shouldShowCancel"\n                   (ionInput)="onInput($event)"\n                   (ionCancel)="onCancel($event)">\n    </ion-searchbar>\n    <h3 *ngIf="!searching && vagas.length === 0" padding>\n        Nenhuma vaga.\n    </h3>\n    <ion-list>\n        <ion-item-sliding *ngFor="let vaga of vagas" >\n            <ion-item (click)="showModal(vaga)">\n                <h4>{{ vaga.descricao }}</h4>\n                <p>{{ vaga.moradia.nome }}. {{ vaga.moradia.endereco }}, {{ vaga.moradia.numero }} {{ vaga.moradia.cidade }} - {{ vaga.moradia.estado }}</p>\n            </ion-item>\n            <ion-item-options side="right">\n                <button ion-button color="primaty" (click)="showModal(vaga)">\n                    <ion-icon name="information-circle"></ion-icon>\n                    Detalhes\n                </button>\n            </ion-item-options>\n            <ion-item-options side="right">\n                <button ion-button color="primaty" (click)="showModal(vaga)">\n                    <ion-icon name="information-circle"></ion-icon>\n                    Detalhes\n                </button>\n            </ion-item-options>\n        </ion-item-sliding>\n    </ion-list>\n    <ion-infinite-scroll (ionInfinite)="scrollVagas($event)" [enabled]="scrollEnabled">\n        <ion-infinite-scroll-content></ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n    <ion-fab *ngIf="dadosUsuario?.usuario.moradia !== null" right bottom>\n        <button ion-fab color="orange" [navPush]="vagaPage" [navParams]="{mode: \'create\', usuario: dadosUsuario}">\n            <ion-icon name="add"></ion-icon>\n        </button>\n    </ion-fab>\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/vaga-list/vaga-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_vaga_vaga__["a" /* VagaProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */], __WEBPACK_IMPORTED_MODULE_9__providers_vars_vars__["a" /* VarsProvider */]])
    ], VagaListPage);
    return VagaListPage;
}());

//# sourceMappingURL=vaga-list.js.map

/***/ }),

/***/ 65:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificacoesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_vars_vars__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_moradia_moradia__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_usuario_usuario__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NotificacoesPage = /** @class */ (function () {
    function NotificacoesPage(navCtrl, navParams, varsProvider, moradiaProvider, loadingCtrl, usuarioProvider, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.varsProvider = varsProvider;
        this.moradiaProvider = moradiaProvider;
        this.loadingCtrl = loadingCtrl;
        this.usuarioProvider = usuarioProvider;
        this.alertCtrl = alertCtrl;
        this.notificacoes = [];
        this.searching = false;
        this.dadosUsuario = this.varsProvider.getUsuario();
        this.moradiaProvider.setHeaderToken(this.dadosUsuario['accessToken']);
    }
    NotificacoesPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.searching = true;
        var loader = this.loadingCtrl.create({
            content: 'Por favor, aguarde.'
        });
        loader.present();
        this.moradiaProvider.listarSolicitacoesIngressoMoradia(this.dadosUsuario.usuario.moradia.id).subscribe(function (response) {
            _this.searching = false;
            loader.dismiss();
            if (response['meta']['messageCode'] === "msg.servico.execucao.sucesso") {
                response['data']['solicitacoes'].forEach(function (item) {
                    _this.notificacoes.push(item);
                });
            }
            else {
            }
        }, function (error) {
            _this.searching = false;
        });
    };
    NotificacoesPage.prototype.aceitarSolicitacao = function (notificacao) {
        var _this = this;
        console.log(notificacao);
        var alert = this.alertCtrl.create({
            title: 'Confirmação',
            subTitle: 'Deseja realmente aceitar a solicitação de ingresso do Usuário ' + notificacao['nomeUsuario'] + '?',
            buttons: [
                {
                    text: 'Sim',
                    handler: function () {
                        var loader = _this.loadingCtrl.create({
                            content: 'Por favor, aguarde.'
                        });
                        loader.present();
                        _this.moradiaProvider.addMembroAMoradia(notificacao['moradiaId'], notificacao['usuarioId']).subscribe(function (response) {
                            loader.dismiss();
                            var index = _this.notificacoes.indexOf(notificacao);
                            _this.notificacoes.splice(index, 1);
                            var alert = _this.alertCtrl.create({
                                title: 'Sucesso!',
                                subTitle: 'O usuário foi inserido na moradia.',
                                buttons: ['OK']
                            });
                            alert.present();
                        }, function (error) {
                            console.log(error);
                            loader.dismiss();
                            var index = _this.notificacoes.indexOf(notificacao);
                            _this.notificacoes.splice(index, 1);
                            var alert = _this.alertCtrl.create({
                                title: 'Aviso',
                                subTitle: 'Não foi possível realizar a operação.',
                                buttons: ['OK']
                            });
                            alert.present();
                        });
                    }
                },
                {
                    text: 'Não',
                    handler: function () { }
                }
            ]
        });
        alert.present();
    };
    NotificacoesPage.prototype.recusarSolicitacao = function (notificacao) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Confirmação',
            subTitle: 'Deseja realmente remover a solicitação de ingresso do Usuário ' + notificacao['nomeUsuario'] + '?',
            buttons: [
                {
                    text: 'Sim',
                    handler: function () {
                        var loader = _this.loadingCtrl.create({
                            content: 'Por favor, aguarde.'
                        });
                        loader.present();
                        _this.moradiaProvider.removerSolicitacao(notificacao['moradiaId'], notificacao['usuarioId']).subscribe(function (response) {
                            loader.dismiss();
                            var index = _this.notificacoes.indexOf(notificacao);
                            _this.notificacoes.splice(index, 1);
                            var alert = _this.alertCtrl.create({
                                title: 'Sucesso!',
                                subTitle: 'Solicitação de ingresso recusada.',
                                buttons: ['OK']
                            });
                            alert.present();
                        }, function (error) {
                            loader.dismiss();
                            var index = _this.notificacoes.indexOf(notificacao);
                            _this.notificacoes.splice(index, 1);
                            var alert = _this.alertCtrl.create({
                                title: 'Aviso',
                                subTitle: 'Não foi possível realizar a operação.',
                                buttons: ['OK']
                            });
                            alert.present();
                        });
                    }
                },
                {
                    text: 'Não',
                    handler: function () { }
                }
            ]
        });
        alert.present();
    };
    NotificacoesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-notificacoes',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/notificacoes/notificacoes.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu" class="headerButton"></ion-icon>\n        </button>\n        <ion-title>Notificações</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <h3 *ngIf="!searching && notificacoes.length === 0" padding>\n        Nenhuma notificação.\n    </h3>\n    <ion-list>\n        <ion-item-sliding *ngFor="let notificacao of notificacoes">\n            <ion-item>\n                <h4>{{ notificacao.nomeUsuario }}</h4>\n                <p>Solicitou Ingresso na Moradia</p>\n            </ion-item>\n            <ion-item-options side="right">\n                <button ion-button color="secondary" (click)="aceitarSolicitacao(notificacao)">\n                    <ion-icon name="checkmark"></ion-icon>\n                    Aceitar\n                </button>\n                <button ion-button color="danger" (click)="recusarSolicitacao(notificacao)">\n                    <ion-icon name="close"></ion-icon>\n                    Recusar\n                </button>\n            </ion-item-options>\n        </ion-item-sliding>\n    </ion-list>\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/notificacoes/notificacoes.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_vars_vars__["a" /* VarsProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_moradia_moradia__["a" /* MoradiaProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_usuario_usuario__["a" /* UsuarioProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], NotificacoesPage);
    return NotificacoesPage;
}());

//# sourceMappingURL=notificacoes.js.map

/***/ }),

/***/ 66:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventoListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__evento_evento__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__evento_modal_evento_modal__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_evento_evento__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_network__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__login_login__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_google_maps__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_vars_vars__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var EventoListPage = /** @class */ (function () {
    function EventoListPage(navCtrl, navParams, eventoProvider, loadingCtrl, network, toastCtrl, alertCtrl, storage, geolocation, varsProvider, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.eventoProvider = eventoProvider;
        this.loadingCtrl = loadingCtrl;
        this.network = network;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.geolocation = geolocation;
        this.varsProvider = varsProvider;
        this.modalCtrl = modalCtrl;
        this.eventoModalPage = __WEBPACK_IMPORTED_MODULE_3__evento_modal_evento_modal__["a" /* EventoModalPage */];
        this.eventos = [];
        this.eventoPage = __WEBPACK_IMPORTED_MODULE_2__evento_evento__["a" /* EventoPage */];
        this.search = false;
        this.searching = false;
        this.pageSearch = 0;
        this.hasNextPageSearch = false;
        this.nomeSearch = '';
        this.cidadeSearch = '';
        this.estadoSearch = '';
        this.scrollEnabled = true;
        this.dadosUsuario = this.varsProvider.getUsuario();
        if (this.dadosUsuario) {
            this.eventoProvider.setHeaderToken(this.dadosUsuario['accessToken']);
        }
        else {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__login_login__["a" /* LoginPage */]);
            this.navCtrl.popToRoot();
        }
    }
    EventoListPage.prototype.toggleSearchBar = function () {
        this.search = !this.search;
        if (this.search)
            this.content.scrollToTop();
    };
    EventoListPage.prototype.onInput = function (event) {
        this.nomeSearch = event.target.value;
        this.pageSearch = 0;
        this.hasNextPageSearch = false;
        this.scrollEnabled = true;
        this.getEventos({
            nome: this.nomeSearch,
            cidade: this.cidadeSearch,
            estado: this.estadoSearch
        }, 0);
    };
    EventoListPage.prototype.getEventos = function (_a, page) {
        var _this = this;
        var nome = _a.nome, cidade = _a.cidade, estado = _a.estado;
        this.searching = true;
        if (!this.loader) {
            this.loader = this.loadingCtrl.create({
                content: 'Por favor, aguarde.'
            });
            this.loader.present();
        }
        if (this.pageSearch == 0)
            this.eventos = [];
        this.eventoProvider.getEventos({
            nome: (nome ? nome : ''),
            cidade: (cidade ? cidade : ''),
            estado: (estado ? estado : '')
        }, page).subscribe(function (response) {
            _this.searching = false;
            _this.loader.dismiss();
            console.log(response);
            _this.pageSearch = response['data']['page'];
            _this.totalPages = response['data']['totalPages'];
            _this.hasNextPageSearch = !response['data']['last'];
            response['data']['content'].forEach(function (item) {
                var dataRealizacaoStr = '';
                dataRealizacaoStr = item['dataRealizacao'].split('T')[0].split('-')[2] + '/' + item['dataRealizacao'].split('T')[0].split('-')[1] + '/' + item['dataRealizacao'].split('T')[0].split('-')[0] + ' às ' + item['dataRealizacao'].split('T')[1].split('.')[0].split(':')[0] + ':' + item['dataRealizacao'].split('T')[1].split('.')[0].split(':')[1];
                item['dataRealizacao'] = dataRealizacaoStr;
                _this.eventos.push(item);
            });
        }, function (error) {
            _this.searching = false;
            _this.loader.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Mensagem',
                subTitle: 'Não foi possível consultar os eventos.',
                buttons: ['OK']
            });
            alert.present();
        });
    };
    EventoListPage.prototype.scrollEventos = function (infiniteScroll) {
        var _this = this;
        setTimeout(function () {
            if (_this.hasNextPageSearch) {
                _this.pageSearch += 1;
                _this.getEventos({
                    nome: _this.nomeSearch,
                    cidade: _this.cidadeSearch,
                    estado: _this.estadoSearch
                }, _this.pageSearch);
            }
            if (_this.pageSearch + 1 >= _this.totalPages) {
                _this.scrollEnabled = false;
                // infiniteScroll.enabled = false;
            }
            infiniteScroll.complete();
        }, 500);
    };
    EventoListPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.searching = true;
        this.loader = this.loadingCtrl.create({
            content: 'Por favor, aguarde'
        });
        this.loader.present();
        this.geolocation.getCurrentPosition().then(function (resp) {
            var req = {
                position: {
                    lat: resp.coords.latitude,
                    lng: resp.coords.longitude
                }
            };
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_google_maps__["b" /* Geocoder */].geocode(req).then(function (results) {
                if (results) {
                    _this.cidadeSearch = results[0]['subAdminArea'];
                    _this.estadoSearch = results[0]['adminArea'];
                    switch (_this.estadoSearch) {
                        case 'Minas Gerais':
                            _this.estadoSearch = 'MG';
                            break;
                        case 'São Paulo':
                            _this.estadoSearch = 'SP';
                            break;
                        case 'Rio de Janeiro':
                            _this.estadoSearch = 'RJ';
                            break;
                        case 'Espírito Santo':
                            _this.estadoSearch = 'ES';
                            break;
                        case 'Bahia':
                            _this.estadoSearch = 'BA';
                            break;
                        case 'Rio Grande do Sul':
                            _this.estadoSearch = 'RS';
                            break;
                        case 'Rio Grande do Norte':
                            _this.estadoSearch = 'RN';
                            break;
                        case 'Santa Catarina':
                            _this.estadoSearch = 'SC';
                            break;
                        case 'Paraná':
                            _this.estadoSearch = 'PR';
                            break;
                        case 'Pará':
                            _this.estadoSearch = 'PA';
                            break;
                        case 'Piauí':
                            _this.estadoSearch = 'PI';
                            break;
                        case 'Pernambuco':
                            _this.estadoSearch = 'PE';
                            break;
                        case 'Sergipe':
                            _this.estadoSearch = 'SE';
                            break;
                        case 'Acre':
                            _this.estadoSearch = 'AC';
                            break;
                        case 'Alagoas':
                            _this.estadoSearch = 'AL';
                            break;
                        case 'Amapá':
                            _this.estadoSearch = 'AP';
                            break;
                        case 'Amazonas':
                            _this.estadoSearch = 'AM';
                            break;
                        case 'Ceará':
                            _this.estadoSearch = 'CE';
                            break;
                        case 'Distrito Federal':
                            _this.estadoSearch = 'DF';
                            break;
                        case 'Goiás':
                            _this.estadoSearch = 'GO';
                            break;
                        case 'Maranhão':
                            _this.estadoSearch = 'MA';
                            break;
                        case 'Mato Grosso':
                            _this.estadoSearch = 'MT';
                            break;
                        case 'Mato Grosso do Sul':
                            _this.estadoSearch = 'MS';
                            break;
                        case 'Paraíba':
                            _this.estadoSearch = 'PB';
                            break;
                        case 'Rondônia':
                            _this.estadoSearch = 'RO';
                            break;
                        case 'Roraima':
                            _this.estadoSearch = 'RR';
                            break;
                        case 'Tocantins':
                            _this.estadoSearch = 'TO';
                            break;
                        default:
                            _this.estadoSearch = '';
                            break;
                    }
                    _this.getEventos({
                        nome: '',
                        cidade: _this.cidadeSearch,
                        estado: _this.estadoSearch
                    }, 0);
                }
                else {
                    _this.cidadeSearch = '';
                    _this.estadoSearch = '';
                }
            });
        }).catch(function (error) {
            _this.loader.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Aviso',
                subTitle: 'Não foi possível determinar sua localização. Utilize os filtros.',
                buttons: ['OK']
            });
            alert.present();
            _this.getEventos({
                nome: '',
                cidade: _this.cidadeSearch,
                estado: _this.estadoSearch
            }, 0);
        });
    };
    EventoListPage.prototype.showModal = function (evento) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__evento_modal_evento_modal__["a" /* EventoModalPage */], { evento: evento });
        modal.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], EventoListPage.prototype, "content", void 0);
    EventoListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-evento-list',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/evento-list/evento-list.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu" class="headerButton"></ion-icon>\n        </button>\n        <ion-title>Eventos</ion-title>\n        <ion-buttons end>\n            <button ion-button (click)="toggleSearchBar()">\n                <ion-icon name="search" class="headerButton"></ion-icon>\n            </button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <ion-searchbar *ngIf="search" debounce="1000"\n                   [(ngModel)]="myInput"\n                   [showCancelButton]="shouldShowCancel"\n                   (ionInput)="onInput($event)"\n                   (ionCancel)="onCancel($event)">\n    </ion-searchbar>\n    <h3 *ngIf="!searching && eventos.length === 0" padding>\n        Nenhum evento.\n    </h3>\n    <ion-list>\n        <ion-item-sliding *ngFor="let evento of eventos" >\n            <ion-item (click)="showModal(evento)">\n                <h4>{{ evento.nome }}</h4>\n                <p>{{ evento.descricao }}</p>\n                <!--<p>{{ evento.dataRealizacao }}</p>-->\n                <!--<p>{{ evento.valorEntrada }}</p>-->\n                <!--<p>{{ evento.moradia.nome }} - {{ evento.moradia.endereco }}, {{ evento.moradia.numero }}, {{ evento.moradia.cidade }} - {{ evento.moradia.estado }}</p>-->\n            </ion-item>\n            <ion-item-options side="right">\n                <button ion-button color="primary" (click)="showModal(evento)">\n                    <ion-icon name="information-circle"></ion-icon>\n                    Detalhes\n                </button>\n            </ion-item-options>\n        </ion-item-sliding>\n    </ion-list>\n    <ion-infinite-scroll (ionInfinite)="scrollEventos($event)" [enabled]="scrollEnabled">\n        <ion-infinite-scroll-content></ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n    <!--<ion-fab *ngIf="dadosUsuario?.usuario.moradia !== null" right bottom>-->\n    <!--<button ion-fab color="orange" [navPush]="moradiaPage" [navParams]="{mode: \'create\'}">-->\n    <!--<ion-icon name="add"></ion-icon>-->\n    <!--</button>-->\n    <!--</ion-fab>-->\n    <ion-fab right bottom>\n        <button ion-fab color="orange" [navPush]="eventoPage" [navParams]="{mode: \'create\'}">\n            <ion-icon name="add"></ion-icon>\n        </button>\n    </ion-fab>\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/evento-list/evento-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__providers_evento_evento__["a" /* EventoProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_network__["a" /* Network */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_10__providers_vars_vars__["a" /* VarsProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], EventoListPage);
    return EventoListPage;
}());

//# sourceMappingURL=evento-list.js.map

/***/ }),

/***/ 79:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventoProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EventoProvider = /** @class */ (function () {
    function EventoProvider(http) {
        this.http = http;
        this.url = 'http://167.250.199.228:8080/api/eventos';
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({
                'Content-Type': 'application/json'
            }),
            observe: 'response'
        };
        this.accessToken = '';
    }
    EventoProvider.prototype.setHeaderToken = function (token) {
        this.accessToken = token;
    };
    EventoProvider.prototype.getEventos = function (_a, page) {
        var nome = _a.nome, cidade = _a.cidade, estado = _a.estado;
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        var params = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpParams */]();
        if (nome)
            params = params.append('nome', nome);
        if (cidade)
            params = params.append('cidade', cidade);
        if (estado)
            params = params.append('estado', estado);
        params = params.append('page', page);
        params = params.append('size', '10');
        return this.http.get(this.url, __assign({}, this.httpOptions, { params: params })).map(function (response) { return response['body']; });
    };
    EventoProvider.prototype.getEvento = function (nome) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        var params = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpParams */]();
        params = params.append('nome', nome);
        params = params.append('page', '0');
        params = params.append('size', '10');
        return this.http.get(this.url, __assign({}, this.httpOptions, { params: params })).map(function (response) { return response['body']; });
    };
    EventoProvider.prototype.addEvento = function (evento) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.post(this.url, evento, this.httpOptions).map(function (response) { return response['body']; });
    };
    EventoProvider.prototype.updateEvento = function (evento, storageData) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.put(this.url, evento, this.httpOptions).map(function (response) { return response['body']; });
    };
    EventoProvider.prototype.deleteEvento = function (evento) {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.delete(this.url, this.httpOptions).map(function (response) { return response['body']; });
    };
    EventoProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], EventoProvider);
    return EventoProvider;
}());

//# sourceMappingURL=evento.js.map

/***/ }),

/***/ 84:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AvaliarTarefaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_vars_vars__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_moradia_moradia__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_tarefa_tarefa__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AvaliarTarefaPage = /** @class */ (function () {
    function AvaliarTarefaPage(navCtrl, navParams, varsProvider, alertCtrl, loadingCtrl, moradiaProvider, tarefaProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.varsProvider = varsProvider;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.moradiaProvider = moradiaProvider;
        this.tarefaProvider = tarefaProvider;
        this.nota = 0;
        this.dadosUsuario = null;
        this.responsaveis = [];
        this.responsaveisStr = '';
        this.tarefa = this.navParams.get('tarefa');
        this.dadosUsuario = this.varsProvider.getUsuario();
        if (this.dadosUsuario) {
            this.moradiaProvider.setHeaderToken(this.dadosUsuario['accessToken']);
            this.tarefaProvider.setHeaderToken(this.dadosUsuario['accessToken']);
        }
        else {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]);
            this.navCtrl.popToRoot();
        }
    }
    AvaliarTarefaPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: 'Por favor, aguarde.'
        });
        loader.present();
        this.moradiaProvider.getMembrosDaMoradia(this.dadosUsuario['usuario']['moradia']['id']).subscribe(function (response) {
            console.log(response);
            if (response['meta']['messageCode'] === "msg.servico.execucao.sucesso") {
                loader.dismiss();
                var nomeResponsaveis_1 = [];
                response['data']['usuarios'].forEach(function (item) {
                    if (_this.tarefa.responsaveis.indexOf(item.id) > -1)
                        nomeResponsaveis_1.push(item.nome);
                });
                _this.responsaveisStr = nomeResponsaveis_1.join(', ');
            }
            else {
            }
        }, function (error) {
            console.log(error);
            loader.dismiss();
        });
    };
    AvaliarTarefaPage.prototype.submit = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: 'Por favor, aguarde.'
        });
        loader.present();
        this.tarefaProvider.avaliarTarefa({
            // idResponsavel: this.dadosUsuario['usuario']['id'],
            idTarefa: this.tarefa.id,
            qualidadeTarefa: this.nota
        }).subscribe(function (response) {
            console.log(response);
            loader.dismiss();
            if (response['meta']['messageCode'] === "msg.tarefa.concluida") {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Tarefa Avaliada!',
                    subTitle: 'A tarefa foi avaliada com sucesso.',
                    buttons: [
                        {
                            text: 'OK',
                            handler: function () {
                                _this.navCtrl.pop();
                            }
                        }
                    ]
                });
                alert_1.present();
            }
        }, function (error) {
            console.log(error);
            loader.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Aviso',
                subTitle: 'Não foi possível realizar a operação.',
                buttons: [
                    {
                        text: 'OK',
                        handler: function () {
                            _this.navCtrl.pop();
                        }
                    }
                ]
            });
            alert.present();
        });
    };
    AvaliarTarefaPage.prototype.onChange = function (event) {
        console.log(event);
    };
    AvaliarTarefaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-avaliar-tarefa',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/avaliar-tarefa/avaliar-tarefa.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>Avaliar Tarefa</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <div class="container">\n        <ion-grid>\n            <ion-row>\n                <ion-item>\n                    <h2 left>Nome</h2>\n                    <p>{{ tarefa.nome }}</p>\n                </ion-item>\n            </ion-row>\n            <ion-row>\n                <ion-item>\n                    <h2 left>Responsáveis</h2>\n                    <p>{{ responsaveisStr }}</p>\n                </ion-item>\n            </ion-row>\n            <ion-row>\n                <ion-item>\n                    <ion-label>Nota</ion-label>\n                    <ion-range [(ngModel)]="nota" min="0" max="5" step="1" pin="true" (ionChange)="onChange($event)">\n                    </ion-range>\n                </ion-item>\n            </ion-row>\n            <div padding>\n                <button ion-button btn-block color="submitButton" (click)="submit()" block>\n                    Avaliar\n                </button>\n            </div>\n        </ion-grid>\n    </div>\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/avaliar-tarefa/avaliar-tarefa.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_vars_vars__["a" /* VarsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_moradia_moradia__["a" /* MoradiaProvider */], __WEBPACK_IMPORTED_MODULE_4__providers_tarefa_tarefa__["a" /* TarefaProvider */]])
    ], AvaliarTarefaPage);
    return AvaliarTarefaPage;
}());

//# sourceMappingURL=avaliar-tarefa.js.map

/***/ }),

/***/ 85:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VagaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_vaga_vaga__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__home_home__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_vars_vars__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var VagaPage = /** @class */ (function () {
    function VagaPage(navCtrl, navParams, loadingCtrl, formBuilder, vagaProvider, alertCtrl, storage, varsProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.vagaProvider = vagaProvider;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.varsProvider = varsProvider;
        if (this.navParams.get('mode') === 'edit')
            this.vaga = this.navParams.get('vaga');
        else
            this.vaga = null;
        this.dadosUsuario = this.varsProvider.getUsuario();
        if (this.dadosUsuario) {
            this.vagaProvider.setHeaderToken(this.dadosUsuario.accessToken);
        }
        else {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* LoginPage */]);
            this.navCtrl.popToRoot();
        }
        var nomeResponsavel = (this.dadosUsuario ? this.dadosUsuario.usuario.nome : '');
        var qtdQuartos = (this.vaga ? this.vaga.qtdQuartos : '');
        var descricao = (this.vaga ? this.vaga.descricao : '');
        var publicoAlvo = (this.vaga ? this.vaga.publicoAlvo : 'todos');
        var observacoes = (this.vaga ? this.vaga.observacoes : '');
        this.vagaForm = this.formBuilder.group({
            nomeResponsavel: [
                nomeResponsavel,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])
            ],
            qtdQuartos: [
                qtdQuartos,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].min(1),
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].max(10),
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required
                ])
            ],
            descricao: [
                descricao,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].minLength(1),
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(200),
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required
                ])
            ],
            publicoAlvo: [
                publicoAlvo,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])
            ],
            observacoes: [
                observacoes,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(200)])
            ],
        });
    }
    VagaPage.prototype.onSubmit = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Por favor, aguarde."
        });
        loader.present();
        var data = {
            criadorId: this.dadosUsuario.usuario.id,
            descricao: this.vagaForm.value.descricao,
            qtdQuartos: this.vagaForm.value.qtdQuartos,
            publicoAlvo: this.vagaForm.value.publicoAlvo,
            moradiaId: this.dadosUsuario.usuario.moradia.id,
            observacoes: this.vagaForm.value.observacoes
        };
        this.vagaProvider.addVaga(data).subscribe(function (response) {
            if (response['meta']['messageCode'] === "msg.anunciovaga.registrado") {
                loader.dismiss();
                var alert_1 = _this.alertCtrl.create({
                    title: 'Vaga Criada!',
                    subTitle: 'Vaga criada com sucesso.',
                    buttons: [
                        {
                            text: 'OK',
                            handler: function () {
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__home_home__["a" /* HomePage */]);
                                _this.navCtrl.popToRoot();
                            }
                        }
                    ]
                });
                alert_1.present();
            }
            else {
                var alert_2 = _this.alertCtrl.create({
                    title: 'Aviso',
                    subTitle: 'Mensagem: ' + response['meta']['messageCode'],
                    buttons: ['OK']
                });
                alert_2.present();
            }
        }, function (error) {
            loader.dismiss();
            if (error['meta']['messageCode'] === "msg.apelido.ja.utilizado") {
                var alert_3 = _this.alertCtrl.create({
                    title: 'Aviso',
                    subTitle: 'Já existe uma Moradia com este nome.',
                    buttons: ['OK']
                });
                alert_3.present();
            }
            else {
                var alert_4 = _this.alertCtrl.create({
                    title: 'Aviso',
                    subTitle: 'Não foi possível criar a vaga.',
                    buttons: ['OK']
                });
                alert_4.present();
            }
        });
    };
    VagaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-vaga',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/vaga/vaga.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>Vaga</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <div class="container">\n        <ion-grid>\n            <form [formGroup]="vagaForm" (ngSubmit)="onSubmit()">\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Nome do Responsável</ion-label>\n                        <ion-input type="text" formControlName="nomeResponsavel" [readonly]="true"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Quantidade de quartos disponíveis</ion-label>\n                        <ion-input type="number" formControlName="qtdQuartos"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Descrição do(s) quarto(s)</ion-label>\n                        <ion-input type="text" formControlName="descricao"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-col col-12>\n                        <ion-list>\n                            <ion-item>\n                                <ion-label floating>Público Alvo</ion-label>\n                                <ion-select formControlName="publicoAlvo">\n                                    <ion-option value="todos">Todos</ion-option>\n                                    <ion-option value="homens">Homens</ion-option>\n                                    <ion-option value="mulheres">Mulheres</ion-option>\n                                </ion-select>\n                            </ion-item>\n                        </ion-list>\n                    </ion-col>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Observações</ion-label>\n                        <ion-textarea formControlName="observacoes"></ion-textarea>\n                    </ion-item>\n                </ion-row>\n                <div padding>\n                    <button ion-button btn-block color="submitButton" type="submit"\n                            block [disabled]="!vagaForm.valid">\n                        Cadastrar Vaga\n                    </button>\n                </div>\n            </form>\n        </ion-grid>\n    </div>\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/vaga/vaga.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_5__providers_vaga_vaga__["a" /* VagaProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_7__providers_vars_vars__["a" /* VarsProvider */]])
    ], VagaPage);
    return VagaPage;
}());

//# sourceMappingURL=vaga.js.map

/***/ }),

/***/ 86:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TarefaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__tarefa_list_tarefa_list__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_tarefa_tarefa__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_moradia_moradia__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_vars_vars__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var TarefaPage = /** @class */ (function () {
    function TarefaPage(navCtrl, navParams, loadingCtrl, formBuilder, tarefaProvider, moradiaProvider, alertCtrl, storage, varsProvider) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.tarefaProvider = tarefaProvider;
        this.moradiaProvider = moradiaProvider;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.varsProvider = varsProvider;
        this.membrosMoradia = [];
        if (this.navParams.get('mode') === 'edit')
            this.tarefa = this.navParams.get('tarefa');
        else
            this.tarefa = null;
        this.dadosUsuario = this.varsProvider.getUsuario();
        if (this.dadosUsuario) {
            this.tarefaProvider.setHeaderToken(this.dadosUsuario['accessToken']);
        }
        else {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* LoginPage */]);
            this.navCtrl.popToRoot();
        }
        var nome = (this.tarefa ? this.tarefa.nome : '');
        var descricao = (this.tarefa ? this.tarefa.descricao : '');
        var tipo = (this.tarefa ? this.tarefa.tipo : '');
        var dataConclusao = (this.tarefa ? this.tarefa.dataConclusao : new Date().toISOString());
        var responsaveis = (this.tarefa ? this.tarefa.responsaveis : '');
        var moradiaId = (this.tarefa ? this.tarefa.moradiaId : this.dadosUsuario.usuario.id);
        this.tarefaForm = this.formBuilder.group({
            nome: [
                nome,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].minLength(1),
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(60),
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required
                ])
            ],
            descricao: [
                descricao,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].minLength(1),
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(200),
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required
                ])
            ],
            tipo: [
                tipo,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])
            ],
            dataConclusao: [
                dataConclusao,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])
            ],
            responsaveis: [responsaveis, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
        });
        this.selectOptions = {
            title: 'Atribuir a Membros',
            subTitle: 'Escolha para quais membros do grupo a tarefa será atribuída.',
            mode: 'md'
        };
        this.moradiaProvider.getMembrosDaMoradia(this.dadosUsuario['usuario']['moradia']['id']).subscribe(function (response) {
            if (response['meta']['messageCode'] === "msg.servico.execucao.sucesso") {
                response['data']['usuarios'].forEach(function (item) {
                    _this.membrosMoradia.push({
                        id: item.id,
                        nome: item.nome,
                    });
                });
            }
            else {
            }
        }, function (error) {
        });
    }
    TarefaPage.prototype.onSubmit = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Por favor, aguarde.",
        });
        loader.present();
        var dadosTarefa = {
            nome: this.tarefaForm.value.nome,
            descricao: this.tarefaForm.value.descricao,
            tipo: this.tarefaForm.value.tipo,
            dataConclusao: this.tarefaForm.value.dataConclusao,
            moradiaId: this.dadosUsuario['usuario']['moradia']['id'],
            responsaveis: this.tarefaForm.value.responsaveis
        };
        if (this.navParams.get('mode') === 'edit') {
            this.tarefaProvider.updateTarefa(dadosTarefa).subscribe(function (response) {
                console.log(response);
                if (response['meta']['messageCode'] === "msg.dataconclusao.invalida") {
                    loader.dismiss();
                    var alert_1 = _this.alertCtrl.create({
                        title: 'Aviso',
                        subTitle: 'A data de conclusão da tarefa é inválida. Selecione outra data maior que a data atual.',
                        buttons: ['OK']
                    });
                    alert_1.present();
                }
                else {
                    loader.dismiss();
                    var alert_2 = _this.alertCtrl.create({
                        title: 'Tarefa editada!',
                        subTitle: 'Tarefa editada com sucesso!',
                        buttons: [
                            {
                                text: 'OK',
                                handler: function () {
                                    _this.navCtrl.popTo(__WEBPACK_IMPORTED_MODULE_5__tarefa_list_tarefa_list__["a" /* TarefaListPage */]);
                                }
                            }
                        ]
                    });
                    alert_2.present();
                }
            }, function (error) {
                console.log(error);
                loader.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Operação incompleta',
                    subTitle: 'Não foi possível editar a tarefa.',
                    buttons: [
                        {
                            text: 'OK',
                            handler: function () {
                                _this.navCtrl.popTo(__WEBPACK_IMPORTED_MODULE_5__tarefa_list_tarefa_list__["a" /* TarefaListPage */]);
                            }
                        }
                    ]
                });
                alert.present();
            });
        }
        else {
            this.tarefaProvider.addTarefa(dadosTarefa).subscribe(function (response) {
                console.log(response);
                if (response['meta']['messageCode'] === "msg.dataconclusao.invalida") {
                    loader.dismiss();
                    var alert_3 = _this.alertCtrl.create({
                        title: 'Aviso',
                        subTitle: 'A data de conclusão da tarefa é inválida. Selecione outra data maior que a data atual.',
                        buttons: ['OK']
                    });
                    alert_3.present();
                }
                else {
                    loader.dismiss();
                    var alert_4 = _this.alertCtrl.create({
                        title: 'Tarefa Criada!',
                        subTitle: 'Tarefa Criada com sucesso!',
                        buttons: [
                            {
                                text: 'OK',
                                handler: function () {
                                    _this.navCtrl.popTo(__WEBPACK_IMPORTED_MODULE_5__tarefa_list_tarefa_list__["a" /* TarefaListPage */]);
                                }
                            }
                        ]
                    });
                    alert_4.present();
                }
            }, function (error) {
                console.log(error);
                // Se nao estiver conectado, salva no dispositivo para posteriormente
                // adicionar tarefa, quando possuir internet.
                _this.storage.get('tarefas').then(function (val) {
                    var tarefasStorage = [];
                    if (val == null) {
                        tarefasStorage = [
                            dadosTarefa
                        ];
                    }
                    else {
                        tarefasStorage = JSON.parse(val);
                        tarefasStorage.push(dadosTarefa);
                    }
                    _this.storage.set('tarefas', JSON.stringify(tarefasStorage));
                });
                loader.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Operação incompleta',
                    subTitle: 'Não foi possível criar a tarefa. A tarefa foi salva em seu dispositivo e será criada assim que o dispositivo conectar-se à Internet.',
                    buttons: [
                        {
                            text: 'OK',
                            handler: function () {
                                _this.navCtrl.popTo(__WEBPACK_IMPORTED_MODULE_5__tarefa_list_tarefa_list__["a" /* TarefaListPage */]);
                            }
                        }
                    ]
                });
                alert.present();
            });
        }
    };
    TarefaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tarefa',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/tarefa/tarefa.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>Tarefa</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <div class="container">\n        <ion-grid>\n            <form [formGroup]="tarefaForm" (ngSubmit)="onSubmit()">\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Nome</ion-label>\n                        <ion-input type="text" formControlName="nome"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Descrição</ion-label>\n                        <ion-input type="text" formControlName="descricao"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Tipo</ion-label>\n                        <ion-input type="text" formControlName="tipo"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Data de Conclusão</ion-label>\n                        <ion-datetime displayFormat="DD MMM YYYY" formControlName="dataConclusao"></ion-datetime>\n                    </ion-item>\n                </ion-row>\n\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Membros</ion-label>\n                        <ion-select multiple="true" [selectOptions]="selectOptions" formControlName="responsaveis">\n                            <ion-option *ngFor="let membro of membrosMoradia" [value]="membro.id">\n                                {{ membro.nome}}\n                            </ion-option>\n                        </ion-select>\n                    </ion-item>\n                </ion-row>\n                <div padding>\n                    <button ion-button btn-block *ngIf="!tarefa" color="submitButton" type="submit" [disabled]="!tarefaForm.valid"\n                            block>\n                        Cadastrar Tarefa\n                    </button>\n                    <button ion-button btn-block *ngIf="tarefa" color="submitButton" type="submit" [disabled]="!tarefaForm.valid"\n                            block>\n                        Editar Tarefa\n                    </button>\n                </div>\n\n            </form>\n        </ion-grid>\n    </div>\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/tarefa/tarefa.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_6__providers_tarefa_tarefa__["a" /* TarefaProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_moradia_moradia__["a" /* MoradiaProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_8__providers_vars_vars__["a" /* VarsProvider */]])
    ], TarefaPage);
    return TarefaPage;
}());

//# sourceMappingURL=tarefa.js.map

/***/ }),

/***/ 87:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoradiaListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__moradia_moradia__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_moradia_moradia__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__moradia_modal_moradia_modal__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__moradia_detail_moradia_detail__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_vars_vars__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var MoradiaListPage = /** @class */ (function () {
    function MoradiaListPage(navCtrl, navParams, moradiaProvider, loadingCtrl, alertCtrl, geolocation, varsProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.moradiaProvider = moradiaProvider;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.geolocation = geolocation;
        this.varsProvider = varsProvider;
        this.moradiaModalPage = __WEBPACK_IMPORTED_MODULE_6__moradia_modal_moradia_modal__["a" /* MoradiaModalPage */];
        this.moradiaPage = __WEBPACK_IMPORTED_MODULE_2__moradia_moradia__["a" /* MoradiaPage */];
        this.moradiaDetailPage = __WEBPACK_IMPORTED_MODULE_7__moradia_detail_moradia_detail__["a" /* MoradiaDetailPage */];
        this.moradias = [];
        this.search = false;
        this.searching = false;
        this.pageSearch = 0;
        this.hasNextPageSearch = false;
        this.nomeSearch = '';
        this.cidadeSearch = '';
        this.estadoSearch = '';
        this.scrollEnabled = true;
        this.dadosUsuario = this.varsProvider.getUsuario();
        this.moradiaProvider.setHeaderToken(this.dadosUsuario.accessToken);
    }
    MoradiaListPage.prototype.toggleSearchBar = function () {
        this.search = !this.search;
        if (this.search)
            this.content.scrollToTop();
    };
    MoradiaListPage.prototype.onInput = function (event) {
        this.nomeSearch = event.target.value;
        this.pageSearch = 0;
        this.hasNextPageSearch = false;
        this.scrollEnabled = true;
        this.getMoradias({
            nome: this.nomeSearch,
            cidade: this.cidadeSearch,
            estado: this.estadoSearch
        }, 0);
    };
    MoradiaListPage.prototype.getMoradias = function (_a, page) {
        var _this = this;
        var nome = _a.nome, cidade = _a.cidade, estado = _a.estado;
        this.searching = true;
        if (!this.loader) {
            this.loader = this.loadingCtrl.create({
                content: 'Por favor, aguarde.'
            });
            this.loader.present();
        }
        if (this.pageSearch == 0)
            this.moradias = [];
        this.moradiaProvider.getMoradias({
            nome: (nome ? nome : ''),
            cidade: (cidade ? cidade : ''),
            estado: (estado ? estado : '')
        }, page).subscribe(function (response) {
            _this.searching = false;
            _this.pageSearch = response['data']['page'];
            _this.totalPages = response['data']['totalPages'];
            _this.hasNextPageSearch = !response['data']['last'];
            response['data']['content'].forEach(function (item) {
                if (_this.dadosUsuario['usuario']['moradia']) {
                    if (item.id !== _this.dadosUsuario['usuario']['moradia']['id'])
                        _this.moradias.push(item);
                }
                else {
                    _this.moradias.push(item);
                }
            });
            _this.loader.dismiss();
        }, function (error) {
            _this.searching = false;
            _this.loader.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Mensagem',
                subTitle: 'Não foi possível consultar as moradias.',
                buttons: ['OK']
            });
            alert.present();
        });
    };
    MoradiaListPage.prototype.scrollMoradias = function (infiniteScroll) {
        var _this = this;
        setTimeout(function () {
            if (_this.hasNextPageSearch) {
                _this.pageSearch += 1;
                _this.getMoradias({
                    nome: _this.nomeSearch,
                    cidade: _this.cidadeSearch,
                    estado: _this.estadoSearch
                }, _this.pageSearch);
            }
            if (_this.pageSearch + 1 >= _this.totalPages) {
                _this.scrollEnabled = false;
                // infiniteScroll.enabled = false;
            }
            infiniteScroll.complete();
        }, 500);
    };
    MoradiaListPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.searching = true;
        this.loader = this.loadingCtrl.create({
            content: 'Por favor, aguarde.'
        });
        this.loader.present();
        this.geolocation.getCurrentPosition().then(function (resp) {
            var req = {
                position: {
                    lat: resp.coords.latitude,
                    lng: resp.coords.longitude
                }
            };
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["b" /* Geocoder */].geocode(req).then(function (results) {
                if (results) {
                    _this.cidadeSearch = results[0]['subAdminArea'];
                    _this.estadoSearch = results[0]['adminArea'];
                    switch (_this.estadoSearch) {
                        case 'Minas Gerais':
                            _this.estadoSearch = 'MG';
                            break;
                        case 'São Paulo':
                            _this.estadoSearch = 'SP';
                            break;
                        case 'Rio de Janeiro':
                            _this.estadoSearch = 'RJ';
                            break;
                        case 'Espírito Santo':
                            _this.estadoSearch = 'ES';
                            break;
                        case 'Bahia':
                            _this.estadoSearch = 'BA';
                            break;
                        case 'Rio Grande do Sul':
                            _this.estadoSearch = 'RS';
                            break;
                        case 'Rio Grande do Norte':
                            _this.estadoSearch = 'RN';
                            break;
                        case 'Santa Catarina':
                            _this.estadoSearch = 'SC';
                            break;
                        case 'Paraná':
                            _this.estadoSearch = 'PR';
                            break;
                        case 'Pará':
                            _this.estadoSearch = 'PA';
                            break;
                        case 'Piauí':
                            _this.estadoSearch = 'PI';
                            break;
                        case 'Pernambuco':
                            _this.estadoSearch = 'PE';
                            break;
                        case 'Sergipe':
                            _this.estadoSearch = 'SE';
                            break;
                        case 'Acre':
                            _this.estadoSearch = 'AC';
                            break;
                        case 'Alagoas':
                            _this.estadoSearch = 'AL';
                            break;
                        case 'Amapá':
                            _this.estadoSearch = 'AP';
                            break;
                        case 'Amazonas':
                            _this.estadoSearch = 'AM';
                            break;
                        case 'Ceará':
                            _this.estadoSearch = 'CE';
                            break;
                        case 'Distrito Federal':
                            _this.estadoSearch = 'DF';
                            break;
                        case 'Goiás':
                            _this.estadoSearch = 'GO';
                            break;
                        case 'Maranhão':
                            _this.estadoSearch = 'MA';
                            break;
                        case 'Mato Grosso':
                            _this.estadoSearch = 'MT';
                            break;
                        case 'Mato Grosso do Sul':
                            _this.estadoSearch = 'MS';
                            break;
                        case 'Paraíba':
                            _this.estadoSearch = 'PB';
                            break;
                        case 'Rondônia':
                            _this.estadoSearch = 'RO';
                            break;
                        case 'Roraima':
                            _this.estadoSearch = 'RR';
                            break;
                        case 'Tocantins':
                            _this.estadoSearch = 'TO';
                            break;
                        default:
                            _this.estadoSearch = '';
                            break;
                    }
                    _this.getMoradias({
                        nome: '',
                        cidade: _this.cidadeSearch,
                        estado: _this.estadoSearch
                    }, 0);
                }
                else {
                    _this.cidadeSearch = '';
                    _this.estadoSearch = '';
                }
            });
        }).catch(function (error) {
            _this.loader.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Aviso',
                subTitle: 'Não foi possível determinar sua localização. Utilize os filtros.',
                buttons: ['OK']
            });
            alert.present();
            _this.getMoradias({
                nome: '',
                cidade: _this.cidadeSearch,
                estado: _this.estadoSearch
            }, 0);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], MoradiaListPage.prototype, "content", void 0);
    MoradiaListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-moradia-list',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/moradia-list/moradia-list.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu" class="headerButton"></ion-icon>\n        </button>\n        <ion-title>Moradias</ion-title>\n        <ion-buttons end>\n            <button ion-button (click)="toggleSearchBar()">\n                <ion-icon name="search" class="headerButton"></ion-icon>\n            </button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <ion-searchbar *ngIf="search" debounce="1000"\n            [(ngModel)]="myInput"\n            [showCancelButton]="shouldShowCancel"\n            (ionInput)="onInput($event)"\n            (ionCancel)="onCancel($event)">\n    </ion-searchbar>\n    <h3 *ngIf="!searching && moradias.length === 0" padding>\n        Nenhuma moradia.\n    </h3>\n    <ion-list>\n        <ion-item-sliding *ngFor="let moradia of moradias" >\n            <ion-item [navPush]="moradiaModalPage" [navParams]="{moradia: moradia, usuario: dadosUsuario}">\n                <h4>{{ moradia.nome }}</h4>\n                <p>{{ moradia.endereco }}, {{ moradia.bairro }} - {{ moradia.cidade }}, {{ moradia.estado }}</p>\n            </ion-item>\n        </ion-item-sliding>\n    </ion-list>\n    <ion-infinite-scroll (ionInfinite)="scrollMoradias($event)" [enabled]="scrollEnabled">\n        <ion-infinite-scroll-content></ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n    <ion-fab *ngIf="dadosUsuario?.usuario.moradia === null" right bottom>\n        <button ion-fab color="orange" [navPush]="moradiaPage" [navParams]="{mode: \'create\', usuario: dadosUsuario}">\n            <ion-icon name="add"></ion-icon>\n        </button>\n    </ion-fab>\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/moradia-list/moradia-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_5__providers_moradia_moradia__["a" /* MoradiaProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_8__providers_vars_vars__["a" /* VarsProvider */]])
    ], MoradiaListPage);
    return MoradiaListPage;
}());

//# sourceMappingURL=moradia-list.js.map

/***/ }),

/***/ 88:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoradiaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_moradia_moradia__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_home__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_vars_vars__ = __webpack_require__(11);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var MoradiaPage = /** @class */ (function () {
    function MoradiaPage(navCtrl, navParams, loadingCtrl, formBuilder, moradiaProvider, alertCtrl, storage, varsProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.moradiaProvider = moradiaProvider;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.varsProvider = varsProvider;
        if (this.navParams.get('mode') === 'edit')
            this.moradia = this.navParams.get('moradia');
        else
            this.moradia = null;
        this.dadosUsuario = this.varsProvider.getUsuario();
        if (this.dadosUsuario) {
            this.moradiaProvider.setHeaderToken(this.dadosUsuario.accessToken);
            var nome = (this.moradia ? this.moradia.nome : '');
            var cep = (this.moradia ? this.moradia.cep : '');
            var endereco = (this.moradia ? this.moradia.endereco : '');
            var numero = (this.moradia ? this.moradia.numero : '');
            var complemento = (this.moradia ? this.moradia.complemento : '');
            var uf = (this.moradia ? this.moradia.estado : '');
            var cidade = (this.moradia ? this.moradia.cidade : '');
            var bairro = (this.moradia ? this.moradia.bairro : '');
            var observacoes = (this.moradia ? this.moradia.comentario : '');
            this.moradiaForm = this.formBuilder.group({
                nome: [
                    nome,
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([
                        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].minLength(1),
                        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(40),
                        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required
                    ])
                ],
                cep: [
                    cep,
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([
                        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required,
                        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].minLength(8),
                        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(8)
                    ])
                ],
                endereco: [
                    endereco,
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([
                        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].minLength(1),
                        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(80),
                        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required
                    ])
                ],
                numero: [
                    numero,
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([
                        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].minLength(1),
                        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(11),
                        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required
                    ])
                ],
                complemento: [complemento, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(40)])],
                uf: [uf, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
                cidade: [
                    cidade,
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([
                        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].minLength(1),
                        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(40),
                        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required
                    ])
                ],
                bairro: [
                    bairro,
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([
                        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].minLength(1),
                        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(40),
                        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required
                    ])
                ],
                observacoes: [observacoes, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(200)])]
            });
        }
        else {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]);
            this.navCtrl.popToRoot();
        }
    }
    MoradiaPage.prototype.slugify = function (text) {
        return text.toString().toLowerCase()
            .replace(/\s+/g, '-') // Replace spaces with -
            .replace(/[^\w\-]+/g, '') // Remove all non-word chars
            .replace(/\-\-+/g, '-') // Replace multiple - with single -
            .replace(/^-+/, '') // Trim - from start of text
            .replace(/-+$/, ''); // Trim - from end of text
    };
    MoradiaPage.prototype.onSubmit = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Por favor, aguarde."
        });
        loader.present();
        var data = {
            nome: this.moradiaForm.value.nome,
            apelido: this.slugify(this.moradiaForm.value.nome),
            cep: this.moradiaForm.value.cep,
            endereco: this.moradiaForm.value.endereco,
            numero: this.moradiaForm.value.numero,
            complemento: this.moradiaForm.value.complemento,
            estado: this.moradiaForm.value.uf,
            cidade: this.moradiaForm.value.cidade,
            bairro: this.moradiaForm.value.bairro,
            comentario: this.moradiaForm.value.observacoes,
            responsavel: this.dadosUsuario.usuario.id
        };
        if (this.moradia) {
            this.moradiaProvider.updateMoradia(__assign({}, data, { id: this.moradia.id })).subscribe(function (response) {
                if (response['meta']['messageCode'] === "msg.moradia.atualizada") {
                    console.log('Moradia Atualizada');
                    _this.moradiaProvider.getMoradiaPeloUsuario(_this.dadosUsuario.usuario.id).subscribe(function (res) {
                        if (res['meta']['messageCode'] === "msg.servico.execucao.sucesso") {
                            var moradia = res['data'];
                            _this.dadosUsuario['usuario']['moradia'] = {
                                id: moradia['id'],
                                nome: moradia['nome'],
                                endereco: moradia['endereco'],
                                numero: moradia['numero'],
                                complemento: moradia['complemento'],
                                bairro: moradia['bairro'],
                                cidade: moradia['cidade'],
                                estado: moradia['estado'],
                                cep: moradia['cep'],
                                responsavel: _this.dadosUsuario.usuario.id,
                            };
                            _this.varsProvider.setUsuario(_this.dadosUsuario);
                            _this.storage.set('dados', JSON.stringify(_this.dadosUsuario));
                        }
                        loader.dismiss();
                        var alert = _this.alertCtrl.create({
                            title: 'Moradia Atualizada!',
                            subTitle: 'Moradia atualizada com sucesso.',
                            buttons: [
                                {
                                    text: 'OK',
                                    handler: function () {
                                        _this.navCtrl.pop();
                                    }
                                }
                            ]
                        });
                        alert.present();
                    }, function (err) {
                        loader.dismiss();
                        var alert = _this.alertCtrl.create({
                            title: 'Aviso',
                            subTitle: 'Nnão foi possível atualizar a moradia.',
                            buttons: [
                                {
                                    text: 'OK',
                                    handler: function () {
                                        _this.navCtrl.pop();
                                    }
                                }
                            ]
                        });
                        alert.present();
                    });
                }
                else {
                }
            }, function (error) {
                loader.dismiss();
                if (error['meta']['messageCode'] === "msg.apelido.ja.utilizado") {
                    var alert_1 = _this.alertCtrl.create({
                        title: 'Aviso',
                        subTitle: 'Já existe uma Moradia com este nome.',
                        buttons: ['OK']
                    });
                    alert_1.present();
                }
                else {
                    var alert_2 = _this.alertCtrl.create({
                        title: 'Aviso',
                        subTitle: 'Não foi possível atualizar a Moradia.',
                        buttons: ['OK']
                    });
                    alert_2.present();
                }
            });
        }
        else {
            this.moradiaProvider.addMoradia(data).subscribe(function (response) {
                if (response['meta']['messageCode'] === "msg.moradia.registrada") {
                    _this.moradiaProvider.getMoradiaPeloUsuario(_this.dadosUsuario.usuario.id).subscribe(function (res) {
                        if (res['meta']['messageCode'] === "msg.servico.execucao.sucesso") {
                            var moradia = res['data'];
                            _this.dadosUsuario['usuario']['moradia'] = {
                                id: moradia['id'],
                                nome: moradia['nome'],
                                endereco: moradia['endereco'],
                                numero: moradia['numero'],
                                complemento: moradia['complemento'],
                                bairro: moradia['bairro'],
                                cidade: moradia['cidade'],
                                estado: moradia['estado'],
                                cep: moradia['cep'],
                                responsavel: _this.dadosUsuario.usuario.id,
                            };
                            _this.varsProvider.setUsuario(_this.dadosUsuario);
                            _this.storage.set('dados', JSON.stringify(_this.dadosUsuario));
                        }
                        loader.dismiss();
                        var alert = _this.alertCtrl.create({
                            title: 'Moradia Criada!',
                            subTitle: 'Moradia criada com sucesso.',
                            buttons: [
                                {
                                    text: 'OK',
                                    handler: function () {
                                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]);
                                        _this.navCtrl.popToRoot();
                                    }
                                }
                            ]
                        });
                        alert.present();
                    }, function (err) {
                        loader.dismiss();
                        var alert = _this.alertCtrl.create({
                            title: 'Aviso!',
                            subTitle: 'Não foi possível criar a moradia.',
                            buttons: [
                                {
                                    text: 'OK',
                                    handler: function () {
                                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]);
                                        _this.navCtrl.popToRoot();
                                    }
                                }
                            ]
                        });
                        alert.present();
                    });
                }
                else if (response['meta']['messageCode'] === "msg.usuario.vinculado.moradia") {
                    var alert_3 = _this.alertCtrl.create({
                        title: 'Aviso',
                        subTitle: 'Seu Usuário já está vinculado a uma Moradia.',
                        buttons: ['OK']
                    });
                    alert_3.present();
                }
                else {
                    var alert_4 = _this.alertCtrl.create({
                        title: 'Aviso',
                        subTitle: 'Mensagem: ' + response['meta']['messageCode'],
                        buttons: ['OK']
                    });
                    alert_4.present();
                }
            }, function (error) {
                loader.dismiss();
                if (error['meta']['messageCode'] === "msg.apelido.ja.utilizado") {
                    var alert_5 = _this.alertCtrl.create({
                        title: 'Aviso',
                        subTitle: 'Já existe uma Moradia com este nome.',
                        buttons: ['OK']
                    });
                    alert_5.present();
                }
                else {
                    var alert_6 = _this.alertCtrl.create({
                        title: 'Aviso',
                        subTitle: 'Não foi possível criar a Moradia.',
                        buttons: ['OK']
                    });
                    alert_6.present();
                }
            });
        }
    };
    MoradiaPage.prototype.consultarCEP = function (e) {
        var _this = this;
        var value = e.target.value;
        if (value.length !== 8)
            return;
        var loader = this.loadingCtrl.create({
            content: "Consultando dados do CEP."
        });
        loader.present();
        this.moradiaProvider.consultarCEP(value).subscribe(function (data) {
            if (data['erro']) {
                loader.dismiss();
                _this.moradiaForm.controls.endereco.setValue('');
                _this.moradiaForm.controls.bairro.setValue('');
                _this.moradiaForm.controls.cidade.setValue('');
                _this.moradiaForm.controls.uf.setValue('');
                var alert_7 = _this.alertCtrl.create({
                    title: 'Aviso',
                    subTitle: 'Não foi possível consultar o CEP. Verifique se o digitou corretamente e tente novamente.',
                    buttons: ['OK']
                });
                alert_7.present();
            }
            else {
                _this.moradiaForm.controls.endereco.setValue(data['logradouro']);
                _this.moradiaForm.controls.bairro.setValue(data['bairro']);
                _this.moradiaForm.controls.cidade.setValue(data['localidade']);
                _this.moradiaForm.controls.uf.setValue(data['uf'].toUpperCase());
                loader.dismiss();
            }
        }, function (error) {
            loader.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Aviso',
                subTitle: 'Não foi possível consultar o CEP. Verifique sua conexão com a Internet e tente novamente',
                buttons: ['OK']
            });
            alert.present();
        });
    };
    MoradiaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-moradia',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/moradia/moradia.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>Moradia</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <div class="container">\n        <ion-grid>\n            <form [formGroup]="moradiaForm" (ngSubmit)="onSubmit()">\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Nome</ion-label>\n                        <ion-input type="text" formControlName="nome"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>CEP</ion-label>\n                        <ion-input type="number" formControlName="cep" (keyup)="consultarCEP($event)"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Endereço</ion-label>\n                        <ion-input type="text" formControlName="endereco"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-col col-3>\n                        <ion-item>\n                            <ion-label floating>Nº</ion-label>\n                            <ion-input type="number" formControlName="numero"></ion-input>\n                        </ion-item>\n                    </ion-col>\n                    <ion-col col-9>\n                        <ion-item>\n                            <ion-label floating>Complemento</ion-label>\n                            <ion-input type="text" formControlName="complemento"></ion-input>\n                        </ion-item>\n                    </ion-col>\n                </ion-row>\n                <ion-row>\n                    <ion-col col-3>\n                        <ion-list>\n                            <ion-item>\n                                <ion-label floating>UF</ion-label>\n                                <ion-select formControlName="uf" name="uf">\n                                    <ion-option value="AC">AC</ion-option>\n                                    <ion-option value="AL">AL</ion-option>\n                                    <ion-option value="AM">AM</ion-option>\n                                    <ion-option value="AP">AP</ion-option>\n                                    <ion-option value="BA">BA</ion-option>\n                                    <ion-option value="CE">CE</ion-option>\n                                    <ion-option value="DF">DF</ion-option>\n                                    <ion-option value="ES">ES</ion-option>\n                                    <ion-option value="GO">GO</ion-option>\n                                    <ion-option value="MA">MA</ion-option>\n                                    <ion-option value="MG">MG</ion-option>\n                                    <ion-option value="MS">MS</ion-option>\n                                    <ion-option value="MT">MT</ion-option>\n                                    <ion-option value="PA">PA</ion-option>\n                                    <ion-option value="PB">PB</ion-option>\n                                    <ion-option value="PE">PE</ion-option>\n                                    <ion-option value="PI">PI</ion-option>\n                                    <ion-option value="PR">PR</ion-option>\n                                    <ion-option value="RJ">RJ</ion-option>\n                                    <ion-option value="RN">RN</ion-option>\n                                    <ion-option value="RO">RO</ion-option>\n                                    <ion-option value="RR">RR</ion-option>\n                                    <ion-option value="RS">RS</ion-option>\n                                    <ion-option value="SC">SC</ion-option>\n                                    <ion-option value="SP">SP</ion-option>\n                                    <ion-option value="SE">SE</ion-option>\n                                    <ion-option value="TO">TO</ion-option>\n                                </ion-select>\n                            </ion-item>\n                        </ion-list>\n                    </ion-col>\n                    <ion-col col-5>\n                        <ion-item>\n                            <ion-label floating>Cidade</ion-label>\n                            <ion-input type="text" formControlName="cidade"></ion-input>\n                        </ion-item>\n                    </ion-col>\n                    <ion-col col-4>\n                        <ion-item>\n                            <ion-label floating>Bairro</ion-label>\n                            <ion-input type="text" formControlName="bairro"></ion-input>\n                        </ion-item>\n                    </ion-col>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Observações</ion-label>\n                        <ion-textarea formControlName="observacoes"></ion-textarea>\n                    </ion-item>\n                </ion-row>\n                <div padding>\n                    <button ion-button *ngIf="!moradia" btn-block color="submitButton" type="submit"\n                            block [disabled]="!moradiaForm.valid">\n                        Cadastrar Moradia\n                    </button>\n                    <button ion-button *ngIf="moradia" btn-block color="submitButton" type="submit"\n                            block [disabled]="!moradiaForm.valid">\n                        Atualizar Moradia\n                    </button>\n                </div>\n            </form>\n        </ion-grid>\n    </div>\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/moradia/moradia.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_4__providers_moradia_moradia__["a" /* MoradiaProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_6__providers_vars_vars__["a" /* VarsProvider */]])
    ], MoradiaPage);
    return MoradiaPage;
}());

//# sourceMappingURL=moradia.js.map

/***/ }),

/***/ 89:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoradiaDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_moradia_moradia__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__tarefa_list_tarefa_list__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__membro_list_membro_list__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__moradia_moradia__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_vars_vars__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__notificacoes_notificacoes__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__vaga_list_vaga_list__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__evento_list_evento_list__ = __webpack_require__(66);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var MoradiaDetailPage = /** @class */ (function () {
    function MoradiaDetailPage(navCtrl, navParams, moradiaProvider, varsProvider) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.moradiaProvider = moradiaProvider;
        this.varsProvider = varsProvider;
        this.membrosMoradia = [];
        this.membroListPage = __WEBPACK_IMPORTED_MODULE_6__membro_list_membro_list__["a" /* MembroListPage */];
        this.tarefaListPage = __WEBPACK_IMPORTED_MODULE_5__tarefa_list_tarefa_list__["a" /* TarefaListPage */];
        this.moradiaPage = __WEBPACK_IMPORTED_MODULE_7__moradia_moradia__["a" /* MoradiaPage */];
        this.vagaListPage = __WEBPACK_IMPORTED_MODULE_10__vaga_list_vaga_list__["a" /* VagaListPage */];
        this.notificacoesPage = __WEBPACK_IMPORTED_MODULE_9__notificacoes_notificacoes__["a" /* NotificacoesPage */];
        this.eventoListPage = __WEBPACK_IMPORTED_MODULE_11__evento_list_evento_list__["a" /* EventoListPage */];
        this.dadosUsuario = this.varsProvider.getUsuario();
        if (this.dadosUsuario) {
            this.moradiaProvider.setHeaderToken(this.dadosUsuario.accessToken);
        }
        else {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
            this.navCtrl.popToRoot();
        }
        if (this.navParams.get('moradia'))
            this.moradia = this.navParams.get('moradia');
        if (!this.moradia) {
            console.log('Moradia', this.moradia);
            this.moradiaProvider.getMoradiaPeloUsuario(this.dadosUsuario.usuario.id).subscribe(function (response) {
                console.log('Buscou moradia', response);
                if (response['meta']['messageCode'] === "msg.servico.execucao.sucesso") {
                    _this.moradia = response['data'];
                }
                else {
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
                    _this.navCtrl.popToRoot();
                }
            }, function (error) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
                _this.navCtrl.popToRoot();
            });
        }
    }
    MoradiaDetailPage.prototype.editarMoradia = function () {
        this.navCtrl.push(this.moradiaPage, { mode: 'edit', moradia: this.moradia });
    };
    MoradiaDetailPage.prototype.goToAndSetRoot = function (page, params) {
        this.navCtrl.setRoot(page, (params ? params : {}));
        // this.navCtrl.setRoot(page, (params ? params : {}));
    };
    MoradiaDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-moradia-detail',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/moradia-detail/moradia-detail.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu" class="headerButton"></ion-icon>\n        </button>\n        <ion-title>Detalhes da Moradia</ion-title>\n        <ion-buttons end>\n            <button ion-button (click)="editarMoradia()">\n                <ion-icon name="create" class="headerButton"></ion-icon>\n            </button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <div class="container">\n        <div text-center>\n            <img width="50px" height="50px" src="assets/imgs/logo_home.png">\n            <h1>{{ moradia?.nome }}</h1>\n            <p>{{ moradia?.endereco }}, {{ moradia?.numero }}<span *ngIf="moradia?.complemento">, {{ moradia?.complemento}}.</span></p>\n            <p>{{ moradia?.bairro }}, {{ moradia?.cidade }} - {{ moradia?.estado }}</p>\n        </div>\n        <ion-grid>\n            <ion-row style="margin-top: -15px;">\n                <ion-col col-2></ion-col>\n                <ion-col col-4 [navPush]="tarefaListPage">\n                    <p text-center>\n                        <img width="50px" height="50px" src="assets/imgs/logo_calendario.jpeg">\n                    </p>\n                    <h5 text-center>\n                        Tarefas\n                    </h5>\n                </ion-col>\n                <ion-col col-4 [navPush]="eventoListPage">\n                    <p text-center>\n                        <img width="50px" height="50px" src="assets/imgs/logo_eventos.jpeg">\n                    </p>\n                    <h5 text-center>\n                        Eventos\n                    </h5>\n                </ion-col>\n                <ion-col col-2></ion-col>\n            </ion-row>\n            <ion-row style="margin-top: -15px;" [hidden]="!dadosUsuario?.usuario?.moradia">\n                <ion-col col-2></ion-col>\n                <ion-col col-4 [navPush]="vagaListPage">\n                    <p text-center>\n                        <img width="50px" height="50px" src="assets/imgs/logo_editar.jpeg">\n                    </p>\n                    <h5 text-center>\n                        Vagas\n                    </h5>\n                </ion-col>\n                <ion-col col-4 [navPush]="membroListPage">\n                    <p text-center>\n                        <img width="50px" height="50px" src="assets/imgs/logo_membros.jpeg">\n                    </p>\n                    <h5 text-center>\n                        Membros\n                    </h5>\n                </ion-col>\n                <ion-col col-2></ion-col>\n            </ion-row>\n            <ion-row style="margin-top: -15px;">\n                <ion-col col-2></ion-col>\n                <ion-col col-4 [hidden]="!dadosUsuario?.usuario?.moradia" [navPush]="notificacoesPage">\n                    <p text-center>\n                        <img width="50px" height="50px" src="assets/imgs/logo_notificacoes.jpeg">\n                    </p>\n                    <h5 text-center>\n                        Notificações\n                    </h5>\n                </ion-col>\n                <ion-col col-4></ion-col>\n                <ion-col col-2></ion-col>\n            </ion-row>\n        </ion-grid>\n    </div>\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/moradia-detail/moradia-detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_moradia_moradia__["a" /* MoradiaProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_vars_vars__["a" /* VarsProvider */]])
    ], MoradiaDetailPage);
    return MoradiaDetailPage;
}());

//# sourceMappingURL=moradia-detail.js.map

/***/ }),

/***/ 90:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MembroListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_vars_vars__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_moradia_moradia__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MembroListPage = /** @class */ (function () {
    function MembroListPage(navCtrl, navParams, varsProvider, moradiaProvider, loadingCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.varsProvider = varsProvider;
        this.moradiaProvider = moradiaProvider;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.dadosUsuario = null;
        this.membros = [];
        this.searching = false;
        this.dadosUsuario = this.varsProvider.getUsuario();
        if (this.dadosUsuario) {
            this.moradiaProvider.setHeaderToken(this.dadosUsuario['accessToken']);
        }
        else {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
            this.navCtrl.popToRoot();
        }
    }
    MembroListPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.searching = true;
        var loader = this.loadingCtrl.create({
            content: 'Por favor, aguarde.'
        });
        loader.present();
        this.moradiaProvider.getMembrosDaMoradia(this.dadosUsuario['usuario']['moradia']['id']).subscribe(function (response) {
            _this.searching = false;
            loader.dismiss();
            console.log(response);
            if (response['meta']['messageCode'] === "msg.servico.execucao.sucesso") {
                response['data']['usuarios'].forEach(function (item) {
                    if (item.id !== _this.dadosUsuario['usuario']['id'])
                        _this.membros.push(item);
                });
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Aviso',
                    subTitle: response['meta']['messageCode'],
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }, function (error) {
            _this.searching = false;
            console.log(error);
            loader.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Aviso',
                subTitle: 'Não foi possível realizar a operação.',
                buttons: ['OK']
            });
            alert.present();
        });
    };
    MembroListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-membro-list',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/membro-list/membro-list.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu" class="headerButton"></ion-icon>\n        </button>\n        <ion-title>Membros da Moradia</ion-title>\n    </ion-navbar>\n</ion-header>\n<ion-content padding>\n    <h3 *ngIf="!searching && membros.length === 0" padding>\n        Nenhum membro.\n    </h3>\n    <ion-list>\n        <ion-item *ngFor="let membro of membros">\n            <h4>{{ membro.nome }}</h4>\n        </ion-item>\n    </ion-list>\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/membro-list/membro-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_vars_vars__["a" /* VarsProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_moradia_moradia__["a" /* MoradiaProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], MembroListPage);
    return MembroListPage;
}());

//# sourceMappingURL=membro-list.js.map

/***/ }),

/***/ 91:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsuarioCreatePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_usuario_usuario__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_home__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_vars_vars__ = __webpack_require__(11);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var UsuarioCreatePage = /** @class */ (function () {
    function UsuarioCreatePage(navCtrl, navParams, loadingCtrl, formBuilder, alertCtrl, usuarioProvider, storage, varsProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.alertCtrl = alertCtrl;
        this.usuarioProvider = usuarioProvider;
        this.storage = storage;
        this.varsProvider = varsProvider;
        this.usuarioForm = this.formBuilder.group({
            nome: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].minLength(1), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(40), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
            email: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].email, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(40), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
            telefone: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].pattern('[0-9]{10,11}'), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(13), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
            senha: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].minLength(1), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(20), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
            confirmacaoSenha: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].minLength(1), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(20), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
        }, {
            validator: this.matchingPassword('senha', 'confirmacaoSenha')
        });
    }
    UsuarioCreatePage.prototype.matchingPassword = function (passwordKey, confirmPasswordKey) {
        return function (group) {
            var passwordInput = group.controls[passwordKey];
            var passwordConfirmationInput = group.controls[confirmPasswordKey];
            if (passwordInput.value !== passwordConfirmationInput.value)
                return passwordConfirmationInput.setErrors({
                    mismatchedPasswords: true
                });
            else
                return passwordConfirmationInput.setErrors(null);
        };
    };
    UsuarioCreatePage.prototype.submitForm = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Por favor, aguarde.",
        });
        loader.present();
        this.usuarioProvider.addUsuario({
            nome: this.usuarioForm.value.nome,
            email: this.usuarioForm.value.email,
            telefone: this.usuarioForm.value.telefone,
            senha: this.usuarioForm.value.senha,
            perfil: 1,
            ativo: true
        }).subscribe(function (response) {
            if (response['meta']['messageCode'] === "msg.usuario.registrado") {
                // Criou usuário, agora autentica ele para salvar o token de acesso
                _this.usuarioProvider.authenticate(_this.usuarioForm.value.email, _this.usuarioForm.value.senha).subscribe(function (response) {
                    if (response['meta']['messageCode'] === "msg.servico.execucao.sucesso") {
                        var dados = {
                            accessToken: response['data']['accessToken'],
                            usuario: __assign({}, response['data']['usuario'] // Copia o json recebido para usuario
                            )
                        };
                        _this.varsProvider.setUsuario(dados);
                        _this.storage.set('dados', JSON.stringify(dados));
                        loader.dismiss();
                        var alert_1 = _this.alertCtrl.create({
                            title: 'Cadastro concluído.',
                            subTitle: 'Cadastro concluído com sucesso.',
                            buttons: [
                                {
                                    text: 'OK',
                                    handler: function () {
                                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]);
                                        _this.navCtrl.popToRoot();
                                    }
                                }
                            ]
                        });
                        alert_1.present();
                    }
                }, function (error) {
                    loader.dismiss();
                    var alert = _this.alertCtrl.create({
                        title: 'Aviso',
                        subTitle: 'Erro: ' + error.status + ' ' + error.statusText,
                        buttons: ['OK']
                    });
                    alert.present();
                });
            }
            else if (response['meta']['messageCode'] === "msg.email.ja.cadastrado") {
                loader.dismiss();
                var alert_2 = _this.alertCtrl.create({
                    title: 'Aviso',
                    subTitle: 'Este email já está cadastrado.',
                    buttons: ['OK']
                });
                alert_2.present();
            }
            else {
                loader.dismiss();
                var alert_3 = _this.alertCtrl.create({
                    title: 'Aviso',
                    subTitle: response['meta']['messageCode'],
                    buttons: ['OK']
                });
                alert_3.present();
            }
        }, function (error) {
            loader.dismiss();
            console.log(error);
            var alert = _this.alertCtrl.create({
                title: 'Aviso',
                subTitle: error['message'],
                buttons: ['OK']
            });
            alert.present();
        });
    };
    UsuarioCreatePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-usuario-create',template:/*ion-inline-start:"/home/caioassis/Documents/IonicDev/factio/src/pages/usuario-create/usuario-create.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>Criar Usuário</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <div class="container">\n        <ion-grid>\n            <form [formGroup]="usuarioForm" (ngSubmit)="submitForm()">\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Nome</ion-label>\n                        <ion-input type="text" formControlName="nome"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Email</ion-label>\n                        <ion-input type="email" formControlName="email" [class.invalid]="!usuarioForm.controls[\'email\'].valid"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Telefone</ion-label>\n                        <ion-input type="text" formControlName="telefone" [class.invalid]="!usuarioForm.controls[\'telefone\'].valid"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item>\n                        <ion-label floating>Senha</ion-label>\n                        <ion-input type="password" formControlName="senha"></ion-input>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <!--[class.invalid]="!usuarioForm.controls[\'confirmacaoSenha\'].valid && usuarioForm.controls[\'confirmacaoSenha\'].touched"-->\n                    <ion-item>\n                        <ion-label floating>Confirmar Senha</ion-label>\n                        <ion-input type="password" formControlName="confirmacaoSenha"></ion-input>\n                    </ion-item>\n                    <!--<ion-item *ngIf="usuarioForm.hasError(\'mismatchedPasswords\') && usuarioForm.controls.confirmacaoSenha.touched" class="invalid">-->\n                    <ion-item *ngIf="!usuarioForm.controls[\'confirmacaoSenha\'].valid && usuarioForm.controls[\'confirmacaoSenha\'].touched && usuarioForm.controls[\'confirmacaoSenha\'].errors?.mismatchedPasswords" class="invalid">\n                        <p>* Senhas não conferem!</p>\n                    </ion-item>\n                    <!--<div class=\'form-text error\' *ngIf="usuarioForm.controls.confirmacaoSenha.touched">-->\n                        <!--<div *ngIf="usuarioForm.hasError(\'mismatchedPasswords\')">Senhas não conferem.</div>-->\n                    <!--</div>-->\n                </ion-row>\n                <div padding>\n                    <button ion-button btn-block color="submitButton" type="submit"\n                            block [disabled]="!usuarioForm.valid">\n                        Concluir\n                    </button>\n                    <!--<button ion-button btn-block color="submitButton" type="submit"-->\n                            <!--block>-->\n                        <!--Concluir-->\n                    <!--</button>-->\n                </div>\n            </form>\n        </ion-grid>\n    </div>\n</ion-content>\n'/*ion-inline-end:"/home/caioassis/Documents/IonicDev/factio/src/pages/usuario-create/usuario-create.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_usuario_usuario__["a" /* UsuarioProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_6__providers_vars_vars__["a" /* VarsProvider */]])
    ], UsuarioCreatePage);
    return UsuarioCreatePage;
}());

//# sourceMappingURL=usuario-create.js.map

/***/ })

},[279]);
//# sourceMappingURL=main.js.map