import { Component } from '@angular/core';
import {App, Events} from "ionic-angular";
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';
import { EsqueciSenhaPage } from "../esqueci-senha/esqueci-senha";
import { UsuarioCreatePage} from "../usuario-create/usuario-create";
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { MoradiaProvider } from "../../providers/moradia/moradia";
import {VarsProvider} from "../../providers/vars/vars";


@IonicPage()
@Component(
    {
        selector: 'page-login',
        templateUrl: 'login.html',
    }
)
export class LoginPage {

    private loginForm : FormGroup;
    esqueciSenhaPage = EsqueciSenhaPage;
    usuarioCreatePage = UsuarioCreatePage;

    constructor(public navCtrl: NavController, public navParams: NavParams, private appCtrl: App,
                private formBuilder: FormBuilder, private loadingCtrl: LoadingController, private storage: Storage,
                private alertCtrl: AlertController, private usuarioProvider: UsuarioProvider,
                private moradiaProvider: MoradiaProvider, private varsProvider: VarsProvider, private events: Events)
    {
        this.loginForm = this.formBuilder.group(
            {
                email: ['', Validators.compose([Validators.email, Validators.required])],
                senha: ['', Validators.compose([Validators.minLength(1), Validators.maxLength(20), Validators.required])]
            }
        );
    }

    public submitForm()
    {
        const loader = this.loadingCtrl.create(
            {
                content: "Por favor, aguarde.",
            }
        );
        loader.present();
        this.usuarioProvider.authenticate(
            this.loginForm.value.email,
            this.loginForm.value.senha
        ).subscribe(
            (response) => {
                if(response['meta']['messageCode'] === "msg.servico.execucao.sucesso")
                {
                    let dados = {
                        accessToken: response['data']['accessToken'],
                        usuario: {
                            ...response['data']['usuario']
                        }
                    };
                    this.moradiaProvider.setHeaderToken(dados['accessToken']);
                    this.moradiaProvider.getMoradiaPeloUsuario(
                        response['data']['usuario']['id']
                    ).subscribe(
                        (res) => {
                            dados['usuario']['moradia'] = {
                                ...res['data']
                            };
                            this.varsProvider.setUsuario(dados);
                            this.storage.set('dados', JSON.stringify(dados));
                            this.events.publish('updateUsuario');
                            this.navCtrl.setRoot(HomePage);
                            this.navCtrl.popToRoot();
                            loader.dismiss();
                        },
                        (err) => {
                            this.varsProvider.setUsuario(dados);
                            this.storage.set('dados', JSON.stringify(dados));
                            this.events.publish('updateUsuario');
                            this.navCtrl.setRoot(HomePage);
                            this.navCtrl.popToRoot();
                            loader.dismiss();
                        }
                    );
                }
            },
            (error) => {
                loader.dismiss();
                const alert = this.alertCtrl.create(
                    {
                        title: 'Aviso',
                        subTitle: (error.status === 401 ? 'Email ou senha não estão corretos.' : 'Não foi possível realizar o login.'),
                        buttons: ['OK']
                    }
                );
                alert.present();
            }
        );
    }
}
