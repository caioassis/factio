import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoradiaListPage } from './moradia-list';

@NgModule({
  declarations: [
    MoradiaListPage,
  ],
  imports: [
    IonicPageModule.forChild(MoradiaListPage),
  ],
})
export class MoradiaListPageModule {}
