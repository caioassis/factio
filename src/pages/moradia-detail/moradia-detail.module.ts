import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoradiaDetailPage } from './moradia-detail';

@NgModule({
  declarations: [
    MoradiaDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(MoradiaDetailPage),
  ],
})
export class MoradiaDetailPageModule {}
