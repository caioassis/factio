import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from "../login/login";
import { MoradiaProvider } from "../../providers/moradia/moradia";
import { Moradia } from "../../models/moradia";
import { HomePage } from "../home/home";
import { Usuario } from "../../models/usuario";
import { TarefaListPage } from "../tarefa-list/tarefa-list";
import { MembroListPage } from "../membro-list/membro-list";
import { MoradiaPage } from "../moradia/moradia";
import {VarsProvider} from "../../providers/vars/vars";
import { NotificacoesPage } from "../notificacoes/notificacoes";
import { VagaListPage } from "../vaga-list/vaga-list";
import { EventoListPage } from "../evento-list/evento-list";

@IonicPage()
@Component({
    selector: 'page-moradia-detail',
    templateUrl: 'moradia-detail.html',
})
export class MoradiaDetailPage
{

    dadosUsuario: any;
    moradia: Moradia;
    membrosMoradia: Array<Usuario> = [];
    membroListPage: any = MembroListPage;
    tarefaListPage: any = TarefaListPage;
    moradiaPage = MoradiaPage;
    vagaListPage = VagaListPage;
    notificacoesPage = NotificacoesPage;
    eventoListPage = EventoListPage;

    constructor(public navCtrl: NavController, public navParams: NavParams,
                private moradiaProvider: MoradiaProvider,
                private varsProvider: VarsProvider)
    {
        this.dadosUsuario = this.varsProvider.getUsuario();
        if(this.dadosUsuario)
        {
            this.moradiaProvider.setHeaderToken(this.dadosUsuario.accessToken);
        } else
        {
            this.navCtrl.setRoot(LoginPage);
            this.navCtrl.popToRoot();
        }

        if(this.navParams.get('moradia'))
            this.moradia = this.navParams.get('moradia');

        if(!this.moradia)
        {
            console.log('Moradia', this.moradia);
            this.moradiaProvider.getMoradiaPeloUsuario(
                this.dadosUsuario.usuario.id
            ).subscribe(
                (response) => {
                    console.log('Buscou moradia', response);
                    if(response['meta']['messageCode'] === "msg.servico.execucao.sucesso")
                    {
                        this.moradia = response['data'];
                    } else
                    {
                        this.navCtrl.setRoot(HomePage);
                        this.navCtrl.popToRoot();
                    }
                },
                (error) => {
                    this.navCtrl.setRoot(HomePage);
                    this.navCtrl.popToRoot();
                }
            );
        }
    }

    editarMoradia()
    {
        this.navCtrl.push(this.moradiaPage, {mode: 'edit', moradia: this.moradia});
    }

    goToAndSetRoot(page: any, params)
    {
        this.navCtrl.setRoot(page,(params ? params : {}));
        // this.navCtrl.setRoot(page, (params ? params : {}));
    }

}
