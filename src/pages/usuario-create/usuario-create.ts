import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { UsuarioProvider } from "../../providers/usuario/usuario";
import { Usuario } from "../../models/usuario";
import {HomePage} from "../home/home";
import { VarsProvider } from "../../providers/vars/vars";

@IonicPage()
@Component({
    selector: 'page-usuario-create',
    templateUrl: 'usuario-create.html',
})
export class UsuarioCreatePage
{
    usuarioForm: FormGroup = this.formBuilder.group(
        {
            nome: ['', Validators.compose([Validators.minLength(1), Validators.maxLength(40), Validators.required])],
            email: ['', Validators.compose([Validators.email, Validators.maxLength(40), Validators.required])],
            telefone: ['', Validators.compose([Validators.pattern('[0-9]{10,11}'), Validators.maxLength(13), Validators.required])],
            senha: ['', Validators.compose([Validators.minLength(1), Validators.maxLength(20), Validators.required])],
            confirmacaoSenha: ['', Validators.compose([Validators.minLength(1), Validators.maxLength(20), Validators.required])],
        }, {
            validator: this.matchingPassword('senha', 'confirmacaoSenha')
        }
    );

    constructor(public navCtrl: NavController, public navParams: NavParams, private loadingCtrl: LoadingController,
                private formBuilder: FormBuilder, public alertCtrl: AlertController,
                private usuarioProvider: UsuarioProvider, private storage: Storage, private varsProvider: VarsProvider)
    {}


    matchingPassword(passwordKey: string, confirmPasswordKey: string)
    {
        return (group: FormGroup) => {
            let passwordInput = group.controls[passwordKey];
            let passwordConfirmationInput = group.controls[confirmPasswordKey];
            if (passwordInput.value !== passwordConfirmationInput.value)
                return passwordConfirmationInput.setErrors(
                    {
                        mismatchedPasswords: true
                    }
                );
            else
                return passwordConfirmationInput.setErrors(null);
        }
    }

    public submitForm()
    {
        const loader = this.loadingCtrl.create(
            {
                content: "Por favor, aguarde.",
            }
        );
        loader.present();
        this.usuarioProvider.addUsuario(
            {
                nome: this.usuarioForm.value.nome,
                email: this.usuarioForm.value.email,
                telefone: this.usuarioForm.value.telefone,
                senha: this.usuarioForm.value.senha,
                perfil: 1, // Usuário comum
                ativo: true
            } as Usuario
        ).subscribe(
            (response) => {
                if(response['meta']['messageCode'] === "msg.usuario.registrado")
                {
                    // Criou usuário, agora autentica ele para salvar o token de acesso
                    this.usuarioProvider.authenticate(
                        this.usuarioForm.value.email,
                        this.usuarioForm.value.senha
                    ).subscribe(
                        (response) => {
                            if(response['meta']['messageCode'] === "msg.servico.execucao.sucesso")
                            {
                                let dados = {
                                    accessToken: response['data']['accessToken'],
                                    usuario: {
                                        ...response['data']['usuario'] // Copia o json recebido para usuario
                                    }
                                };
                                this.varsProvider.setUsuario(dados);
                                this.storage.set('dados', JSON.stringify(dados));
                                loader.dismiss();
                                const alert = this.alertCtrl.create(
                                    {
                                        title: 'Cadastro concluído.',
                                        subTitle: 'Cadastro concluído com sucesso.',
                                        buttons: [
                                            {
                                                text: 'OK',
                                                handler: () => {
                                                    this.navCtrl.setRoot(HomePage);
                                                    this.navCtrl.popToRoot();
                                                }
                                            }
                                        ]
                                    }
                                );
                                alert.present();
                            }
                        },
                        (error) => {
                            loader.dismiss();
                            const alert = this.alertCtrl.create(
                                {
                                    title: 'Aviso',
                                    subTitle: 'Erro: ' + error.status + ' ' + error.statusText,
                                    buttons: ['OK']
                                }
                            );
                            alert.present();
                        }
                    );
                } else if (response['meta']['messageCode'] === "msg.email.ja.cadastrado")
                {
                    loader.dismiss();
                    const alert = this.alertCtrl.create(
                        {
                            title: 'Aviso',
                            subTitle: 'Este email já está cadastrado.',
                            buttons: ['OK']
                        }
                    );
                    alert.present();
                } else
                {
                    loader.dismiss();
                    const alert = this.alertCtrl.create(
                        {
                            title: 'Aviso',
                            subTitle: response['meta']['messageCode'],
                            buttons: ['OK']
                        }
                    );
                    alert.present();
                }
            },
            (error) => {
                loader.dismiss();
                console.log(error);
                const alert = this.alertCtrl.create(
                    {
                        title: 'Aviso',
                        subTitle: error['message'],
                        buttons: ['OK']
                    }
                );
                alert.present();
            }
        );
    }
}
