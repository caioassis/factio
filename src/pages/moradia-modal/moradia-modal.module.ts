import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoradiaModalPage } from './moradia-modal';

@NgModule({
  declarations: [
    MoradiaModalPage,
  ],
  imports: [
    IonicPageModule.forChild(MoradiaModalPage),
  ],
})
export class MoradiaModalPageModule {}
