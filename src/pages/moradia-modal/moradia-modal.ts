import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import { Moradia } from "../../models/moradia";
import { MoradiaProvider } from "../../providers/moradia/moradia";
import { VarsProvider } from "../../providers/vars/vars";


@IonicPage()
@Component({
    selector: 'page-moradia-modal',
    templateUrl: 'moradia-modal.html',
})
export class MoradiaModalPage
{
    moradia: Moradia;
    dadosUsuario: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, private loadingCtrl: LoadingController,
                private moradiaProvider: MoradiaProvider, private alertCtrl: AlertController,
                private varsProvider: VarsProvider)
    {
        this.dadosUsuario = this.varsProvider.getUsuario();
        this.moradia = this.navParams.get('moradia');
    }

    solicitarIngresso()
    {
        const loader = this.loadingCtrl.create(
            {
                content: 'Por favor, aguarde.'
            }
        );
        loader.present();

        this.moradiaProvider.solicitarIngressoAMoradia(
            this.moradia.id,
            this.dadosUsuario['usuario']['id']
        ).subscribe(
            (response) => {
                console.log('Response', response);
                loader.dismiss();
                const alert = this.alertCtrl.create(
                    {
                        title: 'Solicitação Enviada!',
                        subTitle: 'Sua solicitação foi enviada com sucesso.',
                        buttons: [
                            {
                                text: 'OK',
                                handler: () => {
                                    this.navCtrl.pop();
                                }
                            }
                        ]
                    }
                );
                alert.present();
            },
            (error) => {
                loader.dismiss();
                const alert = this.alertCtrl.create(
                    {
                        title: 'Aviso',
                        subTitle: 'Não foi possível realizar a operação.',
                        buttons: [
                            {
                                text: 'OK',
                                handler: () => {
                                    this.navCtrl.pop();
                                }
                            }
                        ]
                    }
                );
                alert.present();
            }
        );
    }
}
