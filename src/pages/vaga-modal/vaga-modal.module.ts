import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VagaModalPage } from './vaga-modal';

@NgModule({
  declarations: [
    VagaModalPage,
  ],
  imports: [
    IonicPageModule.forChild(VagaModalPage),
  ],
})
export class VagaModalPageModule {}
