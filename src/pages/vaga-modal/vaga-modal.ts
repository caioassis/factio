import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {Vaga} from "../../models/vaga";
import {LoginPage} from "../login/login";
import {VarsProvider} from "../../providers/vars/vars";
import {MoradiaProvider} from "../../providers/moradia/moradia";

@IonicPage()
@Component({
    selector: 'page-vaga-modal',
    templateUrl: 'vaga-modal.html',
})
export class VagaModalPage
{
    vaga: Vaga;
    dadosUsuario: any;
    nomeResponsavel: string = '';

    constructor(public navCtrl: NavController, public navParams: NavParams, private varsProvider: VarsProvider,
                private moradiaProvider: MoradiaProvider, private loadingCtrl: LoadingController,
                private alertCtrl: AlertController)
    {
        this.dadosUsuario = this.varsProvider.getUsuario();
        if(!this.dadosUsuario)
        {
            this.navCtrl.setRoot(LoginPage);
            this.navCtrl.popToRoot();
        }
        this.moradiaProvider.setHeaderToken(this.dadosUsuario['accessToken']);
        this.vaga = this.navParams.get('vaga');
    }

    candidatarUsuario()
    {
        const loader = this.loadingCtrl.create(
            {
                content: 'Por favor, aguarde.'
            }
        );
        loader.present();
        this.moradiaProvider.solicitarIngressoAMoradia(
            this.vaga['moradia']['id'],
            this.dadosUsuario.usuario.id
        ).subscribe(
            (response) => {
                if(response['meta']['messageCode'] === "msg.solicitacao.enviada")
                {
                    loader.dismiss();
                    const alert = this.alertCtrl.create(
                        {
                            title: 'Solicitação Enviada!',
                            subTitle: 'Sua solicitação de vaga na moradia foi feita com sucesso.',
                            buttons: [
                                {
                                    text: 'OK',
                                    handler: () => {
                                        this.navCtrl.pop();
                                    }
                                }
                            ]
                        }
                    );
                    alert.present();
                } else
                {
                    loader.dismiss();
                    const alert = this.alertCtrl.create(
                        {
                            title: 'Outra mensage',
                            subTitle: response['meta']['messageCode'],
                            buttons: [
                                {
                                    text: 'OK',
                                    handler: () => {
                                        this.navCtrl.pop();
                                    }
                                }
                            ]

                        }
                    );
                    alert.present();
                }

            },
            (error) => {
                loader.dismiss();
                const alert = this.alertCtrl.create(
                    {
                        title: 'Aviso',
                        subTitle: 'Não foi possível realizar a operação.',
                        buttons: [
                            {
                                text: 'OK',
                                handler: () => {
                                    this.navCtrl.pop();
                                }
                            }
                        ]

                    }
                );
                alert.present();
            }
        );
    }
}
