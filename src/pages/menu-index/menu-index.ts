import { Component } from '@angular/core';
import {App, IonicPage, NavController, NavParams} from 'ionic-angular';
import { VarsProvider } from "../../providers/vars/vars";
import { VagaListPage } from "../vaga-list/vaga-list";
import { TarefaListPage } from "../tarefa-list/tarefa-list";
import { MoradiaListPage } from "../moradia-list/moradia-list";
import { EventoListPage } from "../evento-list/evento-list";
import { NotificacoesPage } from "../notificacoes/notificacoes";
import { MembroListPage } from "../membro-list/membro-list";


@IonicPage()
@Component({
    selector: 'page-menu-index',
    templateUrl: 'menu-index.html',
})
export class MenuIndexPage {
    dadosUsuario: any = null;
    vagaListPage = VagaListPage;
    tarefaListPage = TarefaListPage;
    moradiaListPage = MoradiaListPage;
    eventoListPage = EventoListPage;
    notificacoesPage = NotificacoesPage;
    membroListPage = MembroListPage;

    constructor(private appCtrl: App, public navCtrl: NavController, public navParams: NavParams, private varsProvider: VarsProvider)
    {
        this.dadosUsuario = this.varsProvider.getUsuario();
    }

    goToAndSetRoot(page: any, params)
    {
        this.appCtrl.getRootNav().setRoot(page,(params ? params : {}));
        // this.navCtrl.setRoot(page, (params ? params : {}));
    }

}
