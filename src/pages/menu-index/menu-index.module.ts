import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuIndexPage } from './menu-index';

@NgModule({
  declarations: [
    MenuIndexPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuIndexPage),
  ],
})
export class MenuIndexPageModule {}
