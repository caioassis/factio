import { Component } from '@angular/core';
import { AlertController, IonicPage, Loading, LoadingController, NavController, NavParams } from 'ionic-angular';
import { VarsProvider } from "../../providers/vars/vars";
import { TarefaProvider } from "../../providers/tarefa/tarefa";
import { Moradia } from "../../models/moradia";
import { TarefaPage } from "../tarefa/tarefa";
import { AvaliarTarefaPage } from "../avaliar-tarefa/avaliar-tarefa";
import { Tarefa } from "../../models/tarefa";

@IonicPage()
@Component({
    selector: 'page-semana-index',
    templateUrl: 'semana-index.html',
})
export class SemanaIndexPage
{
    dadosUsuario: any;

    moradia: Moradia;
    search = false;
    searching = false;
    pageSearch: number = 0;
    totalPages: number;
    hasNextPageSearch: boolean = false;
    nomeSearch: string = '';

    scrollEnabled = true;
    loader: Loading;
    tarefaPage = TarefaPage;
    avaliarTarefaPage: any = AvaliarTarefaPage;
    tarefas: Array<Tarefa> = [
        {
            id: 30,
            nome: 'Limpar a casa',
            descricao: 'Tem que limpar a casa, poxa',
            moradiaId: 1,
            responsaveis: [1, 4, 5],
            tipo: 'Tarefa Doméstica',
            dataConclusao: '2018-12-15'
        } as Tarefa
    ];

    constructor(public navCtrl: NavController, public navParams: NavParams, private varsProvider: VarsProvider,
                private tarefaProvider: TarefaProvider, private loadingCtrl: LoadingController,
                private alertCtrl: AlertController)
    {
        this.dadosUsuario = this.varsProvider.getUsuario();
        if(this.dadosUsuario)
        {
            this.tarefaProvider.setHeaderToken(this.dadosUsuario['accessToken']);
        }

    }

    atualizarTarefa(tarefa, finalizado)
    {
        // const loader = this.loadingCtrl.create(
        //     {
        //         content: "Por favor, aguarde enquanto executamos a operação.",
        //     }
        // );
        // let url = 'http://167.250.197.3:1026/v1/contextEntities/' + tarefa.id.toString() + '/attributes/finalizado';
        // let mensagem = "";
        // let headers = new Headers();
        // headers.append('Content-Type', 'application/json');
        // headers.append('Accept', 'application/json');
        //
        // let options = new RequestOptions(
        //     {
        //         headers: headers
        //     }
        // );
        // let data = JSON.stringify(
        //     {
        //         "value": finalizado
        //     }
        // );
        // this.http.put(url, data, options).subscribe(
        //     data => {
        //         let statusCode = data.status;
        //         if(statusCode === 200)
        //         {
        //             mensagem = "Tarefa atualizada com sucesso.";
        //         } else {
        //             mensagem = "Não foi possível atualizar a tarefa. Atualize a lista de tarefas e tente novamente.";
        //         }
        //         loader.dismiss();
        //         const alert = this.alertCtrl.create(
        //             {
        //                 title: 'Mensagem',
        //                 subTitle: mensagem,
        //                 buttons: [
        //                     {
        //                         text: 'OK',
        //                         handler: () => {
        //                             this.getTarefas();
        //                         }
        //                     }
        //                 ]
        //             }
        //         );
        //         alert.present();
        //     },
        //     error => {
        //         mensagem = "Não foi possível atualizar a tarefa. Verifique sua conexão com a internet e tente novamente.";
        //         loader.dismiss();
        //         const alert = this.alertCtrl.create(
        //             {
        //                 title: 'Mensagem',
        //                 subTitle: mensagem,
        //                 buttons: [
        //                     {
        //                         text: 'OK',
        //                         handler: () => {
        //                             this.getTarefas();
        //                         }
        //                     }
        //                 ]
        //             }
        //         );
        //         alert.present();
        //     }
        // );
    }

    excluirTarefa(tarefa)
    {
        // const loader = this.loadingCtrl.create(
        //     {
        //         content: "Por favor, aguarde enquanto executamos a operação.",
        //     }
        // );
        // let url = 'http://167.250.197.3:1026/v1/contextEntities/' + tarefa.id.toString();
        // let mensagem = "";
        // this.http.delete(url).subscribe(
        //     data => {
        //         let statusCode = data.status;
        //         if(statusCode === 200)
        //         {
        //             mensagem = "Tarefa excluída com sucesso.";
        //         } else {
        //             mensagem = "Não foi possível excluir a tarefa. Atualize a lista de tarefas e tente novamente.";
        //         }
        //         loader.dismiss();
        //         const alert = this.alertCtrl.create(
        //             {
        //                 title: 'Mensagem',
        //                 subTitle: mensagem,
        //                 buttons: [
        //                     {
        //                         text: 'OK',
        //                         handler: () => {
        //                             this.getTarefas();
        //                         }
        //                     }
        //                 ]
        //             }
        //         );
        //         alert.present();
        //     },
        //     error => {
        //         mensagem = "Não foi possível excluir a tarefa. Verifique sua conexão com a internet e tente novamente.";
        //         loader.dismiss();
        //         const alert = this.alertCtrl.create(
        //             {
        //                 title: 'Mensagem',
        //                 subTitle: mensagem,
        //                 buttons: [
        //                     {
        //                         text: 'OK',
        //                         handler: () => {
        //                             this.getTarefas();
        //                         }
        //                     }
        //                 ]
        //             }
        //         );
        //         alert.present();
        //     }
        // );

    }

    getTarefas({nome, moradia}, page: number)
    {
        this.searching = true;
        if(!this.loader)
        {
            this.loader = this.loadingCtrl.create(
                {
                    content: 'Por favor, aguarde.'
                }
            );
            this.loader.present();
        }

        if(this.pageSearch == 0)
            this.tarefas = [];

        this.tarefaProvider.getTarefas(
            {
                nome: '',
                moradia: this.dadosUsuario.usuario.moradia.id
            },
            0
        ).subscribe(
            (response) => {
                console.log(response);
                this.searching = false;
                this.loader.dismiss();
                if(response['meta']['messageCode'] === "msg.servico.execucao.sucesso")
                {
                    response['data']['tarefas'].forEach(
                        (item: Tarefa) => {
                            if(item.responsaveis.indexOf(this.dadosUsuario['usuario']['id']) > -1)
                            {
                                this.tarefas.push(item);
                            }
                        }
                    );
                } else
                {

                }
            },
            (error) => {
                console.log(error);
                this.searching = false;
                this.loader.dismiss();
                const alert = this.alertCtrl.create(
                    {
                        title: 'Mensagem',
                        subTitle: 'Não foi possível consultar as tarefas.',
                        buttons: ['OK']
                    }
                );
                alert.present();
            }
        );
    }

    scrollTarefas(infiniteScroll)
    {
        setTimeout(
            () => {
                if(this.hasNextPageSearch)
                {
                    this.pageSearch += 1;
                    this.getTarefas(
                        {
                            nome: this.nomeSearch,
                            moradia: this.dadosUsuario.usuario.moradia.id
                        },
                        this.pageSearch
                    );
                }
                if(this.pageSearch + 1 >= this.totalPages)
                {
                    this.scrollEnabled = false;
                    // infiniteScroll.enabled = false;
                }
                infiniteScroll.complete();
            },
            500
        );
    }

    ionViewWillEnter()
    {
        this.searching = true;
        this.loader = this.loadingCtrl.create(
            {
                content: 'Por favor, aguarde'
            }
        );
        // this.loader.present();
        this.getTarefas(
            {
                nome: '',
                moradia: this.dadosUsuario.usuario.moradia.id
            },
            0
        );
    }

}
