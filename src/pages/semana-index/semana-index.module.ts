import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SemanaIndexPage } from './semana-index';

@NgModule({
  declarations: [
    SemanaIndexPage,
  ],
  imports: [
    IonicPageModule.forChild(SemanaIndexPage),
  ],
})
export class SemanaIndexPageModule {}
