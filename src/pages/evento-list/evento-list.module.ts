import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventoListPage } from './evento-list';

@NgModule({
  declarations: [
    EventoListPage,
  ],
  imports: [
    IonicPageModule.forChild(EventoListPage),
  ],
})
export class EventoListPageModule {}
