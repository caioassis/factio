import {Component, OnInit, ViewChild} from '@angular/core';
import {Content, IonicPage, ModalController, NavController, NavParams, ToastController} from 'ionic-angular';
import { AlertController } from "ionic-angular";
import { LoadingController } from "ionic-angular";
import { Loading } from "ionic-angular";
import { EventoPage } from '../evento/evento';
import { EventoModalPage } from "../evento-modal/evento-modal";
import { EventoProvider } from "../../providers/evento/evento";
import { Evento } from "../../models/evento";
import { Network } from "@ionic-native/network";
import { Storage } from "@ionic/storage";
import { Geolocation } from "@ionic-native/geolocation";
import { LoginPage } from "../login/login";
import { Geocoder, GeocoderRequest } from "@ionic-native/google-maps";
import {VarsProvider} from "../../providers/vars/vars";
import {LocalizacaoModalPage} from "../localizacao-modal/localizacao-modal";

@IonicPage()
@Component({
    selector: 'page-evento-list',
    templateUrl: 'evento-list.html',
})
export class EventoListPage
{
    @ViewChild(Content) content: Content;

    eventoModalPage = EventoModalPage;
    private dadosUsuario: any;
    eventos: Array<Evento> = [];
    eventoPage = EventoPage;
    search = false;
    searching = false;
    pageSearch: number = 0;
    totalPages: number;
    hasNextPageSearch: boolean = false;
    nomeSearch: string = '';
    cidadeSearch: string = '';
    estadoSearch: string = '';
    scrollEnabled = true;
    loader: Loading;

    constructor(public navCtrl: NavController, public navParams: NavParams, private eventoProvider: EventoProvider,
                private loadingCtrl: LoadingController, private network: Network, private toastCtrl: ToastController,
                public alertCtrl: AlertController, private storage: Storage, private geolocation: Geolocation,
                private varsProvider: VarsProvider, private modalCtrl: ModalController)
    {
        this.dadosUsuario = this.varsProvider.getUsuario();
        if(this.dadosUsuario)
        {
           this.eventoProvider.setHeaderToken(this.dadosUsuario['accessToken']);
        } else
        {
            this.navCtrl.setRoot(LoginPage);
            this.navCtrl.popToRoot();
        }
    }

    toggleSearchBar()
    {
        this.search = !this.search;
        if(this.search)
            this.content.scrollToTop();
    }

    onInput(event)
    {
        this.nomeSearch = event.target.value;
        this.pageSearch = 0;
        this.hasNextPageSearch = false;
        this.scrollEnabled = true;

        this.getEventos(
            {
                nome: this.nomeSearch,
                cidade: this.cidadeSearch,
                estado: this.estadoSearch
            },
            0
        );
    }

    getEventos(
        {
            nome,
            cidade,
            estado
        },
        page: number)
    {
        this.searching = true;
        if(!this.loader)
        {
            this.loader = this.loadingCtrl.create(
                {
                    content: 'Por favor, aguarde.'
                }
            );
            this.loader.present();
        }
        if(this.pageSearch == 0)
            this.eventos = [];
        this.eventoProvider.getEventos(
            {
                nome: (nome ? nome : ''),
                cidade: (cidade ? cidade : ''),
                estado: (estado ? estado : '')
            },
            page
        ).subscribe(
            (response) => {
                this.searching = false;
                this.loader.dismiss();
                console.log(response);
                this.pageSearch = response['data']['page'];
                this.totalPages = response['data']['totalPages'];
                this.hasNextPageSearch = !response['data']['last'];
                response['data']['content'].forEach(
                    (item) => {
                        let dataRealizacaoStr = '';
                        dataRealizacaoStr = item['dataRealizacao'].split('T')[0].split('-')[2] + '/' + item['dataRealizacao'].split('T')[0].split('-')[1] + '/' + item['dataRealizacao'].split('T')[0].split('-')[0] + ' às ' + item['dataRealizacao'].split('T')[1].split('.')[0].split(':')[0] + ':' + item['dataRealizacao'].split('T')[1].split('.')[0].split(':')[1];
                        item['dataRealizacao'] = dataRealizacaoStr;
                        this.eventos.push(item);
                    }
                );
            },
            (error) => {
                this.searching = false;
                this.loader.dismiss();

                const alert = this.alertCtrl.create(
                    {
                        title: 'Mensagem',
                        subTitle: 'Não foi possível consultar os eventos.',
                        buttons: ['OK']
                    }
                );
                alert.present();
            }
        );
    }

    scrollEventos(infiniteScroll)
    {
        setTimeout(
            () => {
                if(this.hasNextPageSearch)
                {
                    this.pageSearch += 1;
                    this.getEventos(
                        {
                            nome: this.nomeSearch,
                            cidade: this.cidadeSearch,
                            estado: this.estadoSearch
                        },
                        this.pageSearch
                    );
                }
                if(this.pageSearch + 1 >= this.totalPages)
                {
                    this.scrollEnabled = false;
                    // infiniteScroll.enabled = false;
                }
                infiniteScroll.complete();

            },
            500
        );
    }

    ionViewWillEnter()
    {
        this.searching = true;
        this.loader = this.loadingCtrl.create(
            {
                content: 'Por favor, aguarde'
            }
        );
        this.loader.present();
        this.geolocation.getCurrentPosition().then(
            (resp) => {
                let req: GeocoderRequest = {
                    position: {
                        lat: resp.coords.latitude,
                        lng: resp.coords.longitude
                    }
                };
                Geocoder.geocode(req).then(
                    (results)=> {
                        if(results)
                        {
                            this.cidadeSearch = results[0]['subAdminArea'];
                            this.estadoSearch = results[0]['adminArea'];
                            switch(this.estadoSearch)
                            {
                                case 'Minas Gerais':
                                    this.estadoSearch = 'MG';
                                    break;
                                case 'São Paulo':
                                    this.estadoSearch = 'SP';
                                    break;
                                case 'Rio de Janeiro':
                                    this.estadoSearch = 'RJ';
                                    break;
                                case 'Espírito Santo':
                                    this.estadoSearch = 'ES';
                                    break;
                                case 'Bahia':
                                    this.estadoSearch = 'BA';
                                    break;
                                case 'Rio Grande do Sul':
                                    this.estadoSearch = 'RS';
                                    break;
                                case 'Rio Grande do Norte':
                                    this.estadoSearch = 'RN';
                                    break;
                                case 'Santa Catarina':
                                    this.estadoSearch = 'SC';
                                    break;
                                case 'Paraná':
                                    this.estadoSearch = 'PR';
                                    break;
                                case 'Pará':
                                    this.estadoSearch = 'PA';
                                    break;
                                case 'Piauí':
                                    this.estadoSearch = 'PI';
                                    break;
                                case 'Pernambuco':
                                    this.estadoSearch = 'PE';
                                    break;
                                case 'Sergipe':
                                    this.estadoSearch = 'SE';
                                    break;
                                case 'Acre':
                                    this.estadoSearch = 'AC';
                                    break;
                                case 'Alagoas':
                                    this.estadoSearch = 'AL';
                                    break;
                                case 'Amapá':
                                    this.estadoSearch = 'AP';
                                    break;
                                case 'Amazonas':
                                    this.estadoSearch = 'AM';
                                    break;
                                case 'Ceará':
                                    this.estadoSearch = 'CE';
                                    break;
                                case 'Distrito Federal':
                                    this.estadoSearch = 'DF';
                                    break;
                                case 'Goiás':
                                    this.estadoSearch = 'GO';
                                    break;
                                case 'Maranhão':
                                    this.estadoSearch = 'MA';
                                    break;
                                case 'Mato Grosso':
                                    this.estadoSearch = 'MT';
                                    break;
                                case 'Mato Grosso do Sul':
                                    this.estadoSearch = 'MS';
                                    break;
                                case 'Paraíba':
                                    this.estadoSearch = 'PB';
                                    break;
                                case 'Rondônia':
                                    this.estadoSearch = 'RO';
                                    break;
                                case 'Roraima':
                                    this.estadoSearch = 'RR';
                                    break;
                                case 'Tocantins':
                                    this.estadoSearch = 'TO';
                                    break;
                                default:
                                    this.estadoSearch = '';
                                    break;
                            }
                            this.getEventos(
                                {
                                    nome: '',
                                    cidade: this.cidadeSearch,
                                    estado: this.estadoSearch
                                },
                                0);
                        } else{
                            this.cidadeSearch = '';
                            this.estadoSearch = '';
                        }
                    }
                );
            }
        ).catch(
            (error) => {
                this.loader.dismiss();
                const alert = this.alertCtrl.create(
                    {
                        title: 'Aviso',
                        subTitle: 'Não foi possível determinar sua localização. Utilize os filtros.',
                        buttons: ['OK']
                    }
                );
                alert.present();
                this.getEventos(
                    {
                        nome: '',
                        cidade: this.cidadeSearch,
                        estado: this.estadoSearch
                    },
                    0
                );
            }
        );
    }

    showModal(evento)
    {
        let modal = this.modalCtrl.create(EventoModalPage, {evento});
        modal.present();

    }
}
