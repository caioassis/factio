import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UsuarioCreatePage } from "../usuario-create/usuario-create";
import { LoginPage } from "../login/login";

@IonicPage()
@Component({
    selector: 'page-base',
    templateUrl: 'base.html',
})
export class BasePage {

    usuarioCreatePage: any = UsuarioCreatePage;
    loginPage: any = LoginPage;

    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }

}
