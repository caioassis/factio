import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { LoginPage } from '../login/login';
import { VagaProvider } from "../../providers/vaga/vaga";
import { Vaga } from "../../models/vaga";
import { HomePage } from "../home/home";
import {VarsProvider} from "../../providers/vars/vars";


@IonicPage()
@Component({
    selector: 'page-vaga',
    templateUrl: 'vaga.html',
})
export class VagaPage
{
    private vagaForm : FormGroup;
    private dadosUsuario: any;
    vaga: Vaga;

    constructor(public navCtrl: NavController, public navParams: NavParams, private loadingCtrl: LoadingController,
                private formBuilder: FormBuilder, private vagaProvider: VagaProvider, public alertCtrl: AlertController, private storage: Storage,
                private varsProvider: VarsProvider)
    {
        if(this.navParams.get('mode') === 'edit')
            this.vaga = this.navParams.get('vaga');
        else
            this.vaga = null;
        this.dadosUsuario = this.varsProvider.getUsuario();
        if(this.dadosUsuario)
        {
            this.vagaProvider.setHeaderToken(this.dadosUsuario.accessToken);
        } else
        {
            this.navCtrl.setRoot(LoginPage);
            this.navCtrl.popToRoot();
        }
        let nomeResponsavel = (this.dadosUsuario ? this.dadosUsuario.usuario.nome : '');
        let qtdQuartos = (this.vaga ? this.vaga.qtdQuartos : '');
        let descricao = (this.vaga ? this.vaga.descricao : '');
        let publicoAlvo = (this.vaga ? this.vaga.publicoAlvo : 'todos');
        let observacoes = (this.vaga ? this.vaga.observacoes : '');

        this.vagaForm = this.formBuilder.group(
            {
                nomeResponsavel: [
                    nomeResponsavel,
                    Validators.compose([Validators.required])
                ],
                qtdQuartos: [
                    qtdQuartos,
                    Validators.compose(
                        [

                            Validators.min(1),
                            Validators.max(10),
                            Validators.required
                        ]
                    )
                ],
                descricao: [
                    descricao,
                    Validators.compose(
                        [
                            Validators.minLength(1),
                            Validators.maxLength(200),
                            Validators.required
                        ]
                    )
                ],
                publicoAlvo: [
                    publicoAlvo,
                    Validators.compose([Validators.required])
                ],
                observacoes: [
                    observacoes,
                    Validators.compose([Validators.maxLength(200)])
                ],
            }
        );
    }

    onSubmit()
    {
        const loader = this.loadingCtrl.create(
            {
                content: "Por favor, aguarde."
            }
        );
        loader.present();

        let data = {
            criadorId: this.dadosUsuario.usuario.id,
            descricao: this.vagaForm.value.descricao,
            qtdQuartos: this.vagaForm.value.qtdQuartos,
            publicoAlvo: this.vagaForm.value.publicoAlvo,
            moradiaId: this.dadosUsuario.usuario.moradia.id,
            observacoes: this.vagaForm.value.observacoes
        };

        this.vagaProvider.addVaga(
            data as Vaga
        ).subscribe(
            (response) => {

                if(response['meta']['messageCode'] === "msg.anunciovaga.registrado")
                {
                    loader.dismiss();
                    const alert = this.alertCtrl.create(
                        {
                            title: 'Vaga Criada!',
                            subTitle: 'Vaga criada com sucesso.',
                            buttons: [
                                {
                                    text: 'OK',
                                    handler: () => {
                                        this.navCtrl.setRoot(HomePage);
                                        this.navCtrl.popToRoot();
                                    }
                                }
                            ]
                        }
                    );
                    alert.present();

                } else
                {
                    const alert = this.alertCtrl.create(
                        {
                            title: 'Aviso',
                            subTitle: 'Mensagem: ' + response['meta']['messageCode'],
                            buttons: ['OK']
                        }
                    );
                    alert.present();

                }

            },
            (error) => {
                loader.dismiss();
                if(error['meta']['messageCode'] === "msg.apelido.ja.utilizado")
                {
                    const alert = this.alertCtrl.create(
                        {
                            title: 'Aviso',
                            subTitle: 'Já existe uma Moradia com este nome.',
                            buttons: ['OK']
                        }
                    );
                    alert.present();
                } else
                {
                    const alert = this.alertCtrl.create(
                        {
                            title: 'Aviso',
                            subTitle: 'Não foi possível criar a vaga.',
                            buttons: ['OK']
                        }
                    );
                    alert.present();
                }
            }
        );
    }
}
