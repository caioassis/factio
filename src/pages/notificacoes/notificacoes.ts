import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import { VarsProvider } from "../../providers/vars/vars";
import { MoradiaProvider } from "../../providers/moradia/moradia";
import {UsuarioProvider} from "../../providers/usuario/usuario";

@IonicPage()
@Component({
    selector: 'page-notificacoes',
    templateUrl: 'notificacoes.html',
})
export class NotificacoesPage
{
    dadosUsuario: any;
    notificacoes: Array<{}> = [];
    searching = false;

    constructor(public navCtrl: NavController, public navParams: NavParams, private varsProvider: VarsProvider,
                private moradiaProvider: MoradiaProvider, private loadingCtrl: LoadingController,
                private usuarioProvider: UsuarioProvider, private alertCtrl: AlertController)
    {
        this.dadosUsuario = this.varsProvider.getUsuario();
        this.moradiaProvider.setHeaderToken(this.dadosUsuario['accessToken']);

    }

    ionViewWillEnter()
    {
        this.searching = true;
        const loader = this.loadingCtrl.create(
            {
                content: 'Por favor, aguarde.'
            }
        );
        loader.present();
        this.moradiaProvider.listarSolicitacoesIngressoMoradia(
            this.dadosUsuario.usuario.moradia.id
        ).subscribe(
            (response) => {
                this.searching = false;
                loader.dismiss();
                if(response['meta']['messageCode'] === "msg.servico.execucao.sucesso")
                {
                    response['data']['solicitacoes'].forEach(
                        (item) => {

                            this.notificacoes.push(item);
                        }
                    );
                } else
                {


                }
            },
            (error) => {
                this.searching = false;
            }
        );
    }

    aceitarSolicitacao(notificacao)
    {
        console.log(notificacao);
        const alert = this.alertCtrl.create(
            {
                title: 'Confirmação',
                subTitle: 'Deseja realmente aceitar a solicitação de ingresso do Usuário ' + notificacao['nomeUsuario'] + '?',
                buttons: [
                    {
                        text: 'Sim',
                        handler: () => {
                            const loader = this.loadingCtrl.create(
                                {
                                    content: 'Por favor, aguarde.'
                                }
                            );
                            loader.present();
                            this.moradiaProvider.addMembroAMoradia(
                                notificacao['moradiaId'],
                                notificacao['usuarioId']
                            ).subscribe(
                                (response) => {
                                    loader.dismiss();
                                    let index = this.notificacoes.indexOf(notificacao);
                                    this.notificacoes.splice(index, 1);
                                    let alert = this.alertCtrl.create(
                                        {
                                            title: 'Sucesso!',
                                            subTitle: 'O usuário foi inserido na moradia.',
                                            buttons: ['OK']
                                        }
                                    );
                                    alert.present();
                                },
                                (error) => {
                                    console.log(error);
                                    loader.dismiss();
                                    let index = this.notificacoes.indexOf(notificacao);
                                    this.notificacoes.splice(index, 1);
                                    let alert = this.alertCtrl.create(
                                        {
                                            title: 'Aviso',
                                            subTitle: 'Não foi possível realizar a operação.',
                                            buttons: ['OK']
                                        }
                                    );
                                    alert.present();
                                }
                            );
                        }
                    },
                    {
                        text: 'Não',
                        handler: () => {}
                    }
                ]
            }
        );
        alert.present();
    }

    recusarSolicitacao(notificacao)
    {
        const alert = this.alertCtrl.create(
            {
                title: 'Confirmação',
                subTitle: 'Deseja realmente remover a solicitação de ingresso do Usuário ' + notificacao['nomeUsuario'] + '?',
                buttons: [
                    {
                        text: 'Sim',
                        handler: () => {

                            const loader = this.loadingCtrl.create(
                                {
                                    content: 'Por favor, aguarde.'
                                }
                            );
                            loader.present();
                            this.moradiaProvider.removerSolicitacao(
                                notificacao['moradiaId'],
                                notificacao['usuarioId']
                            ).subscribe(
                                (response) => {
                                    loader.dismiss();
                                    let index = this.notificacoes.indexOf(notificacao);
                                    this.notificacoes.splice(index, 1);
                                    let alert = this.alertCtrl.create(
                                        {
                                            title: 'Sucesso!',
                                            subTitle: 'Solicitação de ingresso recusada.',
                                            buttons: ['OK']
                                        }
                                    );
                                    alert.present();
                                },
                                (error) => {
                                    loader.dismiss();
                                    let index = this.notificacoes.indexOf(notificacao);
                                    this.notificacoes.splice(index, 1);
                                    let alert = this.alertCtrl.create(
                                        {
                                            title: 'Aviso',
                                            subTitle: 'Não foi possível realizar a operação.',
                                            buttons: ['OK']
                                        }
                                    );
                                    alert.present();
                                }
                            );
                        }
                    },
                    {
                        text: 'Não',
                        handler: () => {}
                    }
                ]
            }
        );
        alert.present();
    }
}
