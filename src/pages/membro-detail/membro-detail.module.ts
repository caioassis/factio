import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MembroDetailPage } from './membro-detail';

@NgModule({
  declarations: [
    MembroDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(MembroDetailPage),
  ],
})
export class MembroDetailPageModule {}
