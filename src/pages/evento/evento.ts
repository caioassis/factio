import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { LoadingController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { EventoProvider } from "../../providers/evento/evento";
import { VarsProvider } from "../../providers/vars/vars";
import {LoginPage} from "../login/login";
import {Evento} from "../../models/evento";

@IonicPage()
@Component({
  selector: 'page-evento',
  templateUrl: 'evento.html',
})
export class EventoPage {

    private eventoForm : FormGroup;
    dadosUsuario: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder,
                private eventoProvider: EventoProvider, private loadingCtrl: LoadingController,
                private alertCtrl: AlertController, private toastCtrl: ToastController,
                private varsProvider: VarsProvider)
    {
        this.dadosUsuario = this.varsProvider.getUsuario();
        if(this.dadosUsuario)
        {
            this.eventoProvider.setHeaderToken(this.dadosUsuario['accessToken']);
        } else
        {
            this.navCtrl.setRoot(LoginPage);
            this.navCtrl.popToRoot();
        }
        this.eventoForm = this.formBuilder.group(
            {
                nome: ['', Validators.required],
                descricao: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
                telefoneContato: ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(11)])],
                atracoes: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
                dataRealizacao: ['', Validators.required],
                valorEntrada: [null, Validators.compose([Validators.required])]
            }
        );
    }

    onSubmit()
    {
        const loader = this.loadingCtrl.create(
            {
                content: "Por favor, aguarde."
            }
        );
        loader.present();

        let data = {
            nome: this.eventoForm.value.nome,
            descricao: this.eventoForm.value.descricao,
            dataRealizacao: this.eventoForm.value.dataRealizacao,
            telefoneContato: this.eventoForm.value.telefoneContato,
            atracoes: this.eventoForm.value.atracoes,
            valorEntrada: this.eventoForm.value.valorEntrada,
            criadorId: this.dadosUsuario['usuario']['id'],
            moradiaId: this.dadosUsuario['usuario']['moradia']['id']
        };
        this.eventoProvider.addEvento(
            data as Evento
        ).subscribe(
            (response) => {
                console.log('ADD Evento Response', response);
                if(response['meta']['messageCode'] === "msg.anuncioevento.registrado")
                {
                    loader.dismiss();
                    const alert = this.alertCtrl.create(
                        {
                            title: 'Evento Criado!',
                            subTitle: 'Evento criado com sucesso.',
                            buttons: [
                                {
                                    text: 'OK',
                                    handler: () => {
                                        this.navCtrl.pop();
                                    }
                                }
                            ]
                        }
                    );
                    alert.present();
                } else
                {
                    loader.dismiss();
                    const alert = this.alertCtrl.create(
                        {
                            title: 'Mensagem',
                            subTitle: response['meta']['messageCode'],
                            buttons: [
                                {
                                    text: 'OK',
                                    handler: () => {
                                        this.navCtrl.pop();
                                    }
                                }
                            ]
                        }
                    );
                    alert.present();
                }

            },
            (error) => {
                console.log('ADD Evento ERROR', error);
                loader.dismiss();
                const alert = this.alertCtrl.create(
                    {
                        title: 'Aviso',
                        subTitle: 'Não foi possível criar o evento.',
                        buttons: [
                            {
                                text: 'OK',
                                handler: () => {
                                    this.navCtrl.pop();
                                }
                            }
                        ]
                    }
                );
                alert.present();
            }
        );
    }
}
