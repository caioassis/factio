import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LocalizacaoMembrosPage } from './localizacao-membros';

@NgModule({
  declarations: [
    LocalizacaoMembrosPage,
  ],
  imports: [
    IonicPageModule.forChild(LocalizacaoMembrosPage),
  ],
})
export class LocalizacaoMembrosPageModule {}
