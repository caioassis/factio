import { Component } from '@angular/core';
import { IonicPage, LoadingController, ModalController, NavController, NavParams} from 'ionic-angular';
import { MoradiaProvider } from "../../providers/moradia/moradia";
import { VarsProvider } from "../../providers/vars/vars";
import { LoginPage } from "../login/login";
import { Usuario } from "../../models/usuario";
import { LocalizacaoModalPage } from "../localizacao-modal/localizacao-modal";

@IonicPage()
@Component({
    selector: 'page-localizacao-membros',
    templateUrl: 'localizacao-membros.html',
})
export class LocalizacaoMembrosPage
{
    dadosUsuario: any;
    membros: Array<Usuario> = [];
    todosMembros: Array<Usuario> = [];
    searching = false;

    constructor(public navCtrl: NavController, public navParams: NavParams,
                private moradiaProvider: MoradiaProvider, private varsProvider: VarsProvider,
                private loadingCtrl: LoadingController, private modalCtrl: ModalController)
    {
        this.dadosUsuario = this.varsProvider.getUsuario();
        console.log(this.dadosUsuario);
        if(this.dadosUsuario)
        {
            this.moradiaProvider.setHeaderToken(this.dadosUsuario['accessToken']);
        } else
        {
           this.navCtrl.setRoot(LoginPage);
            this.navCtrl.popToRoot();
        }
    }

    ionViewWillEnter()
    {
        this.searching = true;
        const loader = this.loadingCtrl.create(
            {
                content: 'Por favor, aguarde.'
            }
        );
        loader.present();
        this.membros = [];
        this.moradiaProvider.getMembrosDaMoradia(
            this.dadosUsuario['usuario']['moradia']['id']
        ).subscribe(
            (response) => {
                this.searching = false;
                if(response['meta']['messageCode'] === "msg.servico.execucao.sucesso")
                {
                    response['data']['usuarios'].forEach(
                        (usuario) => {
                            let ultimoLoginStr = usuario['ultimoLogin'].split('T')[0].split('-')[2] + '/' + usuario['ultimoLogin'].split('T')[0].split('-')[1] + '/' + usuario['ultimoLogin'].split('T')[0].split('-')[0] + ' às ' + usuario['ultimoLogin'].split('T')[1].split('.')[0].split(':')[0] + ':' + usuario['ultimoLogin'].split('T')[1].split('.')[0].split(':')[1];
                            usuario['ultimoLogin'] = ultimoLoginStr;
                            this.todosMembros.push(usuario);
                            if(usuario['id'] !== this.dadosUsuario['usuario']['id'])
                                this.membros.push(usuario);
                        }
                    );
                    loader.dismiss()
                } else
                {
                    loader.dismiss();

                }
            },
            (error) => {
                this.searching = false;
                console.log('error', error);
                loader.dismiss();
            }
        );
    }

    showModal(membros)
    {
        let modal = this.modalCtrl.create(LocalizacaoModalPage, {membros});
        modal.present();
    }
}
