import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoradiaPage } from './moradia';

@NgModule({
  declarations: [
    MoradiaPage,
  ],
  imports: [
    IonicPageModule.forChild(MoradiaPage),
  ],
})
export class MoradiaPageModule {}
