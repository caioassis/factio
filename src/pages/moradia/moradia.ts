import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { MoradiaProvider } from "../../providers/moradia/moradia";
import { Moradia } from "../../models/moradia";
import { HomePage } from "../home/home";
import {VarsProvider} from "../../providers/vars/vars";


@IonicPage()
@Component({
    selector: 'page-moradia',
    templateUrl: 'moradia.html',
})
export class MoradiaPage
{
    private moradiaForm : FormGroup;
    private dadosUsuario: any;
    moradia: Moradia;

    constructor(public navCtrl: NavController, public navParams: NavParams, private loadingCtrl: LoadingController,
                private formBuilder: FormBuilder, private moradiaProvider: MoradiaProvider, public alertCtrl: AlertController, private storage: Storage,
                private varsProvider: VarsProvider)
    {
        if(this.navParams.get('mode') === 'edit')
            this.moradia = this.navParams.get('moradia');
        else
            this.moradia = null;

        this.dadosUsuario = this.varsProvider.getUsuario();
        if(this.dadosUsuario)
        {
            this.moradiaProvider.setHeaderToken(this.dadosUsuario.accessToken);
            let nome = (this.moradia ? this.moradia.nome : '');
            let cep = (this.moradia ? this.moradia.cep : '');
            let endereco = (this.moradia ? this.moradia.endereco : '');
            let numero = (this.moradia ? this.moradia.numero : '');
            let complemento = (this.moradia ? this.moradia.complemento : '');
            let uf = (this.moradia ? this.moradia.estado : '');
            let cidade = (this.moradia ? this.moradia.cidade : '');
            let bairro = (this.moradia ? this.moradia.bairro : '');
            let observacoes = (this.moradia ? this.moradia.comentario : '');

            this.moradiaForm = this.formBuilder.group(
                {
                    nome: [
                        nome,
                        Validators.compose(
                            [
                                Validators.minLength(1),
                                Validators.maxLength(40),
                                Validators.required
                            ]
                        )
                    ],
                    cep: [
                        cep,
                        Validators.compose(
                            [
                                Validators.required,
                                Validators.minLength(8),
                                Validators.maxLength(8)
                            ]
                        )
                    ],
                    endereco: [
                        endereco,
                        Validators.compose(
                            [
                                Validators.minLength(1),
                                Validators.maxLength(80),
                                Validators.required
                            ]
                        )
                    ],
                    numero: [
                        numero,
                        Validators.compose(
                            [
                                Validators.minLength(1),
                                Validators.maxLength(11),
                                Validators.required
                            ]
                        )
                    ],
                    complemento: [complemento, Validators.compose([Validators.maxLength(40)])],
                    uf: [uf, Validators.required],
                    cidade: [
                        cidade,
                        Validators.compose(
                            [
                                Validators.minLength(1),
                                Validators.maxLength(40),
                                Validators.required
                            ]
                        )
                    ],
                    bairro: [
                        bairro,
                        Validators.compose(
                            [
                                Validators.minLength(1),
                                Validators.maxLength(40),
                                Validators.required
                            ]
                        )
                    ],
                    observacoes: [observacoes, Validators.compose([Validators.maxLength(200)])]
                }
            );
        } else
        {
            this.navCtrl.setRoot(HomePage);
            this.navCtrl.popToRoot();
        }
    }

    slugify(text)
    {
        return text.toString().toLowerCase()
            .replace(/\s+/g, '-')           // Replace spaces with -
            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '');            // Trim - from end of text
    }

    onSubmit()
    {
        const loader = this.loadingCtrl.create(
            {
                content: "Por favor, aguarde."
            }
        );
        loader.present();

        let data = {
            nome: this.moradiaForm.value.nome,
            apelido: this.slugify(this.moradiaForm.value.nome),
            cep: this.moradiaForm.value.cep,
            endereco: this.moradiaForm.value.endereco,
            numero: this.moradiaForm.value.numero,
            complemento: this.moradiaForm.value.complemento,
            estado: this.moradiaForm.value.uf,
            cidade: this.moradiaForm.value.cidade,
            bairro: this.moradiaForm.value.bairro,
            comentario: this.moradiaForm.value.observacoes,
            responsavel: this.dadosUsuario.usuario.id
        };

        if(this.moradia) // Editar Moradia
        {
            this.moradiaProvider.updateMoradia(
                {
                    ...data,
                    id: this.moradia.id
                } as Moradia
            ).subscribe(
                (response) => {
                    if(response['meta']['messageCode'] === "msg.moradia.atualizada")
                    {
                        console.log('Moradia Atualizada');
                        this.moradiaProvider.getMoradiaPeloUsuario(
                            this.dadosUsuario.usuario.id
                        ).subscribe(
                            (res) => {
                                if(res['meta']['messageCode'] === "msg.servico.execucao.sucesso")
                                {
                                    let moradia = res['data'];
                                    this.dadosUsuario['usuario']['moradia'] = {
                                        id: moradia['id'],
                                        nome: moradia['nome'],
                                        endereco: moradia['endereco'],
                                        numero: moradia['numero'],
                                        complemento: moradia['complemento'],
                                        bairro: moradia['bairro'],
                                        cidade: moradia['cidade'],
                                        estado: moradia['estado'],
                                        cep: moradia['cep'],
                                        responsavel: this.dadosUsuario.usuario.id,
                                    };
                                    this.varsProvider.setUsuario(this.dadosUsuario);
                                    this.storage.set('dados', JSON.stringify(this.dadosUsuario));
                                }
                                loader.dismiss();
                                const alert = this.alertCtrl.create(
                                    {
                                        title: 'Moradia Atualizada!',
                                        subTitle: 'Moradia atualizada com sucesso.',
                                        buttons: [
                                            {
                                                text: 'OK',
                                                handler: () => {
                                                    this.navCtrl.pop();
                                                }
                                            }
                                        ]
                                    }
                                );
                                alert.present();
                            },
                            (err) => {
                                loader.dismiss();
                                const alert = this.alertCtrl.create(
                                    {
                                        title: 'Aviso',
                                        subTitle: 'Nnão foi possível atualizar a moradia.',
                                        buttons: [
                                            {
                                                text: 'OK',
                                                handler: () => {
                                                    this.navCtrl.pop();
                                                }
                                            }
                                        ]
                                    }
                                );
                                alert.present();
                            }
                        );
                    } else
                    {

                    }
                },
                (error) => {
                    loader.dismiss();
                    if(error['meta']['messageCode'] === "msg.apelido.ja.utilizado")
                    {
                        const alert = this.alertCtrl.create(
                            {
                                title: 'Aviso',
                                subTitle: 'Já existe uma Moradia com este nome.',
                                buttons: ['OK']
                            }
                        );
                        alert.present();
                    } else
                    {
                        const alert = this.alertCtrl.create(
                            {
                                title: 'Aviso',
                                subTitle: 'Não foi possível atualizar a Moradia.',
                                buttons: ['OK']
                            }
                        );
                        alert.present();
                    }
                }
            );

        } else // Adicionar Moradia
        {
            this.moradiaProvider.addMoradia(
                data as Moradia
            ).subscribe(
                (response) => {
                    if(response['meta']['messageCode'] === "msg.moradia.registrada")
                    {
                        this.moradiaProvider.getMoradiaPeloUsuario(
                            this.dadosUsuario.usuario.id
                        ).subscribe(
                            (res) => {
                                if(res['meta']['messageCode'] === "msg.servico.execucao.sucesso")
                                {
                                    let moradia = res['data'];
                                    this.dadosUsuario['usuario']['moradia'] = {
                                        id: moradia['id'],
                                        nome: moradia['nome'],
                                        endereco: moradia['endereco'],
                                        numero: moradia['numero'],
                                        complemento: moradia['complemento'],
                                        bairro: moradia['bairro'],
                                        cidade: moradia['cidade'],
                                        estado: moradia['estado'],
                                        cep: moradia['cep'],
                                        responsavel: this.dadosUsuario.usuario.id,
                                    };
                                    this.varsProvider.setUsuario(this.dadosUsuario);
                                    this.storage.set('dados', JSON.stringify(this.dadosUsuario));
                                }
                                loader.dismiss();
                                const alert = this.alertCtrl.create(
                                    {
                                        title: 'Moradia Criada!',
                                        subTitle: 'Moradia criada com sucesso.',
                                        buttons: [
                                            {
                                                text: 'OK',
                                                handler: () => {
                                                    this.navCtrl.setRoot(HomePage);
                                                    this.navCtrl.popToRoot();
                                                }
                                            }
                                        ]
                                    }
                                );
                                alert.present();
                            },
                            (err) => {
                                loader.dismiss();
                                const alert = this.alertCtrl.create(
                                    {
                                        title: 'Aviso!',
                                        subTitle: 'Não foi possível criar a moradia.',
                                        buttons: [
                                            {
                                                text: 'OK',
                                                handler: () => {
                                                    this.navCtrl.setRoot(HomePage);
                                                    this.navCtrl.popToRoot();
                                                }
                                            }
                                        ]
                                    }
                                );
                                alert.present();
                            }
                        );

                    } else if (response['meta']['messageCode'] === "msg.usuario.vinculado.moradia")
                    {
                        const alert = this.alertCtrl.create(
                            {
                                title: 'Aviso',
                                subTitle: 'Seu Usuário já está vinculado a uma Moradia.',
                                buttons: ['OK']
                            }
                        );
                        alert.present();
                    } else
                    {
                        const alert = this.alertCtrl.create(
                            {
                                title: 'Aviso',
                                subTitle: 'Mensagem: ' + response['meta']['messageCode'],
                                buttons: ['OK']
                            }
                        );
                        alert.present();
                    }

                },
                (error) => {
                    loader.dismiss();
                    if(error['meta']['messageCode'] === "msg.apelido.ja.utilizado")
                    {
                        const alert = this.alertCtrl.create(
                            {
                                title: 'Aviso',
                                subTitle: 'Já existe uma Moradia com este nome.',
                                buttons: ['OK']
                            }
                        );
                        alert.present();
                    } else
                    {
                        const alert = this.alertCtrl.create(
                            {
                                title: 'Aviso',
                                subTitle: 'Não foi possível criar a Moradia.',
                                buttons: ['OK']
                            }
                        );
                        alert.present();
                    }
                }
            );
        }
    }

    consultarCEP(e)
    {
        let value = e.target.value;
        if (value.length !== 8)
            return;

        const loader = this.loadingCtrl.create(
            {
                content: "Consultando dados do CEP."
            }
        );
        loader.present();
        this.moradiaProvider.consultarCEP(value).subscribe(
            data => {
                if(data['erro'])
                {
                    loader.dismiss();
                    this.moradiaForm.controls.endereco.setValue('');
                    this.moradiaForm.controls.bairro.setValue('');
                    this.moradiaForm.controls.cidade.setValue('');
                    this.moradiaForm.controls.uf.setValue('');
                    const alert = this.alertCtrl.create(
                        {
                            title: 'Aviso',
                            subTitle: 'Não foi possível consultar o CEP. Verifique se o digitou corretamente e tente novamente.',
                            buttons: ['OK']
                        }
                    );
                    alert.present();
                } else
                {
                    this.moradiaForm.controls.endereco.setValue(data['logradouro']);
                    this.moradiaForm.controls.bairro.setValue(data['bairro']);
                    this.moradiaForm.controls.cidade.setValue(data['localidade']);
                    this.moradiaForm.controls.uf.setValue(data['uf'].toUpperCase());
                    loader.dismiss();
                }
            },
            error => {
                loader.dismiss();
                const alert = this.alertCtrl.create(
                    {
                        title: 'Aviso',
                        subTitle: 'Não foi possível consultar o CEP. Verifique sua conexão com a Internet e tente novamente',
                        buttons: ['OK']
                    }
                );
                alert.present();
            }
        );
    }
}
