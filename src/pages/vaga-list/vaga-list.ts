import { Component, ViewChild } from '@angular/core';
import {Content, IonicPage, Loading, NavController, NavParams} from 'ionic-angular';
import { Vaga } from "../../models/vaga";
import { VagaProvider } from "../../providers/vaga/vaga";
import { LoadingController } from "ionic-angular";
import { LoginPage } from "../login/login";
import { AlertController } from 'ionic-angular';
import { ModalController } from "ionic-angular";
import { Storage } from '@ionic/storage';
import { Geolocation } from "@ionic-native/geolocation";
import { Geocoder, GeocoderRequest } from "@ionic-native/google-maps";
import { VagaModalPage } from "../vaga-modal/vaga-modal";
import { VagaPage } from "../vaga/vaga";
import {VarsProvider} from "../../providers/vars/vars";

@IonicPage()
@Component({
    selector: 'page-vaga-list',
    templateUrl: 'vaga-list.html',
})
export class VagaListPage
{
    @ViewChild(Content) content: Content;

    vagaModalPage = VagaModalPage;
    private dadosUsuario: any;
    vagas: Array<Vaga> = [];
    vagaPage = VagaPage;
    search = false;
    searching = false;
    pageSearch: number = 0;
    totalPages: number;
    hasNextPageSearch: boolean = false;
    nomeSearch: string = '';
    cidadeSearch: string = '';
    estadoSearch: string = '';
    scrollEnabled = true;
    loader: Loading;

    constructor(public navCtrl: NavController, public navParams: NavParams, private vagaProvider: VagaProvider,
                private loadingCtrl: LoadingController,
                public alertCtrl: AlertController, private storage: Storage, private geolocation: Geolocation,
                private modalCtrl: ModalController, private varsProvider: VarsProvider)
    {
        this.dadosUsuario = this.varsProvider.getUsuario();
        if(this.dadosUsuario)
        {
            this.vagaProvider.setHeaderToken(this.dadosUsuario['accessToken']);
        } else
        {
            this.navCtrl.setRoot(LoginPage);
            this.navCtrl.popToRoot();
        }
    }

    toggleSearchBar()
    {
        this.search = !this.search;
        if(this.search)
            this.content.scrollToTop();
    }

    onInput(event)
    {
        this.nomeSearch = event.target.value;
        this.pageSearch = 0;
        this.hasNextPageSearch = false;
        this.scrollEnabled = true;

        this.getVagas(
            {
                nome: this.nomeSearch,
                cidade: this.cidadeSearch,
                estado: this.estadoSearch
            },
            0
        );
    }

    showModal(vaga)
    {
        let modal = this.modalCtrl.create(VagaModalPage, {vaga, usuario: this.dadosUsuario});
        modal.present();
    }

    getVagas({nome, cidade, estado}, page: number)
    {
        this.searching = true;
        if(!this.loader)
        {
            this.loader = this.loadingCtrl.create(
                {
                    content: 'Por favor, aguarde.'
                }
            );
            this.loader.present();
        }

        if(this.pageSearch == 0)
            this.vagas = [];
        this.vagaProvider.getVagas(
            {
                nome: (nome ? nome : ''),
                cidade: (cidade ? cidade : ''),
                estado: (estado ? estado : '')
            },
            page
        ).subscribe(
            (response) => {
                console.log(response);
                this.searching = false;
                // this.pageSearch = response['data']['page'];
                // this.totalPages = response['data']['totalPages'];
                // this.hasNextPageSearch = !response['data']['last'];
                response['data']['content'].forEach(
                    (item) => {
                        this.vagas.push(item)
                    }
                );
                this.loader.dismiss();
            },
            (error) => {
                this.searching = false;
                this.loader.dismiss();
                const alert = this.alertCtrl.create(
                    {
                        title: 'Mensagem',
                        subTitle: 'Não foi possível consultar as vagas.',
                        buttons: ['OK']
                    }
                );
                alert.present();
            }
        );
    }

    scrollVagas(infiniteScroll)
    {
        setTimeout(
            () => {
                if(this.hasNextPageSearch)
                {
                    this.pageSearch += 1;
                    this.getVagas(
                        {
                            nome: this.nomeSearch,
                            cidade: this.cidadeSearch,
                            estado: this.estadoSearch
                        },
                        this.pageSearch
                    );
                }
                if(this.pageSearch + 1 >= this.totalPages)
                {
                    this.scrollEnabled = false;
                    // infiniteScroll.enabled = false;
                }
                infiniteScroll.complete();

            },
            500
        );
    }

    ionViewWillEnter()
    {
        this.searching = true;
        this.loader = this.loadingCtrl.create(
            {
                content: 'Por favor, aguarde.'
            }
        );
        this.loader.present();
        this.geolocation.getCurrentPosition().then(
            (resp) => {
                let req: GeocoderRequest = {
                    position: {
                        lat: resp.coords.latitude,
                        lng: resp.coords.longitude
                    }
                };
                Geocoder.geocode(req).then(
                    (results)=> {
                        if(results)
                        {
                            this.cidadeSearch = results[0]['subAdminArea'];
                            this.estadoSearch = results[0]['adminArea'];
                            switch(this.estadoSearch)
                            {
                                case 'Minas Gerais':
                                    this.estadoSearch = 'MG';
                                    break;
                                case 'São Paulo':
                                    this.estadoSearch = 'SP';
                                    break;
                                case 'Rio de Janeiro':
                                    this.estadoSearch = 'RJ';
                                    break;
                                case 'Espírito Santo':
                                    this.estadoSearch = 'ES';
                                    break;
                                case 'Bahia':
                                    this.estadoSearch = 'BA';
                                    break;
                                case 'Rio Grande do Sul':
                                    this.estadoSearch = 'RS';
                                    break;
                                case 'Rio Grande do Norte':
                                    this.estadoSearch = 'RN';
                                    break;
                                case 'Santa Catarina':
                                    this.estadoSearch = 'SC';
                                    break;
                                case 'Paraná':
                                    this.estadoSearch = 'PR';
                                    break;
                                case 'Pará':
                                    this.estadoSearch = 'PA';
                                    break;
                                case 'Piauí':
                                    this.estadoSearch = 'PI';
                                    break;
                                case 'Pernambuco':
                                    this.estadoSearch = 'PE';
                                    break;
                                case 'Sergipe':
                                    this.estadoSearch = 'SE';
                                    break;
                                case 'Acre':
                                    this.estadoSearch = 'AC';
                                    break;
                                case 'Alagoas':
                                    this.estadoSearch = 'AL';
                                    break;
                                case 'Amapá':
                                    this.estadoSearch = 'AP';
                                    break;
                                case 'Amazonas':
                                    this.estadoSearch = 'AM';
                                    break;
                                case 'Ceará':
                                    this.estadoSearch = 'CE';
                                    break;
                                case 'Distrito Federal':
                                    this.estadoSearch = 'DF';
                                    break;
                                case 'Goiás':
                                    this.estadoSearch = 'GO';
                                    break;
                                case 'Maranhão':
                                    this.estadoSearch = 'MA';
                                    break;
                                case 'Mato Grosso':
                                    this.estadoSearch = 'MT';
                                    break;
                                case 'Mato Grosso do Sul':
                                    this.estadoSearch = 'MS';
                                    break;
                                case 'Paraíba':
                                    this.estadoSearch = 'PB';
                                    break;
                                case 'Rondônia':
                                    this.estadoSearch = 'RO';
                                    break;
                                case 'Roraima':
                                    this.estadoSearch = 'RR';
                                    break;
                                case 'Tocantins':
                                    this.estadoSearch = 'TO';
                                    break;
                                default:
                                    this.estadoSearch = '';
                                    break;
                            }
                            this.getVagas(
                                {
                                    nome: '',
                                    cidade: this.cidadeSearch,
                                    estado: this.estadoSearch
                                },
                                0);
                        } else{
                            this.cidadeSearch = '';
                            this.estadoSearch = '';
                        }
                    }
                );
            }
        ).catch(
            (error) => {
                this.loader.dismiss();
                const alert = this.alertCtrl.create(
                    {
                        title: 'Aviso',
                        subTitle: 'Não foi possível determinar sua localização. Utilize os filtros.',
                        buttons: ['OK']
                    }
                );
                alert.present();
                this.getVagas(
                    {
                        nome: '',
                        cidade: this.cidadeSearch,
                        estado: this.estadoSearch
                    },
                    0
                );
            }
        );
    }
}
