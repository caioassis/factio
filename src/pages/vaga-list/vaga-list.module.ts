import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VagaListPage } from './vaga-list';

@NgModule({
  declarations: [
    VagaListPage,
  ],
  imports: [
    IonicPageModule.forChild(VagaListPage),
  ],
})
export class VagaListPageModule {}
