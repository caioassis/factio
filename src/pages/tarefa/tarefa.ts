import {Component} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { LoadingController } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { AlertController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login/login';
import { TarefaListPage } from '../tarefa-list/tarefa-list';
import { Tarefa } from "../../models/tarefa";
import { TarefaProvider } from "../../providers/tarefa/tarefa";
import { MoradiaProvider } from "../../providers/moradia/moradia";
import {Usuario} from "../../models/usuario";
import {VarsProvider} from "../../providers/vars/vars";

@IonicPage()
@Component({
    selector: 'page-tarefa',
    templateUrl: 'tarefa.html',
})
export class TarefaPage
{
    tarefa: Tarefa;
    private tarefaForm: FormGroup;
    private selectOptions : any;
    dadosUsuario: any;
    membrosMoradia: Array<Usuario> = [];

    constructor(public navCtrl: NavController, public navParams: NavParams, private loadingCtrl: LoadingController,
                private formBuilder: FormBuilder, private tarefaProvider: TarefaProvider,
                private moradiaProvider: MoradiaProvider, public alertCtrl: AlertController,
                private storage: Storage, private varsProvider: VarsProvider)
    {
        if(this.navParams.get('mode') === 'edit')
            this.tarefa = this.navParams.get('tarefa');
        else
            this.tarefa = null;
        this.dadosUsuario = this.varsProvider.getUsuario();
        if(this.dadosUsuario)
        {
            this.tarefaProvider.setHeaderToken(this.dadosUsuario['accessToken']);
        } else
        {
            this.navCtrl.setRoot(LoginPage);
            this.navCtrl.popToRoot();
        }

        let nome = (this.tarefa ? this.tarefa.nome : '');
        let descricao = (this.tarefa ? this.tarefa.descricao : '');
        let tipo = (this.tarefa ? this.tarefa.tipo : '');
        let dataConclusao = (this.tarefa ? this.tarefa.dataConclusao : new Date().toISOString());
        let responsaveis = (this.tarefa ? this.tarefa.responsaveis : '');
        let moradiaId = (this.tarefa ? this.tarefa.moradiaId : this.dadosUsuario.usuario.id);

        this.tarefaForm = this.formBuilder.group(
            {
                nome: [
                    nome,
                    Validators.compose(
                        [
                            Validators.minLength(1),
                            Validators.maxLength(60),
                            Validators.required
                        ]
                    )
                ],
                descricao: [
                    descricao,
                    Validators.compose(
                        [
                            Validators.minLength(1),
                            Validators.maxLength(200),
                            Validators.required
                        ]
                    )
                ],
                tipo: [
                    tipo,
                    Validators.compose([Validators.required])
                ],
                dataConclusao: [
                    dataConclusao,
                    Validators.compose([Validators.required])
                ],
                responsaveis: [responsaveis, Validators.compose([Validators.required])],
            }
        );

        this.selectOptions = {
            title: 'Atribuir a Membros',
            subTitle: 'Escolha para quais membros do grupo a tarefa será atribuída.',
            mode: 'md'
        };

        this.moradiaProvider.getMembrosDaMoradia(
            this.dadosUsuario['usuario']['moradia']['id']
        ).subscribe(
            (response) => {
                if(response['meta']['messageCode'] === "msg.servico.execucao.sucesso")
                {
                    response['data']['usuarios'].forEach(
                        (item) => {
                            this.membrosMoradia.push(
                                {
                                    id: item.id,
                                    nome: item.nome,
                                } as Usuario
                            );
                        }
                    );
                } else
                {

                }
            },
            (error) => {

            }
        );

    }
    onSubmit()
    {

        const loader = this.loadingCtrl.create(
            {
                content: "Por favor, aguarde.",
            }
        );
        loader.present();

        let dadosTarefa = {
            nome: this.tarefaForm.value.nome,
            descricao: this.tarefaForm.value.descricao,
            tipo: this.tarefaForm.value.tipo,
            dataConclusao: this.tarefaForm.value.dataConclusao,
            moradiaId: this.dadosUsuario['usuario']['moradia']['id'],
            responsaveis: this.tarefaForm.value.responsaveis
        };

        if(this.navParams.get('mode') === 'edit')
        {
            this.tarefaProvider.updateTarefa(
                dadosTarefa as Tarefa
            ).subscribe(
                (response) => {
                    console.log(response);
                    if(response['meta']['messageCode'] === "msg.dataconclusao.invalida")
                    {
                        loader.dismiss();
                        const alert = this.alertCtrl.create(
                            {
                                title: 'Aviso',
                                subTitle: 'A data de conclusão da tarefa é inválida. Selecione outra data maior que a data atual.',
                                buttons: ['OK']
                            }
                        );
                        alert.present();
                    } else
                    {
                        loader.dismiss();
                        const alert = this.alertCtrl.create(
                            {
                                title: 'Tarefa editada!',
                                subTitle: 'Tarefa editada com sucesso!',
                                buttons: [
                                    {
                                        text: 'OK',
                                        handler: () => {
                                            this.navCtrl.popTo(TarefaListPage);
                                        }
                                    }
                                ]
                            }
                        );
                        alert.present();
                    }

                },
                (error) => {
                    console.log(error);

                    loader.dismiss();
                    const alert = this.alertCtrl.create(
                        {
                            title: 'Operação incompleta',
                            subTitle: 'Não foi possível editar a tarefa.',
                            buttons: [
                                {
                                    text: 'OK',
                                    handler: () => {
                                        this.navCtrl.popTo(TarefaListPage);
                                    }
                                }
                            ]
                        }
                    );
                    alert.present();
                }
            );

        } else
        {
            this.tarefaProvider.addTarefa(
                dadosTarefa as Tarefa
            ).subscribe(
                (response) => {
                    console.log(response);
                    if(response['meta']['messageCode'] === "msg.dataconclusao.invalida")
                    {
                        loader.dismiss();
                        const alert = this.alertCtrl.create(
                            {
                                title: 'Aviso',
                                subTitle: 'A data de conclusão da tarefa é inválida. Selecione outra data maior que a data atual.',
                                buttons: ['OK']
                            }
                        );
                        alert.present();
                    } else
                    {
                        loader.dismiss();
                        const alert = this.alertCtrl.create(
                            {
                                title: 'Tarefa Criada!',
                                subTitle: 'Tarefa Criada com sucesso!',
                                buttons: [
                                    {
                                        text: 'OK',
                                        handler: () => {
                                            this.navCtrl.popTo(TarefaListPage);
                                        }
                                    }
                                ]
                            }
                        );
                        alert.present();
                    }

                },
                (error) => {
                    console.log(error);
                    // Se nao estiver conectado, salva no dispositivo para posteriormente
                    // adicionar tarefa, quando possuir internet.
                    this.storage.get('tarefas').then(
                        (val) => {
                            let tarefasStorage = [];
                            if(val == null)
                            {
                                tarefasStorage = [
                                    dadosTarefa
                                ];
                            } else
                            {
                                tarefasStorage = JSON.parse(val);
                                tarefasStorage.push(dadosTarefa);
                            }
                            this.storage.set('tarefas', JSON.stringify(tarefasStorage));
                        }
                    );
                    loader.dismiss();
                    const alert = this.alertCtrl.create(
                        {
                            title: 'Operação incompleta',
                            subTitle: 'Não foi possível criar a tarefa. A tarefa foi salva em seu dispositivo e será criada assim que o dispositivo conectar-se à Internet.',
                            buttons: [
                                {
                                    text: 'OK',
                                    handler: () => {
                                        this.navCtrl.popTo(TarefaListPage);
                                    }
                                }
                            ]
                        }
                    );
                    alert.present();
                }
            );
        }
    }
}
