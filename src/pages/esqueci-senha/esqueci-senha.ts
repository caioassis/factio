import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { LoadingController } from 'ionic-angular';
import { UsuarioProvider} from "../../providers/usuario/usuario";
import {Storage} from "@ionic/storage";


@IonicPage()
@Component(
    {
        selector: 'page-esqueci-senha',
        templateUrl: 'esqueci-senha.html',
    }
)
export class EsqueciSenhaPage
{
    private form : FormGroup;

    constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder,
                private loadingCtrl: LoadingController, private usuarioProvider: UsuarioProvider)
    {
        this.form = this.formBuilder.group(
            {
                email: ['', Validators.required],

            }
        );
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad EsqueciSenhaPage');
    }

}
