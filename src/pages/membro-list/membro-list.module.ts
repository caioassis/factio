import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MembroListPage } from './membro-list';

@NgModule({
  declarations: [
    MembroListPage,
  ],
  imports: [
    IonicPageModule.forChild(MembroListPage),
  ],
})
export class MembroListPageModule {}
