import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import { VarsProvider } from "../../providers/vars/vars";
import { LoginPage } from "../login/login";
import { MoradiaProvider } from "../../providers/moradia/moradia";
import { Usuario } from "../../models/usuario";

@IonicPage()
@Component({
    selector: 'page-membro-list',
    templateUrl: 'membro-list.html',
})
export class MembroListPage
{
    dadosUsuario: any = null;
    membros: Array<Usuario> = [];
    searching = false;

    constructor(public navCtrl: NavController, public navParams: NavParams, private varsProvider: VarsProvider,
                private moradiaProvider: MoradiaProvider, private loadingCtrl: LoadingController,
                private alertCtrl: AlertController)
    {
        this.dadosUsuario = this.varsProvider.getUsuario();
        if(this.dadosUsuario)
        {
            this.moradiaProvider.setHeaderToken(this.dadosUsuario['accessToken']);
        } else
        {
            this.navCtrl.setRoot(LoginPage);
            this.navCtrl.popToRoot();
        }

    }

    ionViewWillEnter()
    {
        this.searching = true;
        let loader = this.loadingCtrl.create(
            {
                content: 'Por favor, aguarde.'
            }
        );
        loader.present();
        this.moradiaProvider.getMembrosDaMoradia(
            this.dadosUsuario['usuario']['moradia']['id']
        ).subscribe(
            (response) => {
                this.searching = false;
                loader.dismiss();
                console.log(response);
                if(response['meta']['messageCode'] === "msg.servico.execucao.sucesso")
                {
                    response['data']['usuarios'].forEach(
                        (item) => {
                            if(item.id !== this.dadosUsuario['usuario']['id'])
                                this.membros.push(item);
                        }
                    );
                } else
                {
                    const alert = this.alertCtrl.create(
                        {
                            title: 'Aviso',
                            subTitle: response['meta']['messageCode'],
                            buttons: ['OK']
                        }
                    );
                    alert.present();
                }
            },
            (error) => {
                this.searching = false;
                console.log(error);
                loader.dismiss();
                const alert = this.alertCtrl.create(
                    {
                        title: 'Aviso',
                        subTitle: 'Não foi possível realizar a operação.',
                        buttons: ['OK']
                    }
                );
                alert.present();
            }
        );
    }

}
