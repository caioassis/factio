import { Component, ViewChild } from '@angular/core';
import { Content, IonicPage, Loading, NavController, NavParams} from 'ionic-angular';
import { TarefaPage } from '../tarefa/tarefa';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { AvaliarTarefaPage } from "../avaliar-tarefa/avaliar-tarefa";
import { AlertController } from 'ionic-angular';
import { Moradia } from "../../models/moradia";
import { TarefaProvider } from "../../providers/tarefa/tarefa";
import { Tarefa } from "../../models/tarefa";
import { LoginPage } from "../login/login";
import {VarsProvider} from "../../providers/vars/vars";


@IonicPage()
@Component({
    selector: 'page-tarefa-list',
    templateUrl: 'tarefa-list.html',
})
export class TarefaListPage
{
    @ViewChild(Content) content: Content;
    private dadosUsuario: any;
    moradia: Moradia;
    search = false;
    searching = false;
    pageSearch: number = 0;
    totalPages: number;
    hasNextPageSearch: boolean = false;
    nomeSearch: string = '';

    scrollEnabled = true;
    loader: Loading;
    tarefaPage = TarefaPage;
    avaliarTarefaPage: any = AvaliarTarefaPage;
    tarefas: Array<Tarefa> = [
        // {
        //     id: 30,
        //     nome: 'Limpar a casa',
        //     descricao: 'Tem que limpar a casa, poxa',
        //     moradiaId: 1,
        //     responsaveis: [1, 4, 5],
        //     tipo: 'Tarefa Doméstica',
        //     dataConclusao: '2018-12-15'
        // } as Tarefa
    ];

    constructor(public navCtrl: NavController, public navParams: NavParams,
                private loadingCtrl: LoadingController, private tarefaProvider: TarefaProvider,
                public alertCtrl: AlertController,
                private storage: Storage, private varsProvider: VarsProvider)
    {
        this.moradia = this.navParams.get('moradia');
        this.dadosUsuario = this.varsProvider.getUsuario();
        if(this.dadosUsuario)
        {
            this.tarefaProvider.setHeaderToken(this.dadosUsuario['accessToken']);
        } else
        {
            this.navCtrl.setRoot(LoginPage);
            this.navCtrl.popToRoot();
        }
    }

    toggleSearchBar()
    {
        this.search = !this.search;
        if(this.search)
            this.content.scrollToTop();
    }

    onInput(event)
    {
        this.nomeSearch = event.target.value;
        this.pageSearch = 0;
        this.hasNextPageSearch = false;
        this.scrollEnabled = true;

        this.getTarefas(
            {
                nome: this.nomeSearch,
                moradia: this.moradia.id
            },
            0
        );
    }

    atualizarTarefa(tarefa, finalizado)
    {
        // const loader = this.loadingCtrl.create(
        //     {
        //         content: "Por favor, aguarde enquanto executamos a operação.",
        //     }
        // );
        // let url = 'http://167.250.197.3:1026/v1/contextEntities/' + tarefa.id.toString() + '/attributes/finalizado';
        // let mensagem = "";
        // let headers = new Headers();
        // headers.append('Content-Type', 'application/json');
        // headers.append('Accept', 'application/json');
        //
        // let options = new RequestOptions(
        //     {
        //         headers: headers
        //     }
        // );
        // let data = JSON.stringify(
        //     {
        //         "value": finalizado
        //     }
        // );
        // this.http.put(url, data, options).subscribe(
        //     data => {
        //         let statusCode = data.status;
        //         if(statusCode === 200)
        //         {
        //             mensagem = "Tarefa atualizada com sucesso.";
        //         } else {
        //             mensagem = "Não foi possível atualizar a tarefa. Atualize a lista de tarefas e tente novamente.";
        //         }
        //         loader.dismiss();
        //         const alert = this.alertCtrl.create(
        //             {
        //                 title: 'Mensagem',
        //                 subTitle: mensagem,
        //                 buttons: [
        //                     {
        //                         text: 'OK',
        //                         handler: () => {
        //                             this.getTarefas();
        //                         }
        //                     }
        //                 ]
        //             }
        //         );
        //         alert.present();
        //     },
        //     error => {
        //         mensagem = "Não foi possível atualizar a tarefa. Verifique sua conexão com a internet e tente novamente.";
        //         loader.dismiss();
        //         const alert = this.alertCtrl.create(
        //             {
        //                 title: 'Mensagem',
        //                 subTitle: mensagem,
        //                 buttons: [
        //                     {
        //                         text: 'OK',
        //                         handler: () => {
        //                             this.getTarefas();
        //                         }
        //                     }
        //                 ]
        //             }
        //         );
        //         alert.present();
        //     }
        // );
    }

    excluirTarefa(tarefa)
    {
        const loader = this.loadingCtrl.create(
            {
                content: "Por favor, aguarde enquanto executamos a operação.",
            }
        );

        this.tarefaProvider.deleteTarefa(tarefa.id).subscribe(
            (response) => {
                console.log('response', response);
                loader.dismiss();
                if(response['meta']['messageCode'] === "msg.tarefa.deletada")
                {
                    let index = this.tarefas.indexOf(tarefa);
                    this.tarefas.splice(index, 1);
                    const alert = this.alertCtrl.create(
                        {
                            title: 'Tarefa Excluída!',
                            subTitle: 'A tarefa foi excluída com sucesso.',
                            buttons: ['OK']
                        }
                    );
                    alert.present();
                }

            },
            (error) => {
                loader.dismiss();
                console.log('error', error);
                const alert = this.alertCtrl.create(
                    {
                        title: 'Aviso',
                        subTitle: "Não foi possível excluir a tarefa. Verifique sua conexão com a internet e tente novamente.",
                        buttons: ['OK']
                    }
                );
                alert.present();
            }
        );
    }

    getTarefas({nome, moradia}, page: number)
    {
        this.searching = true;
        if(!this.loader)
        {
            this.loader = this.loadingCtrl.create(
                {
                    content: 'Por favor, aguarde.'
                }
            );
            this.loader.present();
        }

        if(this.pageSearch == 0)
            this.tarefas = [];

        this.tarefaProvider.getTarefas(
            {
                nome: '',
                moradia: this.dadosUsuario.usuario.moradia.id
            },
            0
        ).subscribe(
            (response) => {
                console.log(response);
                this.searching = false;
                this.loader.dismiss();
                if(response['meta']['messageCode'] === "msg.servico.execucao.sucesso")
                {

                    response['data']['tarefas'].forEach(
                        (item) => {
                            if(!item.completa)
                            {
                                item['dataConclusao'] = item['dataConclusao'].split('T')[0].split('-')[2] + '/' + item['dataConclusao'].split('T')[0].split('-')[1] + '/' + item['dataConclusao'].split('T')[0].split('-')[0];
                                this.tarefas.push(item);
                            }
                        }
                    );


                } else
                {

                }
            },
            (error) => {
                console.log(error);
                this.searching = false;
                this.loader.dismiss();
                const alert = this.alertCtrl.create(
                    {
                        title: 'Mensagem',
                        subTitle: 'Não foi possível consultar as tarefas.',
                        buttons: ['OK']
                    }
                );
                alert.present();
            }
        );
    }

    scrollTarefas(infiniteScroll)
    {
        setTimeout(
            () => {
                if(this.hasNextPageSearch)
                {
                    this.pageSearch += 1;
                    this.getTarefas(
                        {
                            nome: this.nomeSearch,
                            moradia: this.dadosUsuario.usuario.moradia.id
                        },
                        this.pageSearch
                    );
                }
                if(this.pageSearch + 1 >= this.totalPages)
                {
                    this.scrollEnabled = false;
                    // infiniteScroll.enabled = false;
                }
                infiniteScroll.complete();
            },
            500
        );
    }

    ionViewWillEnter()
    {
        this.searching = true;
        this.loader = this.loadingCtrl.create(
            {
                content: 'Por favor, aguarde'
            }
        );
        this.loader.present();
        this.getTarefas(
            {
                nome: '',
                moradia: this.dadosUsuario.usuario.moradia.id
            },
            0
        );
    }
}
