import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AvaliarTarefaPage } from './avaliar-tarefa';

@NgModule({
  declarations: [
    AvaliarTarefaPage,
  ],
  imports: [
    IonicPageModule.forChild(AvaliarTarefaPage),
  ],
})
export class AvaliarTarefaPageModule {}
