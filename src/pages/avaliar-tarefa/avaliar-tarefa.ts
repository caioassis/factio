import { Component } from '@angular/core';
import { AlertController, IonicPage, LoadingController, NavController, NavParams } from 'ionic-angular';
import { Tarefa } from "../../models/tarefa";
import { VarsProvider } from "../../providers/vars/vars";
import { MoradiaProvider } from "../../providers/moradia/moradia";
import { TarefaProvider } from "../../providers/tarefa/tarefa";
import {LoginPage} from "../login/login";

@IonicPage()
@Component({
    selector: 'page-avaliar-tarefa',
    templateUrl: 'avaliar-tarefa.html',
})
export class AvaliarTarefaPage {
    nota: number = 0;
    tarefa: Tarefa;
    dadosUsuario: any = null;
    responsaveis = [];
    responsaveisStr = '';

    constructor(public navCtrl: NavController, public navParams: NavParams, private varsProvider: VarsProvider,
                private alertCtrl: AlertController, private loadingCtrl: LoadingController,
                private moradiaProvider: MoradiaProvider, private tarefaProvider: TarefaProvider)
    {
        this.tarefa = this.navParams.get('tarefa');
        this.dadosUsuario = this.varsProvider.getUsuario();
        if(this.dadosUsuario)
        {
            this.moradiaProvider.setHeaderToken(this.dadosUsuario['accessToken']);
            this.tarefaProvider.setHeaderToken(this.dadosUsuario['accessToken']);
        } else
        {
            this.navCtrl.setRoot(LoginPage);
            this.navCtrl.popToRoot();
        }
    }

    ionViewWillEnter()
    {
        const loader = this.loadingCtrl.create(
            {
                content: 'Por favor, aguarde.'
            }
        );
        loader.present();
        this.moradiaProvider.getMembrosDaMoradia(
            this.dadosUsuario['usuario']['moradia']['id']
        ).subscribe(
            (response) => {
                console.log(response);

                if(response['meta']['messageCode'] === "msg.servico.execucao.sucesso")
                {
                    loader.dismiss();
                    let nomeResponsaveis = [];
                    response['data']['usuarios'].forEach(
                        (item) => {
                            if(this.tarefa.responsaveis.indexOf(item.id) > -1)
                                nomeResponsaveis.push(item.nome);
                        }
                    );
                    this.responsaveisStr = nomeResponsaveis.join(', ');
                } else
                {

                }

            },
            (error) => {
                console.log(error);
                loader.dismiss();

            }
        );
    }

    submit()
    {
        let loader = this.loadingCtrl.create(
            {
                content: 'Por favor, aguarde.'
            }
        );
        loader.present();
        this.tarefaProvider.avaliarTarefa(
            {
                // idResponsavel: this.dadosUsuario['usuario']['id'],
                idTarefa: this.tarefa.id,
                qualidadeTarefa: this.nota

            }
        ).subscribe(
            (response) => {
                console.log(response);
                loader.dismiss();
                if(response['meta']['messageCode'] === "msg.tarefa.concluida")
                {
                    const alert = this.alertCtrl.create(
                        {
                            title: 'Tarefa Avaliada!',
                            subTitle: 'A tarefa foi avaliada com sucesso.',
                            buttons: [
                                {
                                    text: 'OK',
                                    handler: () => {
                                        this.navCtrl.pop();
                                    }
                                }
                            ]
                        }
                    );
                    alert.present();
                }
            },
            (error) => {
                console.log(error);
                loader.dismiss();
                const alert = this.alertCtrl.create(
                    {
                        title: 'Aviso',
                        subTitle: 'Não foi possível realizar a operação.',
                        buttons: [
                            {
                                text: 'OK',
                                handler: () => {
                                    this.navCtrl.pop();
                                }
                            }
                        ]
                    }
                );
                alert.present();

            }
        )

    }

    onChange(event)
    {
        console.log(event);
    }

}
