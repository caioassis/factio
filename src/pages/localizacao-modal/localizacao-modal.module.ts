import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LocalizacaoModalPage } from './localizacao-modal';

@NgModule({
  declarations: [
    LocalizacaoModalPage,
  ],
  imports: [
    IonicPageModule.forChild(LocalizacaoModalPage),
  ],
})
export class LocalizacaoModalPageModule {}
