import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Usuario} from "../../models/usuario";
import {VarsProvider} from "../../providers/vars/vars";
import {
    GoogleMaps,
    GoogleMap,
    Geocoder,
    GeocoderRequest,
    GoogleMapsEvent,
    GoogleMapOptions,
    Marker,
    Environment
} from '@ionic-native/google-maps';
import { Geolocation } from "@ionic-native/geolocation";

declare var plugin: any;

// https://docs.google.com/presentation/d/e/2PACX-1vScoho1ensbR4qCI9AIuQN55BZVvK73pAjI7sumDvW3CrxxHnrmpXWUjx2-8CpFibqU1EjLKCRhuthJ/pub?start=false&loop=false&delayms=3000&slide=id.g3e207b82bf_886_0
// http://www.fabricadecodigo.com/google-maps-e-geolocalizacao-com-ionic/
// https://github.com/ionic-team/ionic-native-google-maps/tree/master/documents/marker

// API KEY = AIzaSyCaSPHbraJ2Y2N04TGHVd14TTAdQzx9jIM


@IonicPage()
@Component({
    selector: 'page-localizacao-modal',
    templateUrl: 'localizacao-modal.html',
})
export class LocalizacaoModalPage
{
    membros: Array<Usuario> = [];
    dadosUsuario: any;
    map: GoogleMap;
    markers: Array<Marker> = [];

    constructor(public navCtrl: NavController, public navParams: NavParams, private varsProvider: VarsProvider,
                private geolocation: Geolocation, private alertCtrl: AlertController)
    {
        this.dadosUsuario = this.varsProvider.getUsuario();
        this.membros = this.navParams.get('membros');
        console.log('membros', this.membros);
    }


    ionViewDidLoad()
    {
        this.geolocation.getCurrentPosition().then(
            (resp) => {
                let req: GeocoderRequest = {
                    position: {
                        lat: resp.coords.latitude,
                        lng: resp.coords.longitude
                    }
                };
                Geocoder.geocode(req).then(
                    (results)=> {
                        // console.log('results', results);
                    }
                );
                this.loadMap(resp.coords);
            }
        ).catch(
            (error) => {
                const alert = this.alertCtrl.create(
                    {
                        title: 'Aviso',
                        subTitle: 'Não foi possível determinar sua localização.',
                        buttons: ['OK']
                    }
                );
                alert.present();
                this.loadMap(
                    {
                        // Coordenadas do Brasil
                        latitude: -14.235,
                        longitude: -51.9253
                    }
                );
            }
        );
    }

    public loadMap(coordinates)
    {
        // This code is necessary for browser
        Environment.setEnv(
            {
                'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyCaSPHbraJ2Y2N04TGHVd14TTAdQzx9jIM',
                'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyCaSPHbraJ2Y2N04TGHVd14TTAdQzx9jIM'
            }
        );

        let mapOptions: GoogleMapOptions = {
            camera: {
                target: {
                    lat: coordinates.latitude,
                    lng: coordinates.longitude
                },
                zoom: 14,
                tilt: 30
            }
        };

        this.map = GoogleMaps.create(document.getElementById('map_canvas'), mapOptions);

        this.map.one(GoogleMapsEvent.MAP_READY).then(
            () => {
                this.membros.forEach(
                    (membro) => {
                        if(membro.latitude && membro.longitude)
                        {
                            console.log('blaaaaaaaaaaaaaaaaa', membro);
                            let marker: Marker = this.map.addMarkerSync(
                                {
                                    title: membro.nome,
                                    icon: 'black',
                                    snippet: 'Última atualização: ' + membro['ultimoLogin'],
                                    animation: 'DROP',
                                    position: {
                                        lat: parseFloat(membro.latitude),
                                        lng: parseFloat(membro.longitude)
                                    }
                                }
                            );

                            marker.on(
                                GoogleMapsEvent.MARKER_CLICK
                            ).subscribe(
                                () => {
                                    marker.showInfoWindow();
                                },
                                (error) => {
                                    console.log('errooo')
                                }
                            );
                            this.markers.push(marker);
                        }
                    }
                );
            }
        ).catch(
            (error) => {
                console.log(error);
                const alert = this.alertCtrl.create(
                    {
                        title: 'Aviso',
                        subTitle: 'Não foi possível abrir o mapa.',
                        buttons: [
                            {
                                text: 'OK',
                                handler: () => {
                                    this.navCtrl.pop();
                                }
                            }
                        ]
                    }
                );
                alert.present();
            }
        );
    }
}
