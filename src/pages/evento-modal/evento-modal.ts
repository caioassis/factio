import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {VarsProvider} from "../../providers/vars/vars";
import {EventoProvider} from "../../providers/evento/evento";
import {Evento} from "../../models/evento";

@IonicPage()
@Component({
    selector: 'page-evento-modal',
    templateUrl: 'evento-modal.html',
})
export class EventoModalPage
{
    evento: Evento;
    dadosUsuario: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, private varsProvider: VarsProvider,
                private eventoProvider: EventoProvider)
    {
        this.dadosUsuario = this.varsProvider.getUsuario();
        if(this.dadosUsuario)
        {
            this.eventoProvider.setHeaderToken(this.dadosUsuario['accessToken']);
        }
        this.evento = this.navParams.get('evento');
        // this.evento.dataRealizacao = this.evento.dataRealizacao.split('T')[0].split('-')[2] + '/' + this.evento.dataRealizacao.split('T')[0].split('-')[1] + '/' + this.evento.dataRealizacao.split('T')[0].split('-')[0] + ' às ' + this.evento.dataRealizacao.split('T')[1].split('.')[0].split(':')[0] + ':' + this.evento.dataRealizacao.split('T')[1].split('.')[0].split(':')[1];
    }

    backButton()
    {
        this.navCtrl.pop();
    }

}
