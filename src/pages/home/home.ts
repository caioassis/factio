import { Component } from '@angular/core';
import {NavController, NavParams, ToastController} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login/login';
import { AlertController } from 'ionic-angular';
import { ModalController} from "ionic-angular";
import { MenuIndexPage } from "../menu-index/menu-index";
import { SemanaIndexPage } from "../semana-index/semana-index";
import { TarefaProvider } from "../../providers/tarefa/tarefa";
import { Geolocation } from "@ionic-native/geolocation";
import { MoradiaProvider } from "../../providers/moradia/moradia";
import { VarsProvider } from "../../providers/vars/vars";
import { UsuarioProvider } from "../../providers/usuario/usuario";
import {Tarefa} from "../../models/tarefa";

@Component(
    {
        selector: 'page-home',
        templateUrl: 'home.html'
    }
)
export class HomePage
{
    menuIndexPage: any = MenuIndexPage;
    semanaIndexPage: any = SemanaIndexPage;
    dadosUsuario: any = {};

    constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage,
                private varsProvider: VarsProvider,
                private geolocation: Geolocation,
                private toastCtrl: ToastController, private moradiaProvider: MoradiaProvider,
                private usuarioProvider: UsuarioProvider, private tarefaProvider: TarefaProvider)
    {
        this.dadosUsuario = this.varsProvider.getUsuario();
        if(this.dadosUsuario)
        {
            this.moradiaProvider.setHeaderToken(this.dadosUsuario['accessToken']);
            this.usuarioProvider.setHeaderToken(this.dadosUsuario['accessToken']);
            this.moradiaProvider.getMoradiaPeloUsuario(
                this.dadosUsuario.usuario.id
            ).subscribe(
                (response) => {
                    this.dadosUsuario.usuario.moradia = {
                        ...response['data']
                    };
                    this.varsProvider.setUsuario(this.dadosUsuario);
                    this.storage.set('dados', JSON.stringify(this.dadosUsuario));
                },
                (error) => {
                    // Provavelmente não encontrou a moradia
                }
            );
        } else
        {
            this.navCtrl.setRoot(LoginPage);
            this.navCtrl.popToRoot();
        }
    }

    ionViewWillEnter()
    {
        this.geolocation.getCurrentPosition().then(
            (resp) => {
                // Coordenadas do usuário
                let dataHorarioAtual = new Date().toISOString().split('.')[0];
                this.usuarioProvider.updateLocalizacaoUsuario(
                    {
                        usuarioId: this.dadosUsuario.usuario.id,
                        latitude: resp.coords.latitude.toString(),
                        longitude: resp.coords.longitude.toString(),
                        horaLocalizacao: dataHorarioAtual
                    }
                ).subscribe(
                    (response) => {
                        // console.log('response', response);

                    },
                    (error) => {
                        // console.log('error', error);
                    }
                );

                // sincronizar as tarefas
                this.storage.get('tarefas').then(
                    (val) => {
                        if(val != null)
                        {
                            let tarefas = JSON.parse(val);
                            let toast = this.toastCtrl.create(
                                {
                                    message: 'Sincronizando ' + (tarefas.length).toString() + ( tarefas.length > 1 ?' tarefas.' : ' tarefa.'),
                                    duration: 5000
                                }
                            );
                            toast.present();
                            let tarefasComErro = 0;
                            tarefas.forEach(
                                (item) => {
                                    this.tarefaProvider.addTarefa(
                                        item as Tarefa
                                    ).subscribe(
                                        (response) => {

                                        },
                                        (error) => {
                                            tarefasComErro += 1;

                                        }
                                    );
                                }
                            );
                            if(tarefasComErro > 0)
                            {
                                toast = this.toastCtrl.create(
                                    {
                                        message: 'Não foi possível sincronizar ' + tarefasComErro.toString() + (tarefasComErro == 1 ? ' tarefa.' : ' tarefas.')
                                    }
                                );
                                toast.present();
                            }
                        }
                    }
                );
            }
        ).catch(
            (e) => {
                // Não conseguiu adquirir as coordenadas
            }
        );
    }

    public logoff()
    {
        this.storage.remove('dados');
        this.storage.remove('tarefas');
        this.navCtrl.setRoot(LoginPage);
        this.navCtrl.popToRoot();
    }
}
