import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Vaga } from "../../models/vaga";
import 'rxjs/add/operator/map'


@Injectable()
export class VagaProvider
{
    url = 'http://167.250.199.228:8080';
    httpOptions = {
        headers: new HttpHeaders(
            {
                'Content-Type': 'application/json'
            }
        ),
        observe: 'response' as 'body'
    };

    accessToken = '';

    constructor(public http: HttpClient) {}

    public setHeaderToken(token)
    {
        this.accessToken = token;
    }

    public getVagas(
        {
            nome,
            cidade,
            estado,
        }, page): Observable<Vaga[]>
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);

        let params = new HttpParams();
        if(nome)
            params = params.append('nome', nome);
        if(cidade)
            params = params.append('cidade', cidade);
        if(estado)
            params = params.append('estado', estado);

        params = params.append('page', page);
        params = params.append('size', '10');
        return this.http.get<Vaga[]>(
            this.url + '/api/vagas',
            {
                ...this.httpOptions,
                params
            }
        ).map(
            (response) => response['body']
        );
    }

    public getVaga(id: number): Observable<Vaga>
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);

        let params = new HttpParams();
        params = params.append('id', id.toString());
        params = params.append('page', '0');
        params = params.append('size', '5');
        return this.http.get<Vaga[]>(
            this.url + '/api/moradias',
            {
                ...this.httpOptions,
                params
            }
        ).map(
            (response) => response['body'   ]
        );
    }

    public addVaga(vaga: Vaga): Observable<Vaga>
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);

        return this.http.post(
            this.url + '/api/vagas',
            vaga,
            this.httpOptions
        ).map(
            (response) => response['body']
        );
    }

    public updateVaga(vaga: Vaga): Observable<Vaga>
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);

        return this.http.put(
            this.url + '/api/vagas',
            vaga,
            this.httpOptions
        ).map(
            (response) => response['body']
        );
    }

    public deleteVaga(vaga: Vaga | number): Observable<Vaga>
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);

        const id = typeof vaga === 'number' ? vaga : vaga.id;

        return this.http.delete<Vaga>(
            this.url,
            this.httpOptions
        ).map(
            (response) => response['body']
        )
    }
}
