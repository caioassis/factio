import { Injectable } from '@angular/core';
import { Storage } from "@ionic/storage";


@Injectable()
export class VarsProvider
{
    private usuario: any = null;
    first = true;

    constructor(private storage: Storage)
    {

    }

    public init()
    {
        return this.storage.get('dados');
    }

    public getUsuario()
    {
        return this.usuario;
    }

    public setUsuario(usuario)
    {
        this.usuario = usuario;
    }
}
