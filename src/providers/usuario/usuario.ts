import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Usuario } from "../../models/usuario";
import 'rxjs/add/operator/map'


@Injectable()
export class UsuarioProvider
{
    url = 'http://167.250.199.228:8080';
    httpOptions = {
        headers: new HttpHeaders(
            {
                'Content-Type': 'application/json',
                // 'Accept': 'application/json'
            }
        ),
        observe: 'response' as 'body'
    };
    accessToken = '';

    constructor(public http: HttpClient)
    {

    }

    public setHeaderToken(token)
    {
        this.accessToken = token;
    }

    public authenticate(email: string, senha: string): Observable<Usuario>
    {
        return this.http.post<Usuario>(
            this.url + '/api/auth/login',
            {
                email,
                senha
            },
            this.httpOptions
        ).map(
            (response) => response['body']
        );
    }

    // public getUsuario(id: number): Observable<Usuario>
    // {
    //     this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
    //     return this.http.get<Usuario>(
    //         this.url
    //     ).pipe(
    //         tap(
    //             _ => {
    //                 console.log('pronto')
    //             }
    //         )
    //     );
    // }

    public addUsuario(usuario: Usuario)
    {
        return this.http.post(
            this.url + '/api/auth/signin',
            usuario,
            this.httpOptions
        ).map(
            (response) => response['body']
        );
    }

    public updateLocalizacaoUsuario(dados)
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.post(
            this.url + '/api/usuarios/salvarLocalizacao',
            dados,
            this.httpOptions
        ).map(
            (response) => response['body']
        );
    }

    public updateUsuario(usuario: Usuario)
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.put(
            this.url + '/api/usuarios',
            usuario,
            this.httpOptions
        ).map(
            (response) => response['body']
        );
    }

    public deleteUsuario(usuario: Usuario | number): Observable<Usuario>
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        const id = typeof usuario === 'number' ? usuario : usuario.id;

        return this.http.delete<Usuario>(
            this.url,
            this.httpOptions,
        ).pipe(
            tap(
                _ => {
                    console.log('pronto');
                }
            )
        );
    }

}
