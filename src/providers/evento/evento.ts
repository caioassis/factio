import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Evento } from "../../models/evento";
import 'rxjs/add/operator/map'


@Injectable()
export class EventoProvider
{
    url = 'http://167.250.199.228:8080/api/eventos';
    httpOptions = {
        headers: new HttpHeaders(
            {
                'Content-Type': 'application/json'
            }
        ),
        observe: 'response' as 'body'
    };
    accessToken = '';
    constructor(public http: HttpClient) {}

    public setHeaderToken(token)
    {
        this.accessToken = token;
    }

    public getEventos(
        {
            nome,
            cidade,
            estado
        },
        page
    ): Observable<Evento[]>
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);

        let params = new HttpParams();
        if(nome)
            params = params.append('nome', nome);
        if(cidade)
            params = params.append('cidade', cidade);
        if(estado)
            params = params.append('estado', estado);

        params = params.append('page', page);
        params = params.append('size', '10');
        return this.http.get<Evento[]>(
            this.url,
            {
                ...this.httpOptions,
                params
            }
        ).map(
            (response) => response['body']
        );
    }

    public getEvento(nome: string): Observable<Evento>
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);

        let params = new HttpParams();
        params = params.append('nome', nome);
        params = params.append('page', '0');
        params = params.append('size', '10');
        return this.http.get<Evento[]>(
            this.url,
            {
                ...this.httpOptions,
                params
            }
        ).map(
            (response) => response['body']
        );
    }

    public addEvento(evento: Evento): Observable<Evento>
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);

        return this.http.post(
            this.url,
            evento,
            this.httpOptions
        ).map(
            (response) => response['body']
        );
    }

    public updateEvento(evento: Evento, storageData)
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.put(
            this.url,
            evento,
            this.httpOptions
        ).map(
            (response) => response['body']
        );
    }

    public deleteEvento(evento: Evento): Observable<Evento>
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.delete<Evento>(
            this.url,
            this.httpOptions
        ).map(
            (response) => response['body']
        );
    }

}
