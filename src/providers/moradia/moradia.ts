import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import { Moradia } from "../../models/moradia";

@Injectable()
export class MoradiaProvider
{
    url = 'http://167.250.199.228:8080/api/moradias';
    httpOptions = {
        headers: new HttpHeaders(
            {
                'Content-Type': 'application/json'
            }
        ),
        observe: 'response' as 'body'
    };
    accessToken = '';

    constructor(public http: HttpClient) {}

    public setHeaderToken(token)
    {
        this.accessToken = token;
    }

    public getMoradias(
        {
            nome,
            cidade,
            estado
        }, page): Observable<Moradia[]>
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);

        let params = new HttpParams();
        if(nome)
            params = params.append('nome', nome);
        if(cidade)
            params = params.append('cidade', cidade);
        if(estado)
            params = params.append('estado', estado);

        params = params.append('page', page);
        params = params.append('size', '10');
        return this.http.get<Moradia[]>(
            this.url,
            {
                ...this.httpOptions,
                params
            }
        ).map(
            (response) => response['body']
        );
    }

    public getMembrosDaMoradia(moradiaId: number)
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        let params = new HttpParams();
        params = params.append('moradiaId', moradiaId.toString());
        return this.http.get(
            this.url + '/membros',
            {
                ...this.httpOptions,
                params
            }
        ).map(
            (response) => response['body']
        );
    }

    public getMoradiaPeloUsuario(usuarioId: number): Observable<Moradia>
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);

        return this.http.get<Moradia[]>(
            this.url + '/membro/' + usuarioId.toString(),
            {
                ...this.httpOptions,
            }
        ).map(
            (response) => response['body']
        );
    }

    public getMoradia(nome: string): Observable<Moradia>
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        let params = new HttpParams();
        params = params.append('nome', nome);
        params = params.append('page', '0');
        params = params.append('size', '10');
        return this.http.get<Moradia[]>(
            this.url,
            {
                ...this.httpOptions,
                params
            }
        ).map(
            (response) => response['body']
        );
    }

    public solicitarIngressoAMoradia(moradiaId, usuarioId)
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.post(
            this.url + '/solicitacaoIngresso',
            {
                moradia: moradiaId,
                usuario: usuarioId
            },
            this.httpOptions
        ).map(
            (response) => response['body']
        );
    }

    public listarSolicitacoesIngressoMoradia(moradiaId: number)
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.get(
            this.url + '/solicitacoes/' + moradiaId.toString(),
            this.httpOptions
        ).map(
            (response) => response['body']
        );
    }

    public removerSolicitacao(moradia, usuario)
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);

        return this.http.post(
            this.url + '/removerSolicitacao',
            {
                moradia,
                usuario
            },
            this.httpOptions
        ).map(
            (response) => response['body']
        );
    }

    public addMembroAMoradia(moradia, usuario)
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);

        return this.http.post(
            this.url + '/inserirMembro',
            {
                moradia,
                usuario
            },
            this.httpOptions
        ).map(
            (response) => response['body']
        );
    }

    public addMoradia(moradia: Moradia): Observable<Moradia>
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);

        return this.http.post(
            this.url,
            moradia,
            this.httpOptions
        ).map(
            (response) => response['body']
        );
    }

    public updateMoradia(moradia: Moradia)
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.put(
            this.url,
            moradia,
            this.httpOptions
        ).map(
            (response) => response['body']
        )
    }

    public deleteMoradia(moradia: Moradia | number): Observable<Moradia>
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        const id = typeof moradia === 'number' ? moradia : moradia.id;

        return this.http.delete<Moradia>(
            this.url,
            this.httpOptions
        ).pipe(
            tap(
                _ => {
                    console.log('pronto');
                }
            )
        );
    }

    public consultarCEP(cep)
    {
        let url = `https://viacep.com.br/ws/${cep}/json/`;
        return this.http.get(url);
    }
}
