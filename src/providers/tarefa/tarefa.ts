import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Tarefa } from "../../models/tarefa";
import 'rxjs/add/operator/map'


@Injectable()
export class TarefaProvider
{
    url = 'http://167.250.199.228:8080/api/tarefas';
    httpOptions = {
        headers: new HttpHeaders(
            {
                'Content-Type': 'application/json'
            }
        ),
        observe: 'response' as 'body'
    };
    accessToken = '';

    constructor(public http: HttpClient) {}

    public setHeaderToken(token)
    {
        this.accessToken = token;
    }

    public getTarefas(
        {
            nome,
            moradia
        }, page
    ): Observable<Tarefa[]>
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        let params = new HttpParams();
        if(nome)
            params = params.append('nome', nome);
        if(moradia)
            params = params.append('moradiaId', moradia);

        params = params.append('page', page);
        params = params.append('size', '10');
        return this.http.get<Tarefa[]>(
            this.url,
            {
                ...this.httpOptions,
                params
            }
        ).map(
            (response) => response['body']
        );
    }

    public getTarefa(
        {
            nome,
            moradia
        }
    ): Observable<Tarefa>
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);

        let params = new HttpParams();
        params = params.append('nome', nome);
        params = params.append('moradia', moradia);
        params = params.append('page', '0');
        params = params.append('size', '10');
        return this.http.get<Tarefa>(
            this.url,
            {
                ...this.httpOptions,
                params
            }
        ).map(
            (response) => response['body']
        );
    }

    public avaliarTarefa(dados)
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.post(
            this.url + '/concluirTarefa',
            dados,
            this.httpOptions
        ).map(
            (response) => response['body']
        );
    }

    public addTarefa(tarefa: Tarefa): Observable<Tarefa>
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);

        return this.http.post(
            this.url,
            tarefa,
            this.httpOptions
        ).map(
            (response) => response['body']
        );
    }

    public updateTarefa(tarefa: Tarefa)
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        return this.http.put(
            this.url,
            tarefa,
            this.httpOptions
        ).map(
            (response) => response['body']
        );
    }

    public deleteTarefa(tarefaId): Observable<Tarefa>
    {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + this.accessToken);
        this.httpOptions['body'] = {
            tarefaId: tarefaId.toString()
        };
        let params = new HttpParams();
        params = params.append('tarefaId', tarefaId.toString());
        return this.http.delete<Tarefa>(
            this.url,
            this.httpOptions
        ).map(
            (response) => response['body']
        );
    }
}
