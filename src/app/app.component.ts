import { Component, ViewChild, ElementRef} from '@angular/core';
import { Events} from "ionic-angular";
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { HomePage } from '../pages/home/home';
import { BasePage } from "../pages/base/base";
import { MoradiaListPage } from '../pages/moradia-list/moradia-list';
import { TarefaListPage } from '../pages/tarefa-list/tarefa-list';
import { EventoListPage } from "../pages/evento-list/evento-list";
import { LocalizacaoMembrosPage } from "../pages/localizacao-membros/localizacao-membros";
import { MoradiaDetailPage } from "../pages/moradia-detail/moradia-detail";
import { VagaListPage } from "../pages/vaga-list/vaga-list";
import { VagaPage } from "../pages/vaga/vaga";
import { LoginPage } from "../pages/login/login";
import { VarsProvider } from "../providers/vars/vars";
import { NotificacoesPage } from "../pages/notificacoes/notificacoes";

@Component({
    templateUrl: 'app.html'
})
export class MyApp
{
    @ViewChild(Nav) nav: Nav;
    @ViewChild('menuButton') menuButton: ElementRef;

    rootPage: any = LoginPage;
    tarefaListPage: any = TarefaListPage;
    moradiaListPage: any = MoradiaListPage;
    eventoListPage: any = EventoListPage;
    localizacaoMembros: any = LocalizacaoMembrosPage;
    homePage: any = HomePage;
    dadosUsuario: any;
    moradiaDetailPage: any = MoradiaDetailPage;
    vagaListPage = VagaListPage;
    vagaPage = VagaPage;
    basePage = BasePage;
    notificacoesPage = NotificacoesPage;

    pages: Array<{title: string, component: any}>;

    constructor(public platform: Platform, public statusBar: StatusBar, public events: Events,
                public splashScreen: SplashScreen, private storage: Storage, private varsProvider: VarsProvider)
    {
        this.dadosUsuario = this.varsProvider.init().then(
            (val) => {
                if(val)
                {

                    this.varsProvider.setUsuario(JSON.parse(val));
                }
                this.dadosUsuario = this.varsProvider.getUsuario();
                this.initializeApp();
            }
        );

        // used for an example of ngFor and navigation
        this.pages = [
            {title: 'Home', component: HomePage},
            {title: 'Moradias', component: MoradiaListPage},
            {title: 'Tarefas', component: TarefaListPage},
        ];

        this.events.subscribe(
            'updateUsuario',
            () => {
                this.updateUsuario();
            }
        );
    }

    updateUsuario()
    {
        this.dadosUsuario = this.varsProvider.getUsuario();
    }

    initializeApp() {
        this.platform.ready().then(
            () => {
                // Okay, so the platform is ready and our plugins are available.
                // Here you can do any higher level native things you might need.
                this.nav.setRoot(HomePage);
                this.storage.remove('tarefas');
                this.statusBar.styleDefault();
                this.splashScreen.hide();
            }
        );
    }

    public sair()
    {
        this.storage.remove('dados');
        this.storage.remove('tarefas');
        this.nav.setRoot(LoginPage);
        this.nav.popToRoot();
    }

    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    }

    goToAndSetRoot(page: any, params)
    {
        this.nav.setRoot(page, (params ? params : {}));
    }

    goToWithParams(page: any, params)
    {
        this.nav.push(page, params);
    }
}
