import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { FormsModule } from "@angular/forms";
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleMaps } from '@ionic-native/google-maps';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { MoradiaPage } from '../pages/moradia/moradia';
import { MoradiaListPage } from '../pages/moradia-list/moradia-list';
import { TarefaPage } from '../pages/tarefa/tarefa';
import { TarefaListPage } from '../pages/tarefa-list/tarefa-list';
import { EventoPage } from '../pages/evento/evento';
import { EventoListPage } from '../pages/evento-list/evento-list';
import { VagaPage } from '../pages/vaga/vaga';
import { VagaListPage } from '../pages/vaga-list/vaga-list';
import { EsqueciSenhaPage } from "../pages/esqueci-senha/esqueci-senha";
import { UsuarioCreatePage } from "../pages/usuario-create/usuario-create";
import { BasePage } from "../pages/base/base";
import { MenuIndexPage } from "../pages/menu-index/menu-index";
import { SemanaIndexPage } from "../pages/semana-index/semana-index";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { TarefaProvider } from '../providers/tarefa/tarefa';
import { MoradiaProvider } from '../providers/moradia/moradia';
import { VagaProvider } from '../providers/vaga/vaga';
import { EventoProvider } from '../providers/evento/evento';
import { UsuarioProvider } from '../providers/usuario/usuario';
import { LocalizacaoMembrosPage } from "../pages/localizacao-membros/localizacao-membros";
import { AvaliarTarefaPage } from "../pages/avaliar-tarefa/avaliar-tarefa";
import { MoradiaDetailPage } from "../pages/moradia-detail/moradia-detail";
import { MembroDetailPage } from "../pages/membro-detail/membro-detail";
import { MembroListPage } from "../pages/membro-list/membro-list";
import { MoradiaModalPage } from "../pages/moradia-modal/moradia-modal";
import { EventoModalPage } from "../pages/evento-modal/evento-modal";
import { VagaModalPage } from "../pages/vaga-modal/vaga-modal";
import { VarsProvider } from '../providers/vars/vars';
import { NotificacoesPage } from "../pages/notificacoes/notificacoes";
import { LocalizacaoModalPage } from "../pages/localizacao-modal/localizacao-modal";


@NgModule({
    declarations: [
        MyApp,
        HomePage,
        LoginPage,
        MoradiaPage,
        MoradiaListPage,
        TarefaPage,
        TarefaListPage,
        EventoPage,
        EventoListPage,
        VagaPage,
        VagaListPage,
        EsqueciSenhaPage,
        UsuarioCreatePage,
        BasePage,
        MenuIndexPage,
        SemanaIndexPage,
        LocalizacaoMembrosPage,
        AvaliarTarefaPage,
        MoradiaDetailPage,
        MembroDetailPage,
        MembroListPage,
        MoradiaModalPage,
        EventoModalPage,
        VagaModalPage,
        NotificacoesPage,
        LocalizacaoModalPage
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        IonicStorageModule.forRoot(),
        HttpClientModule,
        FormsModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        LoginPage,
        MoradiaPage,
        MoradiaListPage,
        TarefaPage,
        TarefaListPage,
        EventoPage,
        EventoListPage,
        VagaPage,
        VagaListPage,
        EsqueciSenhaPage,
        UsuarioCreatePage,
        BasePage,
        MenuIndexPage,
        SemanaIndexPage,
        LocalizacaoMembrosPage,
        AvaliarTarefaPage,
        MoradiaDetailPage,
        MembroDetailPage,
        MembroListPage,
        MoradiaModalPage,
        EventoModalPage,
        VagaModalPage,
        NotificacoesPage,
        LocalizacaoModalPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        GoogleMaps,
        Network,
        {
            provide: ErrorHandler,
            useClass: IonicErrorHandler
        },
        Geolocation,
        TarefaProvider,
        MoradiaProvider,
        VagaProvider,
        EventoProvider,
        UsuarioProvider,
        VarsProvider
    ]
})
export class AppModule {}
