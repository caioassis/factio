export class Tarefa
{
    id: number;
    nome: string;
    descricao: string;
    tipo: string;
    dataConclusao: string;
    moradiaId: number;
    responsaveis: Array<number>
}
