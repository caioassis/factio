export class Vaga
{
    id: number;
    descricao: string;
    criadorId: number;
    qtdQuartos: string;
    observacoes: string;
    publicoAlvo: string;
    moradiaId: number;
}
