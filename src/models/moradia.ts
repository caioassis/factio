export class Moradia
{
    id: number;
    Usuario_id: string;
    nome: string;
    apelido: string;
    endereco: string;
    numero: number;
    complemento: string;
    bairro: string;
    cidade: string;
    estado: string;
    cep: string;
    comentario: string;
    data_criacao: string;
    responsavel: number;
}
