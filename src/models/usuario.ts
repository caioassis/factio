export class Usuario
{
    id: number;
    nome: string;
    telefone: string;
    email: string;
    senha: string;
    ativo: boolean;
    perfil: number;
    latitude: string;
    longitude: string;
}
