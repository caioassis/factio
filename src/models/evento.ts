export class Evento
{
    id: number;
    nome: string;
    dataRealizacao: string;
    descricao: string;
    telefoneContato: string;
    atracoes: string;
    valorEntrada: number;
    criadorId: number;
    moradiaId: number;
}
